[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/ambv/black)

## Dependencies

To work with application you need to have `docker-compose` and `Make` 
installed in your environment.


## Installation

To install dependencies use: 
> `make build`

To set up pre-commit hooks for formatter and linter, use: 
> `pre-commit install`

## Development

To run the formatter for a specific file, use:

> `black <path/to/the/file>`

To run the formatter for all project, use:

> `black .`

## Project management
    $ make build
    $ make run
    $ make migrations
    $ make migrate

## Containers description

 `API` - [Flask](https://flask.palletsprojects.com/en/2.0.x/) based backend service. Depend on `postgres` database container.
 
`Postgres` - database container with default database settings. Please use it only for local development.
For other environments use RDS or other remote database.

`s3` - [Minio](https://min.io/) container for local development. It's a replica of AWS S3 file storage.

`worker` - [Celery](https://docs.celeryproject.org/en/stable/getting-started/introduction.html) based container for async tasks.

`Monitor` - [Flower](https://flower.readthedocs.io/en/latest/) monitoring system for async tasks management.

`redis` - [Redis](https://redis.io/) using as a message queue for celery async tasks.
