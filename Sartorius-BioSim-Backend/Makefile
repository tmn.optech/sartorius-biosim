PROJECT_NAME=api

# colors
GREEN = $(shell tput -Txterm setaf 2)
YELLOW = $(shell tput -Txterm setaf 3)
WHITE = $(shell tput -Txterm setaf 7)
RESET = $(shell tput -Txterm sgr0)
GRAY = $(shell tput -Txterm setaf 6)
TARGET_MAX_CHAR_NUM = 20

# Common

all: run

## Runs application. Builds, creates, starts, and attaches to containers for a service. | Common
run:
	docker-compose up

## Rebuild xcm_app container
build:
	docker-compose build

## Stops application. Stops running container without removing them.
stop:
	@docker-compose stop

## Removes stopped service containers.
clean:
	@docker-compose down

## Runs command `bash` commands in docker container.
bash:
	@docker-compose exec $(PROJECT_NAME) sh

test:
	@docker exec -it $(PROJECT_NAME) pytest;
# Help

## Shows help.
help:
	@echo ''
	@echo 'Usage:'
	@echo ''
	@echo '  ${YELLOW}make${RESET} ${GREEN}<target>${RESET}'
	@echo ''
	@echo 'Targets:'
	@awk '/^[a-zA-Z\-]+:/ { \
		helpMessage = match(lastLine, /^## (.*)/); \
		if (helpMessage) { \
		    if (index(lastLine, "|") != 0) { \
				stage = substr(lastLine, index(lastLine, "|") + 1); \
				printf "\n ${GRAY}%s: \n\n", stage;  \
			} \
			helpCommand = substr($$1, 0, index($$1, ":")-1); \
			helpMessage = substr(lastLine, RSTART + 3, RLENGTH); \
			if (index(lastLine, "|") != 0) { \
				helpMessage = substr(helpMessage, 0, index(helpMessage, "|")-1); \
			} \
			printf "  ${YELLOW}%-$(TARGET_MAX_CHAR_NUM)s${RESET} ${GREEN}%s${RESET}\n", helpCommand, helpMessage; \
		} \
	} \
	{ lastLine = $$0 }' $(MAKEFILE_LIST)
	@echo ''

# Docs

# Linters & tests

## Formats code with `black`. | Linters
black:
	@docker-compose run --rm $(PROJECT_NAME) black $(PROJECT_NAME) --exclude migrations -l 79

## Formats code with `flake8`.
lint:
	@docker-compose run --rm $(PROJECT_NAME) flake8 $(PROJECT_NAME)

# Database

## Runs PostgreSQL UI. | Database
psql:
	@docker exec -it postgres psql -U postgres

## Makes migration.
migrations:
	@docker exec -it $(PROJECT_NAME) flask db migrate;

## Upgrades database.
migrate:
	@docker exec -it $(PROJECT_NAME) flask db upgrade;

## Downgrades database.
downgrade:
	@docker-compose exec $(PROJECT_NAME) flask db downgrade;


## Version migrations.
version:
	@docker-compose exec $(PROJECT_NAME) flask db current;