[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/ambv/black)

## API application information

The design of application is based on DDD approach.
is the concept that the structure and language of software code (class names, class methods, class variables) should match the business domain. For example, if a software processes loan applications, it might have classes such as LoanApplication and Customer, and methods such as AcceptOffer and Withdraw.

DDD connects the implementation to an evolving model.

Domain-driven design is predicated on the following goals:

placing the project's primary focus on the core domain and domain logic;

basing complex designs on a model of the domain;

initiating a creative collaboration between technical and domain experts to iteratively refine a conceptual model that addresses particular domain problems.
Criticisms of domain-driven design argue that developers must typically implement a great deal of isolation and encapsulation to maintain the model as a pure and helpful construct. While domain-driven design provides benefits such as maintainability, Microsoft recommends it only for complex domains where the model provide clear benefits in formulating a common understanding of the domain.

Each module contains with common objects:
> `application` - web app representation module. Includes routes, handlers, validators
for processing requests. Also includes services to cover all business logic.

> `domain` - Representation of domain entities.

> `infrastructure` - infrastructure part of the application. Includes SQL alchemy models and repositories.


## Tools to use: 

For logging purpose please use [loguru](https://github.com/Delgan/loguru) logger. It provides a flexible way of logging.
by default it is setting up to represent log message as strings with request detailed information.

Use [flask-id-header](https://pypi.org/project/flask-request-id-header/) for adding specific request id to trek request log message all over the log messages.

For setting up simple way to describe each class initialisation [dependency-injector](https://pypi.org/project/dependency-injector/3.14.12/) is used.
You can use this approach to describe all object initiation one and reuse it all over the application.