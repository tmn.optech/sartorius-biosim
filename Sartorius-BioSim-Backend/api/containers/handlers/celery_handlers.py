from dependency_injector import containers, providers
from celery_queue.application import CeleryHandler


class CeleryContextHandlers(containers.DeclarativeContainer):
    celery_handler = providers.Singleton(CeleryHandler)
