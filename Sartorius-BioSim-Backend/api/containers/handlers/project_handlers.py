from dependency_injector import containers, providers
from containers.services.projects_container import ProjectContextContainer
from projects.application.handlers import (
    ProjectHandler,
    FitParametersHandler,
    FitProcedureHandler,
    ProjectSettingsHandler,
    MetaboliteConsumptionsRateHandler,
    FeedHandler,
)


class ProjectContextHandlers(containers.DeclarativeContainer):
    serializer = ProjectContextContainer.project_serializer
    project_validator = ProjectContextContainer.project_validator
    settings_validator = ProjectContextContainer.project_settings_validator
    feed_validator = ProjectContextContainer.feed_validator
    project_data_service = ProjectContextContainer.project_data_service
    project_settings_service = ProjectContextContainer.project_settings_service
    feed_service = ProjectContextContainer.feed_service
    model_fitting_service = ProjectContextContainer.model_fitting_service
    metabolite_service = ProjectContextContainer.metabolite_rates_service
    project_handler = providers.Singleton(
        ProjectHandler,
        project_validator,
        project_data_service,
        model_fitting_service,
        serializer,
    )

    project_settings_handler = providers.Singleton(
        ProjectSettingsHandler,
        settings_validator,
        project_settings_service,
        serializer,
    )

    fit_parameter_handler = providers.Singleton(
        FitParametersHandler,
        settings_validator,
        project_settings_service,
        serializer,
    )

    fit_procedure_handler = providers.Singleton(
        FitProcedureHandler,
        settings_validator,
        project_settings_service,
        serializer,
    )

    metabolite_handler = providers.Singleton(
        MetaboliteConsumptionsRateHandler,
        project_data_service,
        metabolite_service,
        serializer,
    )

    feed_handler = providers.Singleton(
        FeedHandler,
        feed_validator,
        feed_service,
        serializer,
    )
