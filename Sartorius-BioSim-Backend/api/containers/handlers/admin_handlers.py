from dependency_injector import containers, providers

from admin.application.handlers import ProjectDuplicationHandler
from containers.services.admin_container import AdminContextContainer
from containers.services.application_container import AppContextContainer


class AdminContextHandlers(containers.DeclarativeContainer):
    serializer = AppContextContainer.serializer
    project_duplication_service = (
        AdminContextContainer.project_duplication_service
    )
    project_duplication_handler = providers.Singleton(
        ProjectDuplicationHandler, serializer, project_duplication_service
    )
