from dependency_injector import containers, providers

from containers.services.application_container import AppContextContainer
from simulation.application.handlers import (
    SimulationHandler,
    EventsHandler,
    InitialConditionHandler,
)
from containers.services.projects_container import ProjectContextContainer
from containers.services.simulation_container import SimulationContextContainer


class SimulationContextHandlers(containers.DeclarativeContainer):
    data_service = ProjectContextContainer.project_data_service
    simulation_service = SimulationContextContainer.simulation_service
    event_validator = SimulationContextContainer.events_validator
    initial_conditions_validator = (
        SimulationContextContainer.initial_condition_validator
    )
    event_service = SimulationContextContainer.event_service
    initial_conditions_service = (
        SimulationContextContainer.initial_condition_service
    )
    serializer = AppContextContainer.serializer
    simulation_handler = providers.Singleton(
        SimulationHandler, data_service, simulation_service
    )
    events_handler = providers.Singleton(
        EventsHandler, event_validator, event_service, serializer
    )
    initial_condition_handler = providers.Singleton(
        InitialConditionHandler,
        initial_conditions_validator,
        initial_conditions_service,
        serializer,
    )
