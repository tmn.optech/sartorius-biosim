from dependency_injector import providers, containers

from projects.application.serializers.project_serializer import (
    ProjectSerializer,
)
from projects.application.services.feed_service import FeedService
from projects.application.services.project_service import ProjectService
from projects.application.services.project_settings_service import (
    ProjectSettingsService,
)
from projects.application.services.model_fitting_service import (
    ModelFittingService,
)
from projects.application.services.remote_storage_service import (
    RemoteStorageService,
)
from containers.services.application_container import AppContextContainer
from projects.application.validators.project_data_validators import (
    ProjectDataValidator,
)
from projects.application.validators.project_settings_validators import (
    ProjectSettingsValidator,
)
from projects.application.validators.feed_validators import FeedValidator
from projects.infrastructure.repository import (
    ProjectRepository,
    FitProcedureRepository,
    FitParameterRepository,
)
from projects.infrastructure.repository.feed_repository import (
    FeedRepository,
)
from projects.infrastructure.repository.project_settings_repository import (
    ProjectSettingsRepository,
)
from projects.application.services.metabolite_rates_service import (
    MetaboliteRatesService,
)


class ProjectContextContainer(containers.DeclarativeContainer):
    logger = AppContextContainer.logger
    serializer = AppContextContainer.serializer
    project_validator = providers.Singleton(ProjectDataValidator)
    project_serializer = providers.Singleton(ProjectSerializer)
    project_repository = providers.Singleton(ProjectRepository)
    remote_storage_service = providers.Singleton(RemoteStorageService)
    project_data_service = providers.Singleton(
        ProjectService, project_repository, remote_storage_service
    )
    project_settings_validator = providers.Singleton(ProjectSettingsValidator)
    project_settings_repository = providers.Singleton(
        ProjectSettingsRepository
    )
    fit_procedure_repository = providers.Singleton(FitProcedureRepository)
    fit_parameter_repository = providers.Singleton(FitParameterRepository)
    feed_repository = providers.Singleton(FeedRepository)
    project_settings_service = providers.Singleton(
        ProjectSettingsService,
        project_settings_repository,
        fit_parameter_repository,
        fit_procedure_repository,
    )
    feed_validator = providers.Singleton(FeedValidator)
    feed_service = providers.Singleton(
        FeedService,
        feed_repository,
    )
    model_fitting_service = providers.Singleton(
        ModelFittingService,
        serializer,
        project_data_service,
        remote_storage_service,
        project_settings_service,
        feed_service,
    )
    metabolite_rates_service = providers.Singleton(
        MetaboliteRatesService,
        serializer,
        project_data_service,
        remote_storage_service,
        project_settings_service,
        feed_service,
        model_fitting_service,
    )
