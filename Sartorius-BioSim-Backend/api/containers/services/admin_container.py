from dependency_injector import containers, providers

from admin.application.services.duplication_service import (
    ProjectDuplicationService,
)
from containers.services.application_container import AppContextContainer
from containers.services.projects_container import ProjectContextContainer
from containers.services.simulation_container import SimulationContextContainer


class AdminContextContainer(containers.DeclarativeContainer):
    serializer = AppContextContainer.serializer
    remote_storage = ProjectContextContainer.remote_storage_service
    project_repository = ProjectContextContainer.project_repository
    project_settings_service = ProjectContextContainer.project_settings_service
    feed_service = ProjectContextContainer.feed_service
    event_service = SimulationContextContainer.event_service
    initial_conditions_service = (
        SimulationContextContainer.initial_condition_service
    )
    project_duplication_service = providers.Singleton(
        ProjectDuplicationService,
        serializer,
        remote_storage,
        project_repository,
        project_settings_service,
        feed_service,
        event_service,
        initial_conditions_service,
    )
