from dependency_injector import containers, providers

from helpers.infrastructure.model_serializer import ModelSerializer
from shared.logger import Logger


class AppContextContainer(containers.DeclarativeContainer):
    logger = providers.Singleton(Logger)
    serializer = providers.Singleton(ModelSerializer)
