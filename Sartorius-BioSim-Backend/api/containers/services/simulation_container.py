from dependency_injector import providers, containers

from simulation.application.services.event_service import EventService
from simulation.application.services.initial_condition_service import (
    InitialConditionsService,
)
from simulation.application.services.simulation_service import (
    SimulationService,
)
from containers.services.projects_container import ProjectContextContainer

from projects.application.services.project_settings_service import (
    ProjectSettingsService,
)
from projects.infrastructure.repository import (
    FitProcedureRepository,
    FitParameterRepository,
)
from projects.infrastructure.repository.project_settings_repository import (
    ProjectSettingsRepository,
)
from simulation.application.validators.event_data_validators import (
    EventsDataValidator,
)
from simulation.application.validators.initial_conditions_data_validators import (
    InitialConditionsDataValidator,
)
from simulation.infrastructure.repository.event_repository import (
    EventRepository,
)
from simulation.infrastructure.repository.initial_condition_repository import (
    InitialConditionRepository,
)


class SimulationContextContainer(containers.DeclarativeContainer):
    project_settings_repository = providers.Singleton(
        ProjectSettingsRepository
    )
    fit_procedure_repository = providers.Singleton(FitProcedureRepository)
    fit_parameter_repository = providers.Singleton(FitParameterRepository)
    project_settings_service = providers.Singleton(
        ProjectSettingsService,
        project_settings_repository,
        fit_parameter_repository,
        fit_procedure_repository,
    )
    fitting_service = ProjectContextContainer.model_fitting_service
    simulation_service = providers.Singleton(
        SimulationService, fitting_service, project_settings_service
    )
    initial_condition_validator = providers.Singleton(
        InitialConditionsDataValidator
    )
    events_validator = providers.Singleton(EventsDataValidator)
    initial_condition_repository = providers.Singleton(
        InitialConditionRepository
    )
    events_repository = providers.Singleton(EventRepository)
    event_service = providers.Singleton(EventService, events_repository)
    initial_condition_service = providers.Singleton(
        InitialConditionsService, initial_condition_repository
    )
