from dependency_injector import providers, containers
from users.application.services import AuthService
from users.application.validators import UserDataValidators
from users.application.services.token_provider import TokenProvider
from users.application.services.users_service import UsersService
from containers.services.application_container import AppContextContainer
from users.infrastructure.repository import (
    UsersProfileRepository,
    UsersRepository,
)
from users.infrastructure.repository.token_repository import JwtTokenRepository


class UserContextContainer(containers.DeclarativeContainer):
    user_repository = providers.Singleton(UsersRepository)
    profile_repository = providers.Singleton(UsersProfileRepository)
    token_repository = providers.Singleton(JwtTokenRepository)
    logger = AppContextContainer.logger
    token_provider = providers.Singleton(TokenProvider)
    user_data_service = providers.Singleton(UsersService, user_repository)
    user_data_validators = providers.Singleton(
        UserDataValidators, token_provider
    )
    auth_service = providers.Singleton(
        AuthService,
        token_provider,
        user_repository,
        profile_repository,
        token_repository,
    )
