from flask import Flask
from flask_restful import Api

from admin.application.routes import init_admin_routes
from check_alive import check_alive
from celery_queue.application.routes import init_celery_routes
from projects.application.routes import (
    init_project_routes,
    init_fit_procedure_routes,
    init_fit_parameter_routes,
    init_project_settings_routes,
    init_metabolite_rates_urls,
    init_project_feed_urls,
)
from users.application.routes import init_user_routes, init_user_auth_routes
from simulation.application.routes import (
    init_simulation_routes,
    init_event_routes,
    init_initial_conditions_routes,
)


def init_routes(api: Api, app: Flask):
    init_user_auth_routes(api, app)
    init_user_routes(api, app)
    init_project_routes(api, app)
    init_project_settings_routes(api, app)
    init_fit_procedure_routes(api, app)
    init_fit_parameter_routes(api, app)
    init_celery_routes(api, app)
    init_simulation_routes(api, app)
    init_metabolite_rates_urls(app)
    init_project_feed_urls(app)
    init_event_routes(api, app)
    init_initial_conditions_routes(api, app)
    init_admin_routes(api, app)

    # System routes
    app.add_url_rule(
        "/api/check-alive",
        "check_alive",
        check_alive,
        methods=["GET"],
    )
