from helpers.domain.base_id import BaseID


class UserID(BaseID):
    """Entity for representing id of user entity"""
