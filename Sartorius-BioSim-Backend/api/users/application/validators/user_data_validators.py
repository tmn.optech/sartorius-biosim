from users.application.validators.user_schemas import UserSchemas
from users.application.services.token_provider import TokenProvider
from helpers.validators.request_validator import RequestValidator


class UserDataValidators:
    def __init__(self, token_provider: TokenProvider):
        self.__token_provider = token_provider

    @staticmethod
    def sign_up_validate(params: dict) -> dict:
        """Method validator for body parameters in post sign up request

        :param params: body parameters in dict representation
        :return: raise Bad Request if invalid schema or return clean params"""

        validator = RequestValidator(UserSchemas.sign_up, params)
        validator.is_valid(raise_exception=True)
        data = validator.validated_data
        del data["password_confirmation"]
        return data

    @staticmethod
    def sign_in_validate(params: dict) -> dict:
        """Method validator for body parameters in post sing in request

        :param params: body parameters in dict representation
        :return: raise Bad Request if invalid schema or return clean params"""

        validator = RequestValidator(UserSchemas.sign_in, params)
        validator.is_valid(raise_exception=True)

        return validator.validated_data

    @staticmethod
    def create_user_validate(params: dict) -> dict:
        """Method validator for body parameters in post user request

        :param params: body parameters in dict representation
        :return: raise Bad Request if invalid schema or return clean params"""

        validator = RequestValidator(UserSchemas.user_create, params)
        validator.is_valid(raise_exception=True)

        return validator.validated_data
