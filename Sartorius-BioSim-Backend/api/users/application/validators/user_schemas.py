from helpers.validators.schemas_field_properties import (
    email_property,
    password_property,
)


class UserSchemas:
    """Schemas for user GET / POST requests for user entity"""

    sign_in = {
        "type": "object",
        "properties": {
            "email": email_property._asdict(),
            "password": password_property._asdict(),
        },
        "required": ["email", "password"],
    }

    sign_up = {
        "type": "object",
        "properties": {
            "email": email_property._asdict(),
            "password": password_property._asdict(),
            "password_confirmation": password_property._asdict(),
            "first_name": {"type": "string", "maxLength": 32},
            "last_name": {"type": "string", "maxLength": 32},
            "is_admin": {"type": "boolean"},
        },
        "required": [
            "email",
            "password",
            "password_confirmation",
            "first_name",
            "last_name",
        ],
    }
    user_create = {
        "type": "object",
        "properties": {
            "email": email_property._asdict(),
            "password": password_property._asdict(),
            "is_admin": {"type": "boolean"},
        },
        "required": ["email", "password"],
    }
