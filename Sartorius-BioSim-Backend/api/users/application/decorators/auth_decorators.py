"""
Decorators will be used to auth users with different types of auth.
"""
from functools import wraps
from typing import Callable
from flask import request
from containers.services.users_container import UserContextContainer
from helpers.infrastructure import status
from helpers.infrastructure.status import (
    HTTP_400_BAD_REQUEST,
    HTTP_401_UNAUTHORIZED,
)
from shared.response import Response
from users.domain.user_id import UserID


def jwt_requires(method: Callable) -> Callable:
    """Decorate methods with this to require that the user has valid JWT token.
    If the request will be without token decorator will raise 401 status code response
    """

    @wraps(method)
    def wrapper(self, *args, **kwargs):
        jwt_token = request.headers.get("Authorization")
        if not jwt_token:
            return Response(
                status_code=HTTP_400_BAD_REQUEST, body="JWT token missing"
            ).json
        payload = UserContextContainer.token_provider().get_token_info(
            jwt_token
        )
        token_blacklisted = UserContextContainer.token_repository().get_token(
            jwt_token
        )
        if not payload.get("user_id") or token_blacklisted:
            return Response(
                status_code=HTTP_401_UNAUTHORIZED,
                body="JWT token not verified",
            ).json
        # Check if such user exist
        user_id = UserID(payload["user_id"])
        user = UserContextContainer.user_repository().get_one_by_id(user_id)
        if not user:
            return Response(
                status_code=status.HTTP_400_BAD_REQUEST,
                body="Provided user ID is not valid.",
            ).json
        request.environ["USER-ID"] = user_id.base_58
        request.environ["URL"] = request.path
        request.environ["METHOD"] = request.method
        request.user_id = UserID(payload["user_id"])
        return method(self, request, *args, **kwargs)

    return wrapper


def admin_permission_required(method: Callable) -> Callable:
    """Decorate methods with this to require that the user has valid JWT token.
    If the request will be without token decorator will raise 401 status code response
    """

    @wraps(method)
    def wrapper(self, *args, **kwargs):
        user_id = request.user_id
        user = UserContextContainer.user_repository().get_one_by_id(user_id)
        if not user.is_admin:
            return Response(
                status_code=status.HTTP_403_FORBIDDEN,
                body="Admin permissions required.",
            ).json
        return method(self, request, *args, **kwargs)

    return wrapper
