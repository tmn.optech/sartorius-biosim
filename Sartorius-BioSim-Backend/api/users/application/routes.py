from containers.handlers.user_handlers import UserContextHandlers, UserHandlers
from users.application.handlers import (
    AuthHandler,
)


def init_user_auth_routes(api, app):
    handler: AuthHandler = UserContextHandlers.auth_handler()
    app.add_url_rule(
        "/api/v1/auth/sign-in", "sign_in", handler.sign_in, methods=["POST"]
    )
    app.add_url_rule(
        "/api/v1/auth/sign-up", "sign_up", handler.sign_up, methods=["POST"]
    )
    app.add_url_rule(
        "/api/v1/auth/sign-out", "sign_out", handler.sign_out, methods=["POST"]
    )


def init_user_routes(api, app):
    handler: UserHandlers = UserContextHandlers.user_handler()
    app.add_url_rule(
        "/api/v1/user/get-all",
        "get_all_users",
        handler.get_all,
        methods=["GET"],
    )
    app.add_url_rule(
        "/api/v1/user/me",
        "get_log_in_user",
        handler.my_user,
        methods=["GET"],
    )
    app.add_url_rule(
        "/api/v1/user/", "create_user", handler.post, methods=["POST"]
    )
    app.add_url_rule(
        "/api/v1/user/<user_id>",
        "delete_user",
        handler.delete,
        methods=["DELETE"],
    )
