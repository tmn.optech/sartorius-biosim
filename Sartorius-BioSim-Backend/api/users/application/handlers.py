import flask
from flask import request
from werkzeug.exceptions import Unauthorized

from helpers.infrastructure import status
from helpers.infrastructure.model_serializer import ModelSerializer
from shared.response import Response
from users.application.decorators.auth_decorators import (
    jwt_requires,
    admin_permission_required,
)
from users.application.services import AuthService, UsersService
from users.application.validators import UserDataValidators


class AuthHandler:
    def __init__(
        self,
        auth_service: AuthService,
        user_service: UsersService,
        user_data_validators: UserDataValidators,
    ):
        self._internal_auth_service = auth_service
        self._user_service = user_service
        self._validators = user_data_validators

    def sign_in(self):
        """Endpoint for user authentication

        :return: web response with user token"""
        params = flask.request.json
        cleaned_data = self._validators.sign_in_validate(params)
        data = self._internal_auth_service.sign_in(cleaned_data)
        if not data:
            raise Unauthorized(
                description="Email and password does not match."
            )
        return Response(body=data, status_code=status.HTTP_200_OK).json

    def sign_out(self):
        """Endpoint for user sign out. Parse JWT token and add it to blacklist.

        :return: web response."""
        token = flask.request.headers.get("Authorization")
        print(flask.request.headers)
        self._internal_auth_service.sign_out(token)
        return Response(body={}, status_code=status.HTTP_200_OK).json

    def sign_up(self):
        """Endpoint for user registration

        :return: web response with registration status"""
        params = flask.request.json
        cleaned_data = self._validators.sign_up_validate(params)
        self._internal_auth_service.sign_up(cleaned_data)
        return Response(body={}, status_code=status.HTTP_201_CREATED).json


class UserHandlers:
    def __init__(
        self,
        user_service: UsersService,
        user_data_validators: UserDataValidators,
        serializer: ModelSerializer,
    ):
        self.__user_service = user_service
        self.__user_validators = user_data_validators
        self.__serializer = serializer

    @jwt_requires
    @admin_permission_required
    def get_all(self, *args, **kwargs):
        data = self.__user_service.get_all_users()
        serialized_data = self.__serializer.serialize_list(data)
        return Response(
            body=serialized_data, status_code=status.HTTP_200_OK
        ).json

    @jwt_requires
    def my_user(self, *args, **kwargs):
        user_id = request.user_id
        data = self.__user_service.get_user_by_id(user_id)
        serialized_data = self.__serializer.serialize(data)
        serialized_data.pop("password")
        return Response(
            body=serialized_data, status_code=status.HTTP_200_OK
        ).json

    @jwt_requires
    @admin_permission_required
    def post(self, *args, **kwargs):
        params = request.json
        cleaned_data = self.__user_validators.create_user_validate(params)
        data = self.__user_service.create_user(cleaned_data)
        serialized_data = self.__serializer.serialize(data)
        return Response(
            body=serialized_data, status_code=status.HTTP_201_CREATED
        ).json

    @jwt_requires
    @admin_permission_required
    def delete(self, *args, **kwargs):
        user_id = kwargs.get("user_id")
        self.__user_service.delete_user(user_id)
        return Response(body={}, status_code=status.HTTP_204_NO_CONTENT).json
