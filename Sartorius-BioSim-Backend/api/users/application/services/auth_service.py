from users.infrastructure.models import User, UserProfile
from users.infrastructure.repository import (
    UsersRepository,
    UsersProfileRepository,
)
from .token_provider import TokenProvider
from ...infrastructure.models.jwt_token import JwtToken
from ...infrastructure.repository.token_repository import JwtTokenRepository


class AuthService:
    """Class for describing all internal service_provider"""

    def __init__(
        self,
        token_provider: TokenProvider,
        user_repository: UsersRepository,
        profile_repository: UsersProfileRepository,
        token_repository: JwtTokenRepository,
    ):
        self.__token_provider = token_provider
        self.__repository = user_repository
        self.__profile_repository = profile_repository
        self.__token_repository = token_repository

    def generate_token(self, user: User) -> str:
        """Method for generating jwt using payload data

        :param user: UserDomain object which represent info about user from database
        :return: dict with token in data embedded object"""

        payload = {"user_id": user.user_id}
        return self.__token_provider.generate_token(payload)

    def sign_in(self, params: dict) -> dict:
        """Method for sign in into the system.

        :param params: username and password in dict representation
        :return: dictionary with token"""

        user_email = params["email"]
        user = self.__repository.get_one_by_email(email=user_email)
        if user and user.verify_password(params["password"]):
            return {"token": self.generate_token(user)}

    def sign_out(self, jwt_token: str) -> None:
        """Method for sign out the system.

        :param jwt_token: JWT token string.
        :return: dictionary with token"""
        token = JwtToken(token=jwt_token)
        self.__token_repository.save(token)

    def sign_up(self, params: dict) -> User:
        """Method for registration user into the system

        :param params: cleaned params from body of request
        :return: UserDomain entity"""

        user_data = {
            "email": params["email"],
            "password": params["password"],
            "is_admin": params.get("is_admin", False),
        }
        user = User.query.filter_by(email=params["email"]).first()
        if not user:
            user = User(**user_data)
            user.generate_password(params["password"])
            user = self.__repository.save(user)
        profile_data = {
            "user_id": user.id,
            "first_name": params["first_name"],
            "last_name": params["last_name"],
        }
        user_profile = UserProfile(**profile_data)
        self.__profile_repository.save(user_profile)
        return user
