import jwt
from datetime import datetime, timedelta
from werkzeug.exceptions import BadRequest
from flask import request
from containers.services.application_container import AppContextContainer


class TokenProvider:
    """
    Class to handle JWT token needs
    """

    def __init__(self):
        self.__logger = AppContextContainer.logger()

    secret = "sartorius"

    def generate_token(self, payload: dict):
        """Method for generating token using some payload

        :param payload: payload with some data
        :return: jwt"""

        default = {"exp": datetime.utcnow() + timedelta(hours=8)}
        payload.update(default)

        # TODO: secret should be based on some unique value of app instance (static for now)
        user_jwt = jwt.encode(payload=payload, key=self.secret)
        return user_jwt.decode("utf-8")

    def get_token_info(self, jwt_token: str) -> dict:
        """Method for decoding jwt_tokens with our secret key

        :param jwt_token: jwt_token with info we need to decode
        :return: dict payload with decode result"""

        if not jwt_token.startswith("Bearer "):
            self.__logger.error(
                "Token is not start with 'Bearer' word", request.environ
            )
            raise BadRequest(description="Token error")
        try:
            jwt_token = jwt_token.split(" ")[1]
            return jwt.decode(jwt_token, key=self.secret)
        except Exception as e:
            self.__logger.error(
                f"Error during decoding jwt token: {e.args}", request.environ
            )
            raise BadRequest(description=str(e))

    def get_token_payload(self, jwt_token: str, key: str) -> str:
        """Method for getting data from jwt_tokens

        :param jwt_token: jwt_token with info we need to decode
        :param key: key for getting from payload
        :return: dict payload with decode result"""

        try:
            token_data = jwt.decode(jwt_token, key=self.secret)
        except Exception as e:
            self.__logger.error(
                f"Error during decoding jwt token: {e.args}", request.environ
            )
            raise BadRequest(description="Token error")

        try:
            return token_data[key]
        except KeyError as e:
            self.__logger.error(
                f"Getting payload error: {e.args}", request.environ
            )
            raise BadRequest(description="Payload error.")
