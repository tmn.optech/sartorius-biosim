import uuid

from ...infrastructure.models import User
from ...infrastructure.repository import UsersRepository


class UsersService:
    """Class provides methods to work with users data."""

    def __init__(self, user_repository: UsersRepository):

        self.__user_repository = user_repository

    def get_all_users(self) -> [User]:
        """
        Get all users fom database, by user repository.
        """
        return self.__user_repository.get_all()

    def get_user_by_id(self, user_id: str) -> User:
        """
        Get user by id fom database, by user repository.
        """
        return self.__user_repository.get_one_by_id(user_id)

    def delete_user(self, user_id) -> None:
        """
        Delete user from database by user id.
        """
        self.__user_repository.delete(user_id)

    def create_user(self, data: dict) -> User:
        """
        Create new User from dictionary data.
        """
        user = User(id=uuid.uuid4(), **data)
        user.generate_password(data["password"])
        self.__user_repository.save(user)
        return user
