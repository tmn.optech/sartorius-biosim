import uuid

from sqlalchemy.dialects.postgresql import UUID
from helpers.infrastructure.base_repository import database as db


class JwtToken(db.Model):
    __tablename__ = "jwt_token"
    id = db.Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    token = db.Column(db.String(250), nullable=False)

    def __repr__(self):
        return f"<Token {self.token}"
