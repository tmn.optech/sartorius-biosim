import uuid

from werkzeug.security import check_password_hash, generate_password_hash
from sqlalchemy.dialects.postgresql import UUID
from helpers.infrastructure.base_repository import database as db


class User(db.Model):
    __tablename__ = "app_user"
    id = db.Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    email = db.Column(db.String(50), nullable=False)
    password = db.Column(db.String(200), nullable=False)
    is_admin = db.Column(db.Boolean, default=False, server_default="false")

    def __repr__(self):
        return f"<User {self.id}"

    def generate_password(self, pwd: str) -> str:
        self.password = generate_password_hash(pwd)
        return self.password

    def verify_password(self, pwd: str) -> bool:
        return check_password_hash(self.password, pwd)

    @property
    def user_id(self) -> str:
        return str(self.id)
