from sqlalchemy import ForeignKey
from sqlalchemy.dialects.postgresql import UUID
from helpers.infrastructure.base_repository import database as db


class UserProfile(db.Model):
    __tablename__ = "profile"
    user_id = db.Column(
        UUID(as_uuid=True), ForeignKey("app_user.id"), primary_key=True
    )
    first_name = db.Column(db.String(50), nullable=False)
    last_name = db.Column(db.String(50), nullable=False)

    def __repr__(self):
        return f"<Profile {self.id}"
