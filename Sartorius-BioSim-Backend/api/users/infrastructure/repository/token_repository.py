from helpers.infrastructure.base_repository import BaseRepository
from users.infrastructure.models.jwt_token import JwtToken


class JwtTokenRepository(BaseRepository):
    """Class with describing communication with users table"""

    @staticmethod
    def get_all() -> [JwtToken]:
        """Method for getting all jwt tokens from db

        :return: list of JwtToken instances"""

        tokens = JwtToken.query.all()
        return tokens

    @staticmethod
    def get_token(token) -> JwtToken:
        """Method for getting all jwt tokens from db

        :return: list of JwtToken instances"""

        token = JwtToken.query.filter_by(token=token).first()
        return token

    def save(self, token: JwtToken) -> None:
        """Method for adding jwt token to jwt token blacklist table.

        :param token: token entity from domain level
        :return: None"""
        self.db.session.add(token)
        try:
            self.db.session.commit()
        except Exception:
            self.db.session.rollback()
