from typing import Union

from helpers.infrastructure.base_repository import BaseRepository
from users.infrastructure.models import UserProfile


class UsersProfileRepository(BaseRepository):
    """Class with describing communication with users table"""

    @staticmethod
    def get_all() -> [UserProfile]:
        """Method for getting all users from db

        :return: list of User instances"""

        user_profiles = UserProfile.query.all()
        return user_profiles

    @staticmethod
    def get_one_by_id(user_id: str) -> Union[UserProfile, None]:
        """Method for getting user profile from db by id

        :param user_id: primary key in user table
        :return: User instance"""

        user_profile = UserProfile.query.filter_by(id=user_id).first()
        return user_profile

    def save(self, user_profile: UserProfile) -> None:
        """Method for adding user profile in user_profile table

        :param user_profile: User profile entity from domain level
        :return: None"""
        self.db.session.add(user_profile)
        try:
            self.db.session.commit()
        except Exception:
            self.db.session.rollback()

    def delete(self, user_id: str) -> None:
        """Method to delete user profile from database"""
        try:
            UserProfile.query.filter_by(user_id=user_id).delete()
            self.db.session.commit()
        except Exception as e:
            self.db.session.rollback()
            raise e
