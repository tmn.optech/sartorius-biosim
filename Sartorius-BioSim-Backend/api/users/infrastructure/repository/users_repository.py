from typing import Union

from helpers.infrastructure.base_repository import BaseRepository
from users.domain.user_id import UserID
from users.infrastructure.models import User


class UsersRepository(BaseRepository):
    """Class with describing communication with users table"""

    def get_all(self) -> [User]:
        """Method for getting all users from db

        :return: list of User instances"""

        users = User.query.all()
        return users

    def get_one_by_email(self, email: str) -> Union[User, None]:
        """Method for getting user by email from db

        :param email: email with domain and @ symbol
        :return: User instance"""
        user = User.query.filter_by(email=email).first()
        return user

    def get_one_by_id(self, user_id: UserID) -> Union[User, None]:
        """Method for getting user from db by id

        :param user_id: primary key in user table
        :return: User instance"""

        user = User.query.filter_by(id=user_id.base_58).first()
        return user

    def save(self, user: User) -> User:
        """Method for adding user in user table

        :param user: User entity from domain level
        :return: None"""
        self.db.session.add(user)
        try:
            self.db.session.commit()
        except Exception:
            self.db.session.rollback()
        return user

    def delete(self, user_id: str) -> None:
        """Method to delete user from database"""
        try:
            User.query.filter_by(id=user_id).delete()
            self.db.session.commit()
        except Exception as e:
            self.db.session.rollback()
            raise e
