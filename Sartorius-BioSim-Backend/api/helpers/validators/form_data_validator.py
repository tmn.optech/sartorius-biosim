from wtforms import Form
from werkzeug.datastructures import ImmutableMultiDict


class MultipartDataValidator(Form):
    def __init__(self, data, files, *args, **kwargs):
        data_dict = data.to_dict()
        data_dict.update(files.to_dict())
        merged_data = ImmutableMultiDict(data_dict)
        super().__init__(merged_data, *args, **kwargs)

    @property
    def errors_dict(self):
        return {field: errors for (field, errors) in self.errors.items()}
