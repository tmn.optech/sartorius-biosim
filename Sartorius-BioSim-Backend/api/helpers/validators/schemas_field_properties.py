from collections import namedtuple

Email = namedtuple("Email", ("type", "maxLength", "format", "pattern"))
max_length = 32
email_pattern = r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?"

email_property = Email("string", max_length, "email", email_pattern)


Password = namedtuple("Password", ("type", "maxLength"))
max_length = 32
password_property = Password("string", max_length)
