import jsonschema

from werkzeug.exceptions import BadRequest
from jsonschema.exceptions import _Error


class RequestValidator:
    def __init__(self, schema: dict, params: dict):
        self.__errors = None
        self.__valid_params = None
        self.__schema = schema
        self.__params = params
        self.__is_valid = False
        self.__is_validated = False

    def is_valid(self, raise_exception: bool = False):
        """Method for validating params using schema

        :param raise_exception: boolean param. Use True to raise error if
                params are not valid. User False to return False boolean
                value without raising error if params are not valid.
        :return: (True or False) if raise_err False
                or raise error if params are not valid."""

        try:
            jsonschema.validate(
                schema=self.__schema,
                instance=self.__params,
                format_checker=jsonschema.FormatChecker(),
            )
            self.__is_valid = self.__is_validated = True
        except _Error as e:
            self.__errors = f"Validation error: {e.message}"
            if raise_exception:
                raise BadRequest(description=self.__errors)

        return self.__is_valid

    @property
    def validated_data(self):
        """Property for getting clean params after validation. Error will be raise if no validation was made before.

        :return: clean params after validation"""

        if self.__is_validated:
            return self.__params
        else:
            raise Exception("You should validate data before getting it.")

    @property
    def errors(self):
        return self.__errors
