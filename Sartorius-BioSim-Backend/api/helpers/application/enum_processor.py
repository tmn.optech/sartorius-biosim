from enum import Enum
from typing import Type, TypeVar

T = TypeVar("T")


def get_names(enumerator: Enum) -> [str]:
    """Method for getting list of names from enumerator

    :param enumerator: enumerator object
    :return: list of names from enumerator"""

    return [item.name for item in enumerator]


def get_values(enumerator: Enum) -> [str]:
    """Method for getting list of values from enumerator

    :param enumerator: enumerator object
    :return: list of values from enumerator"""

    return [item.value for item in enumerator]


def get_enum(name: str, enumerator: Type[Enum]) -> Type[T]:
    """Method for getting enum key, value pair using name of enum and Enum.
    Name of enum should be part of enum.

    :param name: name from specified enum
    :param enumerator: enumerator for enumeration for search
    :return: enum pair like name = value"""

    items = [item for item in enumerator if item.name == name]
    return items[0] if len(items) else None
