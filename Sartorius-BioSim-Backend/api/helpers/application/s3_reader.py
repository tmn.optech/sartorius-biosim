"""
 Class for processing files from s3 and convert them to specific data format.
"""
import pandas as pd
import numpy as np
from io import BytesIO


class DataHandler:
    """
    Remote storage data handler. Process uploading and downloading files.
    """

    def __init__(self, s3_object):
        self.content_type = s3_object["ContentType"]
        self.file_content = s3_object["Body"].read()
        self.__supported_methods = {
            "text/csv": pd.read_csv,
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet": pd.read_excel,
        }
        self.__reader = self.__supported_methods.get(self.content_type)

    @property
    def data(self) -> pd.DataFrame:
        """
        Convert s3 bites data Pandas data set.
        """
        if not self.__reader:
            raise AttributeError("File type is not supported")
        content = BytesIO(self.file_content)
        return self.__reader(content).replace(np.nan, "")

    @property
    def bytes_file_content(self) -> BytesIO:
        """
        Get file content as BytesIO object.
        """
        return BytesIO(self.file_content)

    def update_from_dict(self, dict_to_update: dict):
        """
        Update file data from dictionary.
        """
        data_set = pd.DataFrame.from_dict(dict_to_update)
        if self.content_type == "text/csv":
            self.file_content = data_set.to_csv(index=False).encode()
        else:
            self.convert_to_excel(data_set)

    def convert_to_excel(self, data_frame: pd.DataFrame):
        """
        Specific method to convert pandas data frame to excel.
        """
        output = BytesIO()
        writer = pd.ExcelWriter(output, engine="xlsxwriter")
        data_frame.to_excel(writer, index=False)
        writer.save()
        output.seek(0)
        self.file_content = output.read()
