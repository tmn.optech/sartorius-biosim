from enum import Enum

from sqlalchemy.inspection import inspect


class ModelSerializer:
    @staticmethod
    def serialize(model, convert_enum=True):
        serialized_data = {}
        for column in inspect(model).attrs.keys():
            value = getattr(model, column)
            if isinstance(value, Enum):
                serialized_data[column] = (
                    value.value if convert_enum else value.name
                )
            else:
                serialized_data[column] = value
        return serialized_data

    def serialize_list(self, qs, convert_enum=True):
        return [self.serialize(m, convert_enum) for m in qs]
