from flask_sqlalchemy import SQLAlchemy


database = SQLAlchemy()


class BaseRepository:
    db = database
