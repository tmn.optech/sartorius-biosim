import base58

from simpleflake import simpleflake

from containers.services.application_container import AppContextContainer


class BaseID:
    def __init__(self, id: str = None):
        self.__logger = AppContextContainer.logger()
        if not id:
            self.__id = simpleflake()
            self.__bytes = self.__id.to_bytes(length=8, byteorder="big")
            self.__base_58 = None
        else:
            self.__base_58 = id

    @property
    def base_58(self) -> str:
        """Property for getting base58 representing of id in string"""

        if not self.__base_58:
            self.__base_58 = base58.b58encode(self.__bytes).decode()
        return self.__base_58


if __name__ == "__main__":
    base_id_1 = BaseID()
    print(base_id_1.base_58)
    base_id_2 = BaseID(base_id_1.base_58)
    print(base_id_2.base_58)
