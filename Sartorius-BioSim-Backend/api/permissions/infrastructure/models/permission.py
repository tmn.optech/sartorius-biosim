import uuid

from sqlalchemy.dialects.postgresql import UUID
from helpers.infrastructure.base_repository import database as db


class Permission(db.Model):
    __tablename__ = "permissions"
    id = db.Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    name = db.Column(db.String(50), unique=True)


class RolesPermissions(db.Model):
    __tablename__ = "roles_permissions"
    id = db.Column(db.Integer(), primary_key=True)
    permission_id = db.Column(
        UUID(as_uuid=True), db.ForeignKey("permissions.id"), ondelete="CASCADE"
    )
    role_id = db.Column(
        UUID(as_uuid=True), db.ForeignKey("roles.id"), ondelete="CASCADE"
    )
