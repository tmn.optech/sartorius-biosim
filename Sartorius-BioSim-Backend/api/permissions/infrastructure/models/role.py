import uuid

from sqlalchemy.dialects.postgresql import UUID
from helpers.infrastructure.base_repository import database as db


class Role(db.Model):
    __tablename__ = "roles"
    id = db.Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    name = db.Column(db.String(50), unique=True)


class UserRoles(db.Model):
    __tablename__ = "user_roles"
    id = db.Column(db.Integer(), primary_key=True)
    user_id = db.Column(
        UUID(as_uuid=True), db.ForeignKey("app_user.id"), ondelete="CASCADE"
    )
    role_id = db.Column(
        UUID(as_uuid=True), db.ForeignKey("roles.id"), ondelete="CASCADE"
    )
