from helpers.domain.base_id import BaseID


class PermissionID(BaseID):
    """Entity for representing id of user entity"""
