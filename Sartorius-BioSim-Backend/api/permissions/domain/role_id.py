from helpers.domain.base_id import BaseID


class RoleID(BaseID):
    """Entity for representing id of user entity"""
