from datetime import datetime
from functools import wraps
from typing import Callable

from projects.domain.enums.creating_step import RenderStatus
from projects.infrastructure.models import Project


def update_render_status(method: Callable) -> Callable:
    """
    Updates project render status depended on result of executing decorated method.
    Track project render starts and finish time in case if error raised.
    """

    @wraps(method)
    def wrapper(self, project: Project, *args, **kwargs):
        try:
            self._project_service.update_project(
                project.project_id,
                {
                    "render_status": RenderStatus.RENDERING.name,
                    "render_started": datetime.now(),
                },
            )
            return method(self, project, *args, **kwargs)
        except Exception as e:
            self._project_service.update_project(
                project.project_id,
                {
                    "render_status": RenderStatus.FAILED.name,
                    "render_finished": datetime.now(),
                },
            )
            raise e

    return wrapper
