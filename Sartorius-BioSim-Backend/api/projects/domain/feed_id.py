from helpers.domain.base_id import BaseID


class FeedID(BaseID):
    """Entity for representing id of fit parameter entity"""
