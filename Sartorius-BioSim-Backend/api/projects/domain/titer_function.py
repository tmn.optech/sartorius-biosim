from typing import Union


class TiterFunction:
    """
    Represents titer function object
    """

    def __init__(self, titer_function: str):
        self.function_text = titer_function

    def as_object(
        self, glob: Union[dict, None] = None, loc: Union[dict, None] = None
    ):
        loc = loc if loc else {}
        glob = glob if glob else {}
        exec(self.function_text, glob, loc)
        if loc:
            return list(loc.values())[0]
