from enum import Enum, unique


@unique
class ParameterRole(Enum):
    GROWTH_PARAMETER = "Growth parameter"
    INDEPENDENT = "Independent"
    METABOLITE = "Metabolite"
    BUILT_IN = "Built-in"
    USER_DEFINED = "User-defined"


@unique
class ParameterType(Enum):
    QUADRATIC_FACTOR = "Quadratic factor"
    SUBSTRATE_FACTOR = "Substrate factor"
    INHIBITOR_FACTOR = "Inhibitor factor"


@unique
class ObjectivePenalty(Enum):
    SQUARED_ERROR = "SQUARED"
    AUTO_CORRELATION = "SSE"
    AUTOCOV = "AUTOCOV"


@unique
class DateType(Enum):
    DAYS = "Days"
    HOURS = "Hours"


@unique
class ViableCellDensityType(Enum):
    E_SIX = "1e6 cells/ml"
    E_FIVE = "1e5 cells/ml"
    NORMAL = "cells/ml"


@unique
class DataSource(Enum):
    LYSED_CELLS = "Lysed cells"
    BIOMATERIAL = "Biomaterial"
