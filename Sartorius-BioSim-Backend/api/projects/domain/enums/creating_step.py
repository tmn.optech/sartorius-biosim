from enum import Enum, unique


@unique
class CreatingStep(Enum):
    DATA_UPLOAD = "Data upload"
    MODEL_CONFIGURATION = "Model configuration"
    METABOLITE_CONSUMPTION = "Metabolite consumption"
    GROWTH_KINETICS = "Growth kinetics"
    METABOLITE_FITTING = "Metabolite fitting"
    TITER_FITTING = "Titer fitting"


@unique
class RenderStatus(Enum):
    NOT_STARTED = "Not started"
    RENDERING = "Rendering"
    COMPLETED = "Completed"
    FAILED = "Failed"
