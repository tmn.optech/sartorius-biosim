from enum import Enum, unique


@unique
class ProjectType(Enum):
    DIGITAL_TWIN = "digital twin"
    CLONE_SELECTION = "clone selection"
