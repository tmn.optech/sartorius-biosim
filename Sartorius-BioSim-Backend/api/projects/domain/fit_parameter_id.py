from helpers.domain.base_id import BaseID


class FitParameterID(BaseID):
    """Entity for representing id of fit parameter entity"""
