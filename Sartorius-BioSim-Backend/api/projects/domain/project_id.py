from helpers.domain.base_id import BaseID


class ProjectID(BaseID):
    """Entity for representing id of project entity"""
