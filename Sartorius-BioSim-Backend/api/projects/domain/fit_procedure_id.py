from helpers.domain.base_id import BaseID


class FitProcedureID(BaseID):
    """Entity for representing id of fit procedure entity"""
