from containers.handlers import ProjectContextHandlers
from projects.application.handlers import (
    ProjectHandler,
    FitParametersHandler,
    FitProcedureHandler,
    ProjectSettingsHandler,
    MetaboliteConsumptionsRateHandler,
    FeedHandler,
)


def init_project_routes(api, app):
    handler: ProjectHandler = ProjectContextHandlers.project_handler()
    app.add_url_rule(
        "/api/v1/projects",
        "all-projects",
        handler.get_all_users_projects,
        methods=["GET"],
    )
    app.add_url_rule(
        "/api/v1/projects/users-projects",
        "users-projects",
        handler.get_projects_by_user_id,
        methods=["GET"],
    )
    app.add_url_rule(
        "/api/v1/projects", "create-project", handler.post, methods=["POST"]
    )
    app.add_url_rule(
        "/api/v1/projects/<project_id>",
        "update-project",
        handler.patch,
        methods=["PATCH"],
    )
    app.add_url_rule(
        "/api/v1/projects/<project_id>",
        "delete-project",
        handler.delete,
        methods=["DELETE"],
    )

    app.add_url_rule(
        "/api/v1/projects/<project_id>/change-owner",
        "change-project-owner",
        handler.change_project_owner,
        methods=["POST"],
    )

    app.add_url_rule(
        "/api/v1/projects/<project_id>/initial-data",
        "initial-data",
        handler.get_initial_file,
        methods=["GET"],
    )
    app.add_url_rule(
        "/api/v1/projects/<project_id>/initial-data",
        "update-initial-data",
        handler.update_initial_data,
        methods=["PATCH"],
    )
    app.add_url_rule(
        "/api/v1/projects/<project_id>/fit",
        "fit-project",
        handler.fit_model,
        methods=["POST"],
    )
    app.add_url_rule(
        "/api/v1/projects/<project_id>/digital-twin/fit",
        "fit-digital-twin-project",
        handler.fit_digital_twin_model,
        methods=["POST"],
    )
    app.add_url_rule(
        "/api/v1/projects/rendering",
        "rendering-projects",
        handler.get_rendering_projects,
        methods=["GET"],
    )


def init_project_settings_routes(api, app):
    handler: ProjectSettingsHandler = (
        ProjectContextHandlers.project_settings_handler()
    )
    app.add_url_rule(
        "/api/v1/project/<project_id>/project-settings",
        "project-settings",
        handler.get_project_settings_by_id,
        methods=["GET"],
    )
    app.add_url_rule(
        "/api/v1/project/project-settings",
        "create-project-settings",
        handler.post,
        methods=["POST"],
    )

    app.add_url_rule(
        "/api/v1/project/<project_id>/project-settings",
        "update-project-settings",
        handler.patch,
        methods=["PATCH"],
    )


def init_fit_parameter_routes(api, app):
    handler: FitParametersHandler = (
        ProjectContextHandlers.fit_parameter_handler()
    )
    app.add_url_rule(
        "/api/v1/projects/<project_id>/fit-parameters",
        "project-parameters",
        handler.get_fit_parameters_by_project_id,
        methods=["GET"],
    )
    app.add_url_rule(
        "/api/v1/fit-parameter",
        "create-parameter",
        handler.post,
        methods=["POST"],
    )
    app.add_url_rule(
        "/api/v1/fit-parameters",
        "create-parameters",
        handler.bulk_create,
        methods=["POST"],
    )
    app.add_url_rule(
        "/api/v1/fit-parameters",
        "update-parameters",
        handler.bulk_update,
        methods=["PATCH"],
    )
    app.add_url_rule(
        "/api/v1/fit-parameter/<fit_parameter_id>",
        "delete-parameter",
        handler.delete,
        methods=["DELETE"],
    )


def init_fit_procedure_routes(api, app):
    handler: FitProcedureHandler = (
        ProjectContextHandlers.fit_procedure_handler()
    )
    app.add_url_rule(
        "/api/v1/projects/<project_id>/fit-procedures",
        "project-procedures",
        handler.get_fit_procedures_by_project_id,
        methods=["GET"],
    )
    app.add_url_rule(
        "/api/v1/fit-procedure",
        "create-procedure",
        handler.post,
        methods=["POST"],
    )
    app.add_url_rule(
        "/api/v1/fit-procedures",
        "create-procedures",
        handler.bulk_create,
        methods=["POST"],
    )
    app.add_url_rule(
        "/api/v1/fit-procedures",
        "update-procedures",
        handler.bulk_update,
        methods=["PATCH"],
    )
    app.add_url_rule(
        "/api/v1/fit-procedure/<fit_procedure_id>",
        "delete-procedure",
        handler.delete,
        methods=["DELETE"],
    )


def init_metabolite_rates_urls(app):
    handler: MetaboliteConsumptionsRateHandler = (
        ProjectContextHandlers.metabolite_handler()
    )
    app.add_url_rule(
        "/api/v1/projects/<project_id>/metabolite_rates",
        "metabolite-rates",
        handler.get_metabolite_consumptions_rates,
        methods=["GET"],
    )
    app.add_url_rule(
        "/api/v1/projects/<project_id>/metabolite_rates",
        "calculate-metabolite-rates",
        handler.recalculate_metabolite_consumptions_rate,
        methods=["POST"],
    )


def init_project_feed_urls(app):
    handler: FeedHandler = ProjectContextHandlers.feed_handler()
    app.add_url_rule(
        "/api/v1/projects/<project_id>/feeds/bulk",
        "create-feeds",
        handler.bulk_create,
        methods=["POST"],
    )
    app.add_url_rule(
        "/api/v1/projects/<project_id>/feeds",
        "get-feeds",
        handler.get_feeds_by_project_id,
        methods=["GET"],
    )
    app.add_url_rule(
        "/api/v1/projects/<project_id>/feeds",
        "update-feeds",
        handler.bulk_update,
        methods=["PATCH"],
    )
    app.add_url_rule(
        "/api/v1/projects/<project_id>/feeds/<feed_id>",
        "delete-feed",
        handler.delete,
        methods=["DELETE"],
    )
