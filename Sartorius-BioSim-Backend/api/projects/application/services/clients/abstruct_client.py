from abc import abstractmethod, ABC


class BaseStorageClient(ABC):
    @abstractmethod
    def read(self, *args, **kwargs):
        pass

    @abstractmethod
    def delete(self, *args, **kwargs):
        pass

    @abstractmethod
    def upload(self, *args, **kwargs):
        pass
