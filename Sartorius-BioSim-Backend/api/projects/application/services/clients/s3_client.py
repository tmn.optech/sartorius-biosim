from io import BytesIO

import boto3
from flask import current_app, request

from containers.services.application_container import AppContextContainer
from middlewares.error_types import RemoteStorageError
from projects.application.services.clients.abstruct_client import (
    BaseStorageClient,
)

logger = AppContextContainer.logger()


class AwsS3Client(BaseStorageClient):
    """
    Client to work with remote AWS S3 storage files.
    """

    def read(self, bucket: str, path: str) -> dict:
        s3 = boto3.client(
            "s3", **current_app.config.get("AWS_STORAGE_SETTINGS", {})
        )
        try:
            logger.context_info(
                f"Reading {path} from {bucket} bucket.", request.environ
            )
            obj = s3.get_object(Bucket=bucket, Key=path)
            return obj
        except Exception as e:
            error_message = (
                f"Unable to read {path} from {bucket} bucket: {e} ({type(e)})"
            )
            logger.error(
                error_message,
                request.environ,
            )
            raise RemoteStorageError(error_message)

    def delete(self):
        pass

    def upload(
        self,
        bucket: str,
        path: str,
        file_bytes: BytesIO,
        content_type: str,
        acl="public-read",
    ) -> bool:
        s3 = boto3.client(
            "s3", **current_app.config.get("AWS_STORAGE_SETTINGS", {})
        )
        try:
            logger.context_info(
                f"Uploading {path} to {bucket} bucket.", request.environ
            )
            s3.upload_fileobj(
                file_bytes,
                bucket,
                path,
                ExtraArgs={"ACL": acl, "ContentType": content_type},
            )
            logger.context_info(
                f"Finished uploading {path} to {bucket} bucket.",
                request.environ,
            )
        except Exception as e:
            error_message = (
                f"Unable upload {path} to {bucket} bucket: {e} ({type(e)})"
            )
            logger.error(
                error_message,
                request.environ,
            )
            raise RemoteStorageError(error_message)
        return True
