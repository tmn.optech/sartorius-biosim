from projects.infrastructure.models import Feed
from projects.domain.project_id import ProjectID
from projects.infrastructure.repository.feed_repository import (
    FeedRepository,
)


class FeedService:
    """Class provides methods to work with project feeds data."""

    def __init__(
        self,
        feed_repository: FeedRepository,
    ):
        self.__feed_repository = feed_repository

    def create_feed(self, data: dict) -> Feed:
        """
        Using request cleaned data, create new project's feed.
        :param data: request cleaned data.
        :return: feed.
        """
        feed = Feed(**data)
        self.__feed_repository.save(feed)
        return feed

    def delete_feed(self, feed_id):
        """
        Delete specific feed from database.
        :param feed_id: requested feed id to delete.
        """
        self.__feed_repository.delete(feed_id)

    def get_feeds_by_project_id(self, project_id: ProjectID) -> [Feed]:
        """
        Using request cleaned data, get all project's feeds.
        :param project_id: id of current processed project.
        :return: dict with all data data connected to this project.
        """
        result = self.__feed_repository.get_all_by_project_id(project_id)
        return result

    def bulk_create_feeds(self, bulk_list: [dict]) -> [Feed]:
        """
        Using request cleaned data, create new project's feeds.
        :param bulk_list: request cleaned data.
        :return: list of created feeds.
        """
        data_to_create = [Feed(**data) for data in bulk_list]
        self.__feed_repository.bulk_create(data_to_create)
        return data_to_create

    def bulk_update_feeds(self, bulk_list: [dict]) -> [Feed]:
        """
        Using request cleaned data, update existing feeds.
        :param bulk_list: request cleaned data.
        :return: list of updated feeds.
        """
        self.__feed_repository.bulk_update(bulk_list)
        return bulk_list

    def get_feed_composition(self, project_id: ProjectID) -> [dict]:
        """
        Return formatted feed composition data for fitting
        """

        feeds = self.__feed_repository.get_all_by_project_id(project_id)
        result = [feed.as_dict for feed in feeds]
        return result  # TODO: return required formatted format for feed composition
