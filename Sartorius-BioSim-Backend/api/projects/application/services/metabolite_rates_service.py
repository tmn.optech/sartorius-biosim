from containers.services.application_container import AppContextContainer
from helpers.infrastructure.model_serializer import ModelSerializer
from middlewares.error_types import SartoriusWrappersError
from projects.application.services import RemoteStorageService, ProjectService
from projects.application.services.feed_service import FeedService
from projects.application.services.model_fitting_service import (
    ModelFittingService,
)
from projects.application.services.project_settings_service import (
    ProjectSettingsService,
)
from sartorius_wrappers import calculate_metabolic_rates

logger = AppContextContainer.logger()


class MetaboliteRatesService:
    """
    Service providing all metabolite consumption rates methods.
    """

    def __init__(
        self,
        serializer: ModelSerializer,
        project_data_service: ProjectService,
        remote_storage: RemoteStorageService,
        project_settings_service: ProjectSettingsService,
        feeds_service: FeedService,
        fitting_service: ModelFittingService,
    ):
        self.__serializer = serializer
        self.__project_service = project_data_service
        self.__remote_storage = remote_storage
        self.__project_settings_service = project_settings_service
        self.__feeds_service = feeds_service
        self.__fitting_service = fitting_service

    def get_metabolite_consumption_rates(self, project_id: str) -> dict:
        """
        Get or calculate Calculate metabolite consumption rates for specific project.
        """
        project = self.__project_service.get_project_by_id(project_id)
        metabolite_rates = project.metabolite_consumption_rates
        if not metabolite_rates:
            initial_data = self.__project_service.get_initial_file_data(
                project_id
            )
            metabolite_rates = self.calculate_consumptions_rates(
                project_id, initial_data
            )
        return metabolite_rates

    def calculate_consumptions_rates(
        self, project_id: str, initial_data: dict
    ) -> dict:
        """
        Calculate existed metabolite rates. To calculate rates method use
        list of fit parameters, witch are 'Metabolite' type,
        feeds for the specific project,
        dict of initial data.
        """
        project = self.__project_service.get_project_by_id(project_id)
        project_parameters = (
            self.__project_settings_service.get_parameters_by_project_id(
                project.id
            )
        )
        fit_config, _ = self.__fitting_service.get_fitting_config(
            project, initial_data
        )
        parameters = [
            fit_parameter.dict_config for fit_parameter in project_parameters
        ]
        metabolites = list(
            {
                parameter["name"]
                for parameter in parameters
                if parameter["role"] == "Metabolite"
            }
        )
        feeds = self.__feeds_service.get_feeds_by_project_id(project_id)
        serialized_feeds = [feed.as_dict for feed in feeds]
        try:
            (
                metabolic_rates,
                average_metabolite_consumption,
            ) = calculate_metabolic_rates(
                initial_data,
                metabolites,
                project.columns_map,
                fit_config,
                serialized_feeds,
            )
        except Exception as e:
            raise SartoriusWrappersError(str(e))

        metabolite_consumption_rate = {
            "metabolic_rates": metabolic_rates,
            "average_metabolite_consumption": average_metabolite_consumption,
        }
        self.__project_service.update_project(
            project_id,
            {"metabolite_consumption_rates": metabolite_consumption_rate},
        )
        return metabolite_consumption_rate
