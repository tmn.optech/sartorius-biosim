import uuid
from io import BytesIO

from flask import request
from flask import current_app

from helpers.application.s3_reader import DataHandler
from middlewares.error_types import DoesNotExist
from projects.application.services.remote_storage_service import (
    RemoteStorageService,
)
from projects.application.validators import ProjectsFieldsForm
from projects.domain.enums.creating_step import RenderStatus
from projects.domain.project_id import ProjectID
from projects.infrastructure.models import Project
from projects.infrastructure.repository import ProjectRepository
from users.domain.user_id import UserID


class ProjectService:
    """Class provides methods to work with project data."""

    def __init__(
        self,
        project_repository: ProjectRepository,
        remote_storage: RemoteStorageService,
    ):
        self.__project_repository = project_repository
        self.__remote_storage = remote_storage

    def create_project(
        self, user_id: UserID, form: ProjectsFieldsForm
    ) -> Project:
        """
        Using request cleaned data, create new users project.
        :param user_id: current user id.
        :param form: project form with cleaned data.
        :return: dict with created data.
        """
        # Create new project
        project = Project(
            id=uuid.uuid4(),
            user=user_id.base_58,
            type=form.type.data,
            name=form.name.data,
            initial_data=form.initial_file.data.filename,
        )

        # Get file content and content type
        initial_file = request.files["initial_file"]
        bytes_stream = initial_file.stream.read()
        file_content = BytesIO(bytes_stream)

        self.__remote_storage.upload_file(
            current_app.config.get("STORAGE_TYPE"),
            current_app.config.get("BUCKET_NAME"),
            project.initial_file_path,
            file_content,
            initial_file.content_type,
        )
        self.__project_repository.save(project)
        return project

    def update_project(self, project_id: ProjectID, data: dict) -> None:
        """
        Using request cleaned data, update users project.
        :param project_id: id of updated project.
        :param data: dict with new data for update.
        :return: None.
        """

        self.__project_repository.update(project_id, data)

    def delete_project(self, project_id) -> None:
        """
        Delete user project.
        :param project_id: id of project is going to be deleted.
        :return: None
        """
        self.__project_repository.delete(project_id)

    def get_user_projects(self, user_id: UserID) -> [Project]:
        """
        Using request cleaned data, get all user's projects.
        :param user_id: id of current processed user.
        :return: dict with all data data connected to this user.
        """
        result = self.__project_repository.get_all_by_user_id(user_id)
        return result

    def get_project_by_id(self, project_id: ProjectID) -> Project:
        """
        Using request cleaned data, get projects by id.
        :param project_id: id of current project.
        :return: dict with all data data connected to this project.
        """
        result = self.__project_repository.get_one_by_id(project_id)
        return result

    def get_all_projects(self) -> [Project]:
        """
        Using request cleaned data, get all exiting projects.
        :return: dict with all projects data.
        """
        result = self.__project_repository.get_all()
        return result

    def get_rendering_projects(self) -> [Project]:
        """
        Using request cleaned data, get all rendering projects.
        :return: list with project_ids.
        """
        projects = self.__project_repository.get_filtered(
            {"render_status": RenderStatus.RENDERING.name}
        )
        result = [project.id for project in projects]
        return result

    def get_initial_file_data(self, project_id) -> dict:
        """
        Using request cleaned data, get initial file project data.
        :return: dict with all projects data.
        """
        bucket_name = current_app.config.get("BUCKET_NAME")
        bucket_type = current_app.config.get("STORAGE_TYPE")
        project = self.__project_repository.get_one_by_id(project_id)
        if not project:
            raise DoesNotExist("Project does not exist")
        path = project.initial_file_path
        result = self.__remote_storage.get_file(bucket_type, bucket_name, path)
        rows = DataHandler(result).data.to_dict()
        return rows

    def update_initial_file_data(
        self, project_id, data_to_update: dict
    ) -> dict:
        """
        Using request cleaned data, get all exiting projects.
        :return: dict with all projects data.
        """
        bucket_type = current_app.config.get("STORAGE_TYPE")
        bucket_name = current_app.config.get("BUCKET_NAME")
        project = self.__project_repository.get_one_by_id(project_id)
        path = project.initial_file_path
        result = self.__remote_storage.get_file(bucket_type, bucket_name, path)
        data_handler = DataHandler(result)
        data_handler.update_from_dict(data_to_update)
        self.__remote_storage.upload_file(
            bucket_type,
            bucket_name,
            path,
            data_handler.bytes_file_content,
            data_handler.content_type,
        )
        return data_handler.data.to_dict()
