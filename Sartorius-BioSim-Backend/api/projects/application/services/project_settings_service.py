from projects.infrastructure.models import (
    FitProcedure,
    FitParameter,
    ProjectSettings,
)
from projects.infrastructure.repository import (
    FitParameterRepository,
    FitProcedureRepository,
    ProjectSettingsRepository,
)
from projects.domain.project_id import ProjectID


class ProjectSettingsService:
    """Class provides methods to work with project settings data."""

    def __init__(
        self,
        project_settings_repository: ProjectSettingsRepository,
        fit_parameters_repository: FitParameterRepository,
        fit_procedure_repository: FitProcedureRepository,
    ):
        self.__project_settings_repository = project_settings_repository
        self.__fit_parameters_repository = fit_parameters_repository
        self.__fit_procedure_repository = fit_procedure_repository

    def create_project_settings(self, data) -> ProjectSettings:
        """
        Using request cleaned data, create new project's settings.
        :param data: request cleaned data.
        :return: project settings.
        """
        project_settings = ProjectSettings(**data)
        self.__project_settings_repository.save(project_settings)
        return project_settings

    def update_project_settings(self, project_id, data: dict) -> None:
        """
        Using request cleaned data, update users project.
        :param project_id: id of updated project.
        :param data: dict
        :return: dict with created data.
        """

        self.__project_settings_repository.update(project_id, data)

    def get_project_settings_by_id(self, project_id) -> ProjectSettings:
        """
        Return project settings by project id.
        :param project_id: project unique identity.
        :return: project settings.
        """
        project_settings = self.__project_settings_repository.get_one_by_id(
            project_id
        )
        return project_settings

    def create_fit_parameter(self, data: dict) -> FitParameter:
        """
        Using request cleaned data, create new project's fit parameter.
        :param data: request cleaned data.
        :return: fit parameter.
        """
        parameter = FitParameter(**data)
        self.__fit_parameters_repository.save(parameter)
        return parameter

    def create_fit_procedure(self, data: dict) -> FitProcedure:
        """
        Using request cleaned data, create new project's fit procedure.
        :param data: request cleaned data.
        :return: fit procedure.
        """
        parameter = FitProcedure(**data)
        self.__fit_procedure_repository.save(parameter)
        return parameter

    def delete_fit_parameter(self, parameter_id):
        """
        Delete specific fit parameter from database.
        :param parameter_id: requested parameter id to delete.
        """
        self.__fit_parameters_repository.delete(parameter_id)

    def delete_fit_procedure(self, procedure_id):
        """
        Delete specific fit parameter from database.
        :param procedure_id: requested procedure id to delete.
        """
        self.__fit_procedure_repository.delete(procedure_id)

    def get_parameters_by_project_id(
        self, project_id: ProjectID
    ) -> [FitParameter]:
        """
        Using request cleaned data, get all project's fit parameters.
        :param project_id: id of current processed project.
        :return: dict with all data data connected to this project.
        """
        result = self.__fit_parameters_repository.get_all_by_project_id(
            project_id
        )
        return result

    def get_procedures_by_project_id(
        self, project_id: ProjectID
    ) -> [FitProcedure]:
        """
        Using request cleaned data, get all user's projects.
        :param project_id: id of current processed project.
        :return: dict with all data data connected to this project.
        """
        result = self.__fit_procedure_repository.get_all_by_project_id(
            project_id
        )
        return result

    def bulk_create_fit_parameter(self, bulk_list: [dict]) -> [FitParameter]:
        """
        Using request cleaned data, create new project's fit parameters.
        :param bulk_list: request cleaned data.
        :return: list of created fit parameters.
        """
        data_to_create = [FitParameter(**data) for data in bulk_list]
        self.__fit_parameters_repository.bulk_create(data_to_create)
        return data_to_create

    def bulk_create_fit_procedure(self, bulk_list: [dict]) -> [FitProcedure]:
        """
        Using request cleaned data, create new project's fit procedures.
        :param bulk_list: request cleaned data.
        :return: list of created fit procedures.
        """
        data_to_create = [FitProcedure(**data) for data in bulk_list]
        self.__fit_procedure_repository.bulk_create(data_to_create)
        return data_to_create

    def bulk_update_fit_parameter(self, bulk_list: [dict]) -> [dict]:
        """
        Using request cleaned data, update new project's fit parameters.
        :param bulk_list: request cleaned data.
        :return: list of created fit parameters.
        """
        self.__fit_parameters_repository.bulk_update(bulk_list)
        return bulk_list

    def bulk_update_fit_procedure(self, bulk_list: [dict]) -> [dict]:
        """
        Using request cleaned data, update new project's fit procedures.
        :param bulk_list: request cleaned data.
        :return: list of created fit procedures.
        """
        self.__fit_procedure_repository.bulk_update(bulk_list)
        return bulk_list
