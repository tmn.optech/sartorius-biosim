from celery_queue.tasks import fit_model, digital_twin_fit
from containers.services.application_container import AppContextContainer
from helpers.infrastructure.model_serializer import ModelSerializer
from middlewares.error_types import ProjectConfigInvalid
from projects.application.services import RemoteStorageService, ProjectService
from projects.application.services.project_settings_service import (
    ProjectSettingsService,
)
from projects.application.services.feed_service import FeedService
from projects.decorators.project_decorators import update_render_status
from projects.infrastructure.models import Project
from bioreactormodelling.growth_model import reserved_words as rw

from sartorius_wrappers.validation import validate_and_fix_clone_data
from sartorius_wrappers.utilities import construct_dataframe

from typing import Tuple

logger = AppContextContainer.logger()


class ModelFittingService:
    """
    Service providing all method to fit the model.
    """

    def __init__(
        self,
        serializer: ModelSerializer,
        project_data_service: ProjectService,
        remote_storage: RemoteStorageService,
        project_settings_service: ProjectSettingsService,
        feed_service: FeedService,
    ):
        self.__serializer = serializer
        self._project_service = project_data_service
        self.__remote_storage = remote_storage
        self.__project_settings_service = project_settings_service
        self.__feed_service = feed_service

    @update_render_status
    def fit(self, project: Project) -> dict:
        """
        Method for fitting project. Collecting initial data, model config and batches to
        fit before the fitting. After all parameters is set, method starts a b a background task.
        :param project: project database model.
        :return dictionary with task id.
        """
        # Get DataFrame from initial data.
        initial_data = self._project_service.get_initial_file_data(project.id)
        fit_config, batches_to_fit = self.get_fitting_config(
            project, initial_data
        )
        self.__validate_initial_data(
            initial_data, fit_config, project.columns_map
        )
        # Init model fitting.
        result = fit_model.delay(
            project.id,
            initial_data,
            fit_config,
            project.columns_map,
            batches_to_fit,
        )
        return {"task_id": result.task_id}

    @update_render_status
    def digital_twin_fit(self, project: Project) -> dict:
        """
        Method for fitting project. Collecting initial data, model config and batches to
        fit before the fitting. After all parameters is set, method starts a b a background task.
        :param project: project database model.
        :return dictionary with task id.
        """
        # Get DataFrame from initial data.
        initial_data = self._project_service.get_initial_file_data(project.id)
        fit_config, _ = self.get_fitting_config(project, initial_data)
        fit_config["bleed_controller"][
            "control_interval"
        ] = 0.5  # TODO: Hot fix for empty data
        feed_composition = self.__feed_service.get_feed_composition(project.id)
        metabolic_rates = project.metabolite_consumption_rates[
            "average_metabolite_consumption"
        ]
        # Init model fitting.
        result = digital_twin_fit.delay(
            project.id,
            initial_data,
            fit_config,
            project.columns_map,
            metabolic_rates,
            feed_composition,
        )
        return {"task_id": result.task_id}

    @staticmethod
    def __validate_initial_data(
        initial_data: dict, fit_config: dict, columns_map: dict
    ):
        """
        Check if the data complete. In case if data is not valid the error will throw.
        """
        data_frame = construct_dataframe(initial_data, columns_map)
        # This should throw an error if there is an error in the data
        validate_and_fix_clone_data(data_frame, fit_config)

    def get_fitting_config(
        self, project: Project, initial_data: dict
    ) -> Tuple[dict, list]:
        """
        Collect all data from project settings block and creates a dictionary config.
        :param project: project database model.
        :param initial_data: Dict with initial file data.
        """
        # get project dict config
        column_map = project.columns_map
        if not column_map:
            raise ProjectConfigInvalid(
                "Column map for project is missing. Can not run fitting."
            )
        # get project settings
        project_settings = (
            self.__project_settings_service.get_project_settings_by_id(
                project.id
            )
        )
        fit_config = project_settings.dict_config

        # get project fit parameters
        project_fit_parameters = (
            self.__project_settings_service.get_parameters_by_project_id(
                project.id
            )
        )
        fit_config["parameters"] = [
            fit_parameter.dict_config
            for fit_parameter in project_fit_parameters
        ]

        # get project fit procedures
        project_fit_procedures = (
            self.__project_settings_service.get_procedures_by_project_id(
                project.id
            )
        )
        fit_config["fit_procedure"] = [
            fit_procedure.dict_config
            for fit_procedure in project_fit_procedures
        ]

        # get butches to fit
        batch_id_key = column_map.get(rw.BATCH_ID)
        if not batch_id_key:
            raise ProjectConfigInvalid("Can not find batch ID column map")
        list_of_batches = initial_data[batch_id_key].values()
        batches_to_fit = list(map(str, set(list_of_batches)))
        # attach batches to fit
        for procedure in fit_config["fit_procedure"]:
            procedure["batches_to_fit"] = (
                batches_to_fit
                if not procedure["batches_to_fit"]
                else procedure["batches_to_fit"]
            )
        return fit_config, batches_to_fit
