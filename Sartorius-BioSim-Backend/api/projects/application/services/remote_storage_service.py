from io import BytesIO

from projects.application.services.clients.s3_client import AwsS3Client

SUPPORTED_CLIENTS = {"s3": AwsS3Client}


class RemoteStorageService:
    """
    Class to handle file upload and read file data from remote data storages
    """

    def get_file(self, storage_type: str, bucket: str, file_path: str):
        client = self.__get_client(storage_type)
        result = client.read(bucket, file_path)
        return result

    def upload_file(
        self,
        storage_type: str,
        bucket: str,
        file_path: str,
        file_content: BytesIO,
        content_type: str,
    ):
        client = self.__get_client(storage_type)
        result = client.upload(bucket, file_path, file_content, content_type)
        return result

    @staticmethod
    def __get_client(storage_type):
        client = SUPPORTED_CLIENTS.get(storage_type)
        return client()
