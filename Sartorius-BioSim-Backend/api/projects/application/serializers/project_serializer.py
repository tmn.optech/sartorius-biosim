"""
Specific project serializer class. Implemented from base ModelSerializer
but with additional custom fields.
"""
from datetime import datetime
from time import strftime, gmtime
from typing import Union

from helpers.infrastructure.model_serializer import ModelSerializer


class ProjectSerializer(ModelSerializer):
    def serialize(self, model, convert_enum=True):
        serialized_data = super().serialize(model, convert_enum)
        serialized_data["render_uptime"] = self.get_render_uptime(
            serialized_data
        )
        return serialized_data

    @staticmethod
    def get_render_uptime(serialized_project: dict) -> Union[str, None]:
        render_started = (
            serialized_project.get("render_started") or datetime.now()
        )
        render_finished = (
            serialized_project.get("render_finished") or datetime.now()
        )
        render_uptime = render_finished - render_started
        return strftime("%H:%M:%S", gmtime(render_uptime.seconds))
