class FeedSchemas:
    """Schemas for project feed GET / POST validate"""

    feed_schema = {
        "type": "object",
        "properties": {
            "id": {"type": "string", "maxLength": 100},
            "project": {"type": "string", "maxLength": 100},
            "value": {"anyOf": [{"type": "number"}, {"type": "null"}]},
            "name": {"type": "string", "maxLength": 50},
            "metabolite_name": {"type": "string", "maxLength": 50},
            "type": {"type": "string", "maxLength": 50},
        },
        "required": ["project", "name", "metabolite_name", "type"],
        "additionalProperties": False,
    }

    bulk_create_schema = {"type": "array", "items": feed_schema}
