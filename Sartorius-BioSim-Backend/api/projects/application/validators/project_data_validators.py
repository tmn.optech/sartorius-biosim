import re

from wtforms import StringField, SelectField
from flask_wtf.file import FileField, FileRequired, FileAllowed
from wtforms.validators import DataRequired
from middlewares.error_types import ValidationError

from helpers.validators.form_data_validator import MultipartDataValidator
from helpers.validators.request_validator import RequestValidator
from projects.application.validators.project_schemas import ProjectSchemas
from projects.domain.enums.project_type import ProjectType


class ProjectsFieldsForm(MultipartDataValidator):

    name = StringField(validators=[DataRequired()])
    type = SelectField(
        choices=[v.name for v in ProjectType], validators=[DataRequired()]
    )
    initial_file = FileField(
        validators=[
            FileRequired(),
            FileAllowed(["xls", "xlsx", "csv"], "Not supported format!"),
        ]
    )


class ProjectDataValidator:
    def project_data_update_validate(self, params: dict) -> dict:
        """Method validator for body parameters in update request.

        :param params: body parameters in dict representation
        :return: raise Bad Request if invalid schema or return clean params"""

        validator = RequestValidator(
            ProjectSchemas.update_project_request, params
        )
        validator.is_valid(raise_exception=True)
        self.validate_titer_functions(params)
        data = validator.validated_data
        return data

    def validate_project_owner(self, params: dict) -> dict:
        """
        Method to validate project owner request change
        """

        validator = RequestValidator(
            ProjectSchemas.change_user_owner_request, params
        )
        validator.is_valid(raise_exception=True)
        self.validate_titer_functions(params)
        data = validator.validated_data
        return data

    @staticmethod
    def validate_titer_functions(params):
        titer_data = params.get("titer_functions")
        if titer_data:
            # Check that titer has python function attributes
            reg_exp = re.compile(r"^def [a-z]+[(][a-z*, =]+[)]:")
            for function in titer_data.values():
                if not reg_exp.match(function):
                    raise ValidationError(
                        "Titer does not looks like python function. Make sure its def <name>():"
                    )
