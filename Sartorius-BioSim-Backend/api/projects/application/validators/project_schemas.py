from helpers.application.enum_processor import get_names
from projects.domain.enums import ProjectType, CreatingStep


class ProjectSchemas:
    """Schemas for user GET / POST requests for project entity"""

    create_project_request = {
        "type": "object",
        "properties": {
            "type": {"type": "string", "enum": get_names(ProjectType)},
            "name": {"type": "string"},
            "initial_data": {
                "type": "string",
                "pattern": r"\.(csv|xls|xlsx|ambr)$",
            },
        },
        "required": ["type", "name", "initial_data"],
        "additionalProperties": False,
    }

    change_user_owner_request = {
        "type": "object",
        "properties": {
            "user": {"type": "string", "maxLength": 255},
        },
        "required": ["user"],
        "additionalProperties": False,
    }

    update_project_request = {
        "type": "object",
        "properties": {
            "name": {"type": "string"},
            "columns_map": {
                "type": "object",
                "properties": {
                    "BatchID": {"type": "string"},
                    "Bio material": {"type": "string"},
                    "Bleed rate": {"type": "string"},
                    "Dead cell density": {"type": "string"},
                    "Death rate": {"type": "string"},
                    "Effective growth rate": {"type": "string"},
                    "Feed rate": {"type": "string"},
                    "feed conc": {"type": "string"},
                    "Harvest": {"type": "string"},
                    "Lysed cell density": {"type": "string"},
                    "Specific growth rate": {"type": "string"},
                    "Specific productivity": {"type": "string"},
                    "Time": {"type": "string"},
                    "Titer": {"type": "string"},
                    "Viable cell density": {"type": "string"},
                    "Cell viability": {"type": "string"},
                    "Volume": {"type": "string"},
                },
                "additionalProperties": False,
            },
            "creating_step": {
                "type": "string",
                "enum": get_names(CreatingStep),
            },
            "titer_functions": {
                "type": "object",
            },
            "user": {"type": "string"},
        },
        "additionalProperties": False,
    }
