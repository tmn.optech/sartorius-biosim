from projects.application.validators.feed_schemas import FeedSchemas
from helpers.validators.request_validator import RequestValidator


class FeedValidator:

    @staticmethod
    def feed_bulk_create_validate(params: dict) -> dict:
        """Method validator for body parameters in bulk create feed request.

        :param params: body parameters in dict representation
        :return: raise Bad Request if invalid schema or return clean params"""

        validator = RequestValidator(
            FeedSchemas.bulk_create_schema, params
        )
        validator.is_valid(raise_exception=True)

        return validator.validated_data

    @staticmethod
    def feed_bulk_update_validate(params: dict) -> dict:
        """Method validator for body parameters in bulk update feed request.

        :param params: body parameters in dict representation
        :return: raise Bad Request if invalid schema or return clean params"""

        validator = RequestValidator(
            FeedSchemas.bulk_create_schema, params
        )
        validator.is_valid(raise_exception=True)

        return validator.validated_data
