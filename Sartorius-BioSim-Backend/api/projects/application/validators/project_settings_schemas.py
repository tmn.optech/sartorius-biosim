from helpers.application.enum_processor import get_names
from projects.domain.enums.project_property import (
    ParameterRole,
    ParameterType,
    ObjectivePenalty,
    DateType,
    ViableCellDensityType,
    DataSource,
)


class ProjectSettingsSchema:
    """Schemas for project settings GET / POST validate"""

    project_settings_schema = {
        "type": "object",
        "properties": {
            "project_id": {"type": "string", "maxLength": 100},
            "fed_batch_call_density": {
                "type": "number",
                "minimum": 0,
                "maximum": 100,
            },
            "fed_batch_viability": {
                "type": "number",
                "minimum": 0,
                "maximum": 10,
            },
            "perfusion_bleed": {"type": "number", "minimum": 0, "maximum": 10},
            "perfusion_viability": {
                "type": "number",
                "minimum": 0,
                "maximum": 10,
            },
            "objective_penalty": {
                "type": "string",
                "enum": get_names(ObjectivePenalty),
            },
            "date_type": {"type": "string", "enum": get_names(DateType)},
            "viable_cell_density_type": {
                "type": "string",
                "enum": get_names(ViableCellDensityType),
            },
            "bleed_controller_gain": {
                "type": "number",
                "minimum": 1e-5,
                "maximum": 1e-2,
            },
            "bleed_controller_rest_time": {
                "type": "number",
                "minimum": 0.2,
                "maximum": 5,
            },
            "max_oxygen_rate": {"type": "number"},
            "perfusion_rate_threshold": {
                "type": "number",
                "minimum": 0,
                "maximum": 2,
            },
            "source_of_toxicity": {
                "type": "string",
                "enum": get_names(DataSource),
            },
            "source_of_inhibition": {
                "type": "string",
                "enum": get_names(DataSource),
            },
        },
        "additionalProperties": False,
        "minProperties": 14,
    }


class FitParametersSchema:
    """Schemas for project fit parameters GET / POST validate"""

    fit_parameter_schema = {
        "type": "object",
        "properties": {
            "id": {"type": "string", "maxLength": 100},
            "project": {"type": "string", "maxLength": 100},
            "name": {"type": "string", "maxLength": 50},
            "role": {"type": "string", "enum": get_names(ParameterRole)},
            "type": {"type": "string", "enum": get_names(ParameterType)},
            "upper_bound": {"type": "number"},
            "lower_bound": {"type": "number"},
            "target": {"type": "number"},
            "spread": {
                "type": "object",
                "properties": {
                    "upper_bound": {"type": "number"},
                    "lower_bound": {"type": "number"},
                },
            },
            "threshold": {"type": "object"},
            "user_function": {"type": "string", "maxLength": 50},
        },
        "required": ["project", "name", "role"],
        "additionalProperties": False,
    }

    bulk_create_schema = {"type": "array", "items": fit_parameter_schema}


class FitProcedureSchema:
    """Schemas for project fit procedures GET / POST validate"""

    fit_procedure_schema = {
        "type": "object",
        "properties": {
            "id": {"type": "string", "maxLength": 100},
            "project": {"type": "string", "maxLength": 100},
            "step_name": {"type": "string", "maxLength": 50},
            "batches_to_fit": {"type": "array"},
            "number_of_particles": {
                "type": "number",
                "minimum": 10,
                "maximum": 500,
            },
            "iterations": {"type": "number", "minimum": 10, "maximum": 500},
            "spread": {
                "type": "object",
                "properties": {
                    "upper_bound": {"type": "number"},
                    "lower_bound": {"type": "number"},
                },
            },
            "number_of_processes": {"type": "number"},
        },
        "required": [
            "project",
            "step_name",
        ],
        "additionalProperties": False,
    }

    bulk_create_schema = {"type": "array", "items": fit_procedure_schema}
