from .project_settings_schemas import (
    FitProcedureSchema,
    FitParametersSchema,
    ProjectSettingsSchema,
)
from helpers.validators.request_validator import RequestValidator


class ProjectSettingsValidator:
    @staticmethod
    def project_settings_create_validate(params: dict) -> dict:
        """Method validator for body parameters in post project settings request.

        :param params: body parameters in dict representation
        :return: raise Bad Request if invalid schema or return clean params"""

        validator = RequestValidator(
            ProjectSettingsSchema.project_settings_schema, params
        )
        validator.is_valid(raise_exception=True)
        data = validator.validated_data
        return data

    @staticmethod
    def fit_parameter_create_validate(params: dict) -> dict:
        """Method validator for body parameters in post fit parameters request.

        :param params: body parameters in dict representation
        :return: raise Bad Request if invalid schema or return clean params"""

        validator = RequestValidator(
            FitParametersSchema.fit_parameter_schema, params
        )
        validator.is_valid(raise_exception=True)
        data = validator.validated_data
        return data

    @staticmethod
    def fit_parameter_bulk_create_validate(params: dict) -> dict:
        """Method validator for body parameters in bulk create fit parameter request.

        :param params: body parameters in dict representation
        :return: raise Bad Request if invalid schema or return clean params"""

        validator = RequestValidator(
            FitParametersSchema.bulk_create_schema, params
        )
        validator.is_valid(raise_exception=True)

        return validator.validated_data

    @staticmethod
    def fit_parameter_bulk_update_validate(params: dict) -> dict:
        """Method validator for body parameters in bulk create fit parameter request.

        :param params: body parameters in dict representation
        :return: raise Bad Request if invalid schema or return clean params"""

        validator = RequestValidator(
            FitParametersSchema.bulk_create_schema, params
        )
        validator.is_valid(raise_exception=True)

        return validator.validated_data

    @staticmethod
    def fit_procedure_create_validate(params: dict) -> dict:
        """Method validator for body parameters in post fit parameters request.

        :param params: body parameters in dict representation
        :return: raise Bad Request if invalid schema or return clean params"""

        validator = RequestValidator(
            FitProcedureSchema.fit_procedure_schema, params
        )
        validator.is_valid(raise_exception=True)
        data = validator.validated_data
        return data

    @staticmethod
    def fit_procedure_bulk_create_validate(params: dict) -> dict:
        """Method validator for body parameters in bulk create fit parameter request.

        :param params: body parameters in dict representation
        :return: raise Bad Request if invalid schema or return clean params"""

        validator = RequestValidator(
            FitProcedureSchema.bulk_create_schema, params
        )
        validator.is_valid(raise_exception=True)

        return validator.validated_data

    @staticmethod
    def fit_procedure_bulk_update_validate(params: dict) -> dict:
        """Method validator for body parameters in bulk update fit parameter request.

        :param params: body parameters in dict representation
        :return: raise Bad Request if invalid schema or return clean params"""

        validator = RequestValidator(
            FitProcedureSchema.bulk_create_schema, params
        )
        validator.is_valid(raise_exception=True)

        return validator.validated_data
