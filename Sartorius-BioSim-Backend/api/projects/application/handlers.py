from flask import request
from helpers.infrastructure import status
from helpers.infrastructure.model_serializer import ModelSerializer
from middlewares.error_types import DoesNotExist
from projects.application.serializers.project_serializer import (
    ProjectSerializer,
)
from projects.application.services.metabolite_rates_service import (
    MetaboliteRatesService,
)
from projects.application.services.model_fitting_service import (
    ModelFittingService,
)
from projects.application.services.project_service import ProjectService
from projects.application.services.project_settings_service import (
    ProjectSettingsService,
)
from projects.application.services.feed_service import FeedService
from projects.application.validators.project_data_validators import (
    ProjectsFieldsForm,
    ProjectDataValidator,
)
from projects.application.validators.project_settings_validators import (
    ProjectSettingsValidator,
)
from projects.application.validators.feed_validators import FeedValidator
from shared.response import Response
from users.application.decorators.auth_decorators import (
    jwt_requires,
    admin_permission_required,
)


class ProjectHandler:
    def __init__(
        self,
        validators: ProjectDataValidator,
        project_service: ProjectService,
        fitting_serves: ModelFittingService,
        serializer: ProjectSerializer,
    ):
        self._validators = validators
        self._data_service = project_service
        self._fitting_service = fitting_serves
        self._serializer = serializer

    @jwt_requires
    def get_projects_by_user_id(self, *args, **kwargs):
        data = self._data_service.get_user_projects(request.user_id)
        serialized_data = self._serializer.serialize_list(data)
        return Response(
            body=serialized_data, status_code=status.HTTP_200_OK
        ).json

    @jwt_requires
    @admin_permission_required
    def get_all_users_projects(self, *args, **kwargs):
        data = self._data_service.get_all_projects()
        serialized_data = self._serializer.serialize_list(data)
        return Response(
            body=serialized_data, status_code=status.HTTP_200_OK
        ).json

    @jwt_requires
    @admin_permission_required
    def change_project_owner(self, *args, **kwargs):
        params = request.json
        cleaned_data = self._validators.validate_project_owner(params)
        project_id = kwargs["project_id"]
        project = self._data_service.get_project_by_id(project_id)
        if not project:
            return Response(
                body={"error": "project does not exists"},
                status_code=status.HTTP_404_NOT_FOUND,
            ).json
        self._data_service.update_project(project_id, cleaned_data)
        project.update_from_dict(cleaned_data)
        response_data = self._serializer.serialize(project)
        return Response(
            body=response_data, status_code=status.HTTP_200_OK
        ).json

    @jwt_requires
    def post(self, *args, **kwargs):
        form = ProjectsFieldsForm(request.form, request.files)
        if form.validate():
            data = self._data_service.create_project(request.user_id, form)
            serialized_data = self._serializer.serialize(data)
            return Response(
                body=serialized_data, status_code=status.HTTP_201_CREATED
            ).json
        return Response(
            body=form.errors_dict, status_code=status.HTTP_400_BAD_REQUEST
        ).json

    @jwt_requires
    def patch(self, *args, **kwargs):
        params = request.json
        cleaned_data = self._validators.project_data_update_validate(params)
        project_id = kwargs["project_id"]
        self._data_service.update_project(project_id, cleaned_data)
        data = self._data_service.get_project_by_id(project_id)
        if not data:
            return Response(
                body={"error": "project does not exists"},
                status_code=status.HTTP_404_NOT_FOUND,
            ).json
        response_data = self._serializer.serialize(data)
        return Response(
            body=response_data, status_code=status.HTTP_200_OK
        ).json

    @jwt_requires
    def delete(self, *args, **kwargs):
        project_id = kwargs.get("project_id")
        self._data_service.delete_project(project_id)
        return Response(body={}, status_code=status.HTTP_204_NO_CONTENT).json

    @jwt_requires
    def get_initial_file(self, *args, project_id):
        data = self._data_service.get_initial_file_data(project_id)
        return Response(body=data, status_code=status.HTTP_200_OK).json

    def update_initial_data(self, *args, project_id):
        data_to_update = request.json
        data = self._data_service.update_initial_file_data(
            project_id, data_to_update
        )
        return Response(body=data, status_code=status.HTTP_200_OK).json

    @jwt_requires
    def fit_model(self, *args, project_id):
        project = self._data_service.get_project_by_id(project_id)
        if not project:
            return Response(
                body={}, status_code=status.HTTP_404_NOT_FOUND
            ).json
        data = self._fitting_service.fit(project)
        return Response(body=data, status_code=status.HTTP_200_OK).json

    @jwt_requires
    def fit_digital_twin_model(self, *args, project_id):
        project = self._data_service.get_project_by_id(project_id)
        if not project:
            return Response(
                body={}, status_code=status.HTTP_404_NOT_FOUND
            ).json

        data = self._fitting_service.digital_twin_fit(project)
        return Response(body=data, status_code=status.HTTP_200_OK).json

    @jwt_requires
    def get_rendering_projects(self, *args, **kwargs):
        project_ids = self._data_service.get_rendering_projects()
        return Response(body=project_ids, status_code=status.HTTP_200_OK).json


class ProjectSettingsHandler:
    """
    Handler for managing Project settings.
    """

    def __init__(
        self,
        validators: ProjectSettingsValidator,
        project_settings_service: ProjectSettingsService,
        serializer: ModelSerializer,
    ):
        self._validators = validators
        self._data_service = project_settings_service
        self._serializer = serializer

    @jwt_requires
    def get_project_settings_by_id(self, *args, **kwargs):
        project_id = kwargs["project_id"]
        data = self._data_service.get_project_settings_by_id(project_id)
        if not data:
            raise DoesNotExist("No such project settings")
        serialized_data = self._serializer.serialize(data)
        return Response(
            body=serialized_data, status_code=status.HTTP_200_OK
        ).json

    @jwt_requires
    def post(self, *args, **kwargs):
        params = request.json
        cleaned_data = self._validators.project_settings_create_validate(
            params
        )
        data = self._data_service.create_project_settings(cleaned_data)
        serialized_data = self._serializer.serialize(data)
        return Response(
            body=serialized_data, status_code=status.HTTP_201_CREATED
        ).json

    @jwt_requires
    def patch(self, *args, **kwargs):
        params = request.json
        cleaned_data = self._validators.project_settings_create_validate(
            params
        )
        project_id = kwargs["project_id"]
        self._data_service.update_project_settings(project_id, cleaned_data)
        data = self._data_service.get_project_settings_by_id(project_id)
        response_data = self._serializer.serialize(data)
        return Response(
            body=response_data, status_code=status.HTTP_200_OK
        ).json


class FitParametersHandler:
    """
    Handler for managing fit parameters settings.
    """

    def __init__(
        self,
        validator: ProjectSettingsValidator,
        project_settings_service: ProjectSettingsService,
        serializer: ModelSerializer,
    ):
        self._data_service = project_settings_service
        self._serializer = serializer
        self._validators = validator

    @jwt_requires
    def get_fit_parameters_by_project_id(self, *args, **kwargs):
        data = self._data_service.get_parameters_by_project_id(
            kwargs["project_id"]
        )
        serialized_data = self._serializer.serialize_list(data)
        return Response(
            body=serialized_data, status_code=status.HTTP_200_OK
        ).json

    @jwt_requires
    def post(self, *args, **kwargs):
        params = request.json
        cleaned_data = self._validators.fit_parameter_create_validate(params)
        data = self._data_service.create_fit_parameter(cleaned_data)
        serialized_data = self._serializer.serialize(data)
        return Response(
            body=serialized_data, status_code=status.HTTP_201_CREATED
        ).json

    @jwt_requires
    def bulk_create(self, *args, **kwargs):
        params = request.json
        cleaned_data = self._validators.fit_parameter_bulk_create_validate(
            params
        )
        parameters_list = self._data_service.bulk_create_fit_parameter(
            cleaned_data
        )
        serialized_data = self._serializer.serialize_list(parameters_list)
        return Response(
            body=serialized_data, status_code=status.HTTP_201_CREATED
        ).json

    @jwt_requires
    def bulk_update(self, *args, **kwargs):
        params = request.json
        cleaned_data = self._validators.fit_parameter_bulk_update_validate(
            params
        )
        parameters_list = self._data_service.bulk_update_fit_parameter(
            cleaned_data
        )
        return Response(
            body=parameters_list, status_code=status.HTTP_200_OK
        ).json

    @jwt_requires
    def delete(self, *args, **kwargs):
        fit_parameter_id = kwargs.get("fit_parameter_id")
        self._data_service.delete_fit_parameter(fit_parameter_id)
        return Response(body={}, status_code=status.HTTP_204_NO_CONTENT).json


class FitProcedureHandler:
    """
    Handler for managing fit procedure settings.
    """

    def __init__(
        self,
        validator: ProjectSettingsValidator,
        project_settings_service: ProjectSettingsService,
        serializer: ModelSerializer,
    ):
        self._data_service = project_settings_service
        self._serializer = serializer
        self._validators = validator

    @jwt_requires
    def get_fit_procedures_by_project_id(self, *args, **kwargs):
        data = self._data_service.get_procedures_by_project_id(
            kwargs["project_id"]
        )
        serialized_data = self._serializer.serialize_list(data)
        return Response(
            body=serialized_data, status_code=status.HTTP_200_OK
        ).json

    @jwt_requires
    def post(self, *args, **kwargs):
        params = request.json
        cleaned_data = self._validators.fit_procedure_create_validate(params)
        data = self._data_service.create_fit_procedure(cleaned_data)
        serialized_data = self._serializer.serialize(data)
        return Response(
            body=serialized_data, status_code=status.HTTP_201_CREATED
        ).json

    @jwt_requires
    def bulk_create(self, *args, **kwargs):
        params = request.json
        cleaned_data = self._validators.fit_procedure_bulk_create_validate(
            params
        )
        procedures_list = self._data_service.bulk_create_fit_procedure(
            cleaned_data
        )
        serialized_data = self._serializer.serialize_list(procedures_list)
        return Response(
            body=serialized_data, status_code=status.HTTP_201_CREATED
        ).json

    @jwt_requires
    def bulk_update(self, *args, **kwargs):
        params = request.json
        cleaned_data = self._validators.fit_procedure_bulk_update_validate(
            params
        )
        procedures_list = self._data_service.bulk_update_fit_procedure(
            cleaned_data
        )
        return Response(
            body=procedures_list, status_code=status.HTTP_200_OK
        ).json

    @jwt_requires
    def delete(self, *args, **kwargs):
        fit_procedure_id = kwargs.get("fit_procedure_id")
        self._data_service.delete_fit_procedure(fit_procedure_id)
        return Response(body={}, status_code=status.HTTP_204_NO_CONTENT).json


class MetaboliteConsumptionsRateHandler:
    def __init__(
        self,
        project_service: ProjectService,
        metabolite_rates_service: MetaboliteRatesService,
        serializer: ModelSerializer,
    ):
        self._data_service = project_service
        self._metabolite_rates_service = metabolite_rates_service
        self._serializer = serializer

    def get_metabolite_consumptions_rates(self, *args, **kwargs):
        """
        Method to process get metabolite consumption rates from specific project.
        """
        metabolite_rates = (
            self._metabolite_rates_service.get_metabolite_consumption_rates(
                kwargs["project_id"]
            )
        )
        return Response(
            body=metabolite_rates, status_code=status.HTTP_200_OK
        ).json

    def recalculate_metabolite_consumptions_rate(self, *args, **kwargs):
        """
        Recalculate metabolite rates for specific project.
        """
        data = request.json
        self._data_service.update_initial_file_data(kwargs["project_id"], data)
        metabolite_rates = (
            self._metabolite_rates_service.calculate_consumptions_rates(
                kwargs["project_id"], data
            )
        )
        return Response(
            body=metabolite_rates, status_code=status.HTTP_200_OK
        ).json


class FeedHandler:
    def __init__(
        self,
        validator: FeedValidator,
        feed_service: FeedService,
        serializer: ModelSerializer,
    ):
        self._data_service = feed_service
        self._serializer = serializer
        self._validators = validator

    @jwt_requires
    def bulk_create(self, *args, **kwargs):
        """
        Method to bulk create feed from specific project.
        """
        params = request.json
        cleaned_data = self._validators.feed_bulk_create_validate(params)
        feeds = self._data_service.bulk_create_feeds(cleaned_data)
        serialized_data = self._serializer.serialize_list(feeds)
        return Response(
            body=serialized_data, status_code=status.HTTP_201_CREATED
        ).json

    @jwt_requires
    def get_feeds_by_project_id(self, *args, **kwargs):
        data = self._data_service.get_feeds_by_project_id(kwargs["project_id"])
        serialized_data = self._serializer.serialize_list(data)
        return Response(
            body=serialized_data, status_code=status.HTTP_200_OK
        ).json

    @jwt_requires
    def bulk_update(self, *args, **kwargs):
        params = request.json
        cleaned_data = self._validators.feed_bulk_update_validate(params)
        procedures_list = self._data_service.bulk_update_feeds(cleaned_data)
        return Response(
            body=procedures_list, status_code=status.HTTP_200_OK
        ).json

    @jwt_requires
    def delete(self, *args, **kwargs):
        feed_id = kwargs.get("feed_id")
        self._data_service.delete_feed(feed_id)
        return Response(body={}, status_code=status.HTTP_204_NO_CONTENT).json
