from sqlalchemy import event
from datetime import datetime

from projects.infrastructure.models import (
    Project,
    FitParameter,
    Feed,
    FitProcedure,
)


def update_project_listener(mapper, connection, target):
    Project.query.filter_by(id=target.project).update(
        {"date_modified": datetime.now()}
    )


event.listen(FitParameter, "after_insert", update_project_listener)
event.listen(FitParameter, "after_delete", update_project_listener)
event.listen(FitParameter, "after_update", update_project_listener)
event.listen(Feed, "after_insert", update_project_listener)
event.listen(Feed, "after_delete", update_project_listener)
event.listen(Feed, "after_update", update_project_listener)
event.listen(FitProcedure, "after_insert", update_project_listener)
event.listen(FitProcedure, "after_delete", update_project_listener)
event.listen(FitProcedure, "after_update", update_project_listener)
