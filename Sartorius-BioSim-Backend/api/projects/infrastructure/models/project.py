import uuid

from sqlalchemy import ForeignKey
from sqlalchemy.dialects.postgresql import UUID, JSON

from helpers.infrastructure.base_repository import database as db
from projects.domain.enums import ProjectType, CreatingStep
from projects.domain.enums.creating_step import RenderStatus


class Project(db.Model):
    __tablename__ = "project"
    id = db.Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    user = db.Column(UUID(as_uuid=True), ForeignKey("app_user.id"))
    name = db.Column(db.String(50), nullable=False)
    type = db.Column(
        db.Enum(ProjectType),
        default=ProjectType.CLONE_SELECTION.name,
        server_default=ProjectType.CLONE_SELECTION.name,
        nullable=False,
    )
    creating_step = db.Column(
        db.Enum(CreatingStep),
        default=CreatingStep.DATA_UPLOAD.name,
        server_default=CreatingStep.DATA_UPLOAD.name,
        nullable=False,
    )
    render_status = db.Column(
        db.Enum(RenderStatus),
        default=RenderStatus.NOT_STARTED.name,
        server_default=RenderStatus.NOT_STARTED.name,
        nullable=False,
    )
    render_started = db.Column(db.DateTime, nullable=True)
    render_finished = db.Column(db.DateTime, nullable=True)
    initial_data = db.Column(db.String(200))
    columns_map = db.Column(JSON)
    kinetic_summary = db.Column(JSON)
    fit_statistics = db.Column(JSON)
    titer_functions = db.Column(JSON)
    date_created = db.Column(db.DateTime, default=db.func.current_timestamp())
    date_modified = db.Column(
        db.DateTime,
        default=db.func.current_timestamp(),
        onupdate=db.func.current_timestamp(),
    )
    metabolite_consumption_rates = db.Column(JSON)

    def __repr__(self):
        return f"<Project {self.id}"

    @property
    def project_id(self):
        return str(self.id)

    @property
    def initial_file_path(self):
        return f"{self.user}/{self.id}/initial_file/{self.initial_data}"

    def update_from_dict(self, data: dict):
        for key, value in data.items():
            if getattr(self, key):
                setattr(self, key, value)
