import uuid
from flask import current_app as app
from sqlalchemy import ForeignKey
from sqlalchemy.dialects.postgresql import UUID, JSON, ARRAY

from helpers.infrastructure.base_repository import database as db
from projects.domain.enums.project_property import (
    ParameterRole,
    ParameterType,
    ObjectivePenalty,
    DateType,
    ViableCellDensityType,
    DataSource,
)


class ProjectSettings(db.Model):
    __tablename__ = "project_settings"
    project_id = db.Column(
        UUID(as_uuid=True),
        ForeignKey("project.id", ondelete="CASCADE"),
        primary_key=True,
    )

    # Objective penalty block
    fed_batch_call_density = db.Column(db.Float())
    fed_batch_viability = db.Column(db.Float())
    perfusion_bleed = db.Column(db.Float())
    perfusion_viability = db.Column(db.Float())
    objective_penalty = db.Column(db.Enum(ObjectivePenalty), nullable=False)

    # Units
    date_type = db.Column(
        db.Enum(DateType), nullable=False, default=DateType.DAYS.name
    )
    viable_cell_density_type = db.Column(
        db.Enum(ViableCellDensityType),
        nullable=False,
        default=ViableCellDensityType.E_SIX.name,
    )

    # Bleed control
    bleed_controller_gain = db.Column(db.Float())
    bleed_controller_rest_time = db.Column(db.Float())
    max_oxygen_rate = db.Column(db.Float())
    perfusion_rate_threshold = db.Column(db.Float())
    source_of_toxicity = db.Column(
        db.Enum(DataSource),
        nullable=False,
        default=DataSource.LYSED_CELLS.name,
    )
    source_of_inhibition = db.Column(
        db.Enum(DataSource),
        nullable=False,
        default=DataSource.BIOMATERIAL.name,
    )

    def __repr__(self):
        return f"<Project setting {self.project_id}"

    @property
    def project_setting_id(self):
        return str(self.project_id)

    @property
    def dict_config(self):
        dict_to_return = {
            "objective_function": ObjectivePenalty(
                self.objective_penalty
            ).value,
            "fit_variables": ["Viable cell density", "Cell viability"],
            "fit_weights": [
                self.fed_batch_call_density,
                self.fed_batch_viability,
            ],
            "date_type": DateType(self.date_type).value,
            "viable_cell_density_type": ViableCellDensityType(
                self.viable_cell_density_type
            ).value,
            "bleed_controller": {
                "gain": self.bleed_controller_gain,
                "rest_time": self.bleed_controller_rest_time,
            },
            "max_oxygen_rate": self.max_oxygen_rate,
            "source_of_toxicity": DataSource(self.source_of_toxicity).value,
            "source_of_inhibition": DataSource(
                self.source_of_inhibition
            ).value,
        }
        return dict_to_return


class FitParameter(db.Model):
    __tablename__ = "fit_parameters"
    id = db.Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    project = db.Column(
        UUID(as_uuid=True), ForeignKey("project.id", ondelete="CASCADE")
    )
    name = db.Column(db.String(50), nullable=False)
    module = db.Column(db.String(50))
    function = db.Column(db.String(50))
    role = db.Column(db.Enum(ParameterRole), nullable=False)
    type = db.Column(db.Enum(ParameterType))
    upper_bound = db.Column(db.Float())
    lower_bound = db.Column(db.Float())
    target = db.Column(db.Float())
    spread = db.Column(JSON)
    threshold = db.Column(JSON)
    user_function = db.Column(db.String(50))
    is_additional = db.Column(db.Boolean, default=False, server_default='false')

    def __repr__(self):
        return f"<Fit parameter {self.id}"

    @property
    def fit_parameter_id(self):
        return str(self.id)

    @property
    def dict_config(self):
        dict_to_return = {
            "name": self.name,
            "role": ParameterRole(self.role).value,
        }
        if self.type:
            dict_to_return["type"] = ParameterType(self.type).value
        if self.upper_bound:
            dict_to_return["upper_bound"] = self.upper_bound
        if self.lower_bound:
            dict_to_return["lower_bound"] = self.lower_bound
        if self.target:
            dict_to_return["target"] = self.target
        if self.threshold:
            dict_to_return["threshold"] = self.threshold
        if self.module:
            dict_to_return["module"] = self.module
        if self.function:
            dict_to_return["function"] = self.function
        if self.spread:
            dict_to_return["spread"] = self.spread
        return dict_to_return


class FitProcedure(db.Model):
    __tablename__ = "fit_procedures"
    id = db.Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    project = db.Column(
        UUID(as_uuid=True), ForeignKey("project.id", ondelete="CASCADE")
    )
    step_name = db.Column(db.String(50), nullable=False)
    batches_to_fit = db.Column(ARRAY(db.String))
    number_of_particles = db.Column(db.Float())
    iterations = db.Column(db.Float())

    def __repr__(self):
        return f"<Fit procedure {self.id}"

    @property
    def fit_procedure_id(self):
        return str(self.id)

    @property
    def dict_config(self):
        return {
            "step_name": self.step_name,
            "batches_to_fit": self.batches_to_fit,
            "number_of_particles": self.number_of_particles,
            "iterations": self.iterations,
            "speed": 2,
            "number_of_processes": app.config.get("NUMBER_OF_PROCESSES", 8),
        }
