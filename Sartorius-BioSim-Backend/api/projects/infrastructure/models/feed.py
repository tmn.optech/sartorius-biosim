import uuid

from sqlalchemy import ForeignKey, UniqueConstraint
from sqlalchemy.dialects.postgresql import UUID

from helpers.infrastructure.base_repository import database as db


class Feed(db.Model):
    __tablename__ = "feed"
    __table_args__ = (
        UniqueConstraint(
            "name", "metabolite_name", "project", name="feed_metabolite_uc"
        ),
    )
    id = db.Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    project = db.Column(UUID(as_uuid=True), ForeignKey("project.id"))
    value = db.Column(db.Float())
    name = db.Column(db.String(50), nullable=False)
    metabolite_name = db.Column(db.String(50), nullable=False)
    type = db.Column(db.String(50), nullable=False)

    def __repr__(self):
        return f"<Feed {self.id}"

    @property
    def feed_id(self):
        return str(self.id)

    @property
    def as_dict(self):
        return {
            "value": self.value,
            "name": self.name,
            "metabolite_name": self.metabolite_name,
            "type": self.type,
        }
