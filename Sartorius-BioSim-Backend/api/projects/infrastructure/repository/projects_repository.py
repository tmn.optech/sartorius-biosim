from typing import Union

from helpers.infrastructure.base_repository import BaseRepository
from middlewares.error_types import DoesNotExist
from users.domain.user_id import UserID
from projects.infrastructure.models import Project
from projects.domain.project_id import ProjectID


class ProjectRepository(BaseRepository):
    """Class with describing communication with projects table"""

    def get_all(self) -> [Project]:
        """Method for getting all projects from db

        :return: list of Projects instances"""

        projects = Project.query.order_by(Project.date_created.desc()).all()
        return projects

    def get_filtered(self, filtered_data: dict) -> [Project]:
        """Method for getting filtered projects from db

        :return: list of Projects instances"""

        projects = Project.query.filter_by(**filtered_data)
        return projects

    def get_one_by_id(self, project_id: ProjectID) -> Union[Project, None]:
        """Method for getting project from db by id

        :param project_id: primary key in project table
        :return: Project instance"""
        try:
            project = Project.query.filter_by(id=project_id).first()
            return project
        except Exception:
            raise DoesNotExist("Project does not exist")

    def get_all_by_user_id(self, user_id: UserID) -> Union[Project, None]:
        """Method for getting user from db by id

        :param user_id: primary key in user table
        :return: projects instance"""

        projects = Project.query.filter_by(user=user_id.base_58).order_by(
            Project.date_created.desc()
        )
        return projects

    def save(self, project: Project) -> None:
        """Method for adding project in projects table

        :param project: Project entity from domain level
        :return: None"""
        self.db.session.add(project)
        try:
            self.db.session.commit()
        except Exception as e:
            self.db.session.rollback()
            raise e

    def update(self, project_id: ProjectID, data: dict) -> None:
        """Update project data by project id
        :param project_id: ID of project to be updated
        :param data: data to be updated
        :return None
        """
        Project.query.filter_by(id=project_id).update(data)
        self.db.session.commit()

    def delete(self, project_id) -> None:
        """Method to delete project from database"""
        try:
            Project.query.filter_by(id=project_id).delete()
            self.db.session.commit()
        except Exception as e:
            self.db.session.rollback()
            raise e
