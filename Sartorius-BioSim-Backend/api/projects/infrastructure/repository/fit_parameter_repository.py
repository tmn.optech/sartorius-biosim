from typing import Union, List

from helpers.infrastructure.base_repository import BaseRepository
from projects.infrastructure.models import FitParameter
from projects.domain.project_id import ProjectID
from projects.domain.fit_parameter_id import FitParameterID


class FitParameterRepository(BaseRepository):
    """Class with describing communication with Fit parameter table"""

    @staticmethod
    def get_all() -> [FitParameter]:
        """Method for getting all fit parameters from db

        :return: list of FitParameter instances"""

        projects = FitParameter.query.all()
        return projects

    @staticmethod
    def get_one_by_id(
        fit_parameter_id: FitParameterID,
    ) -> Union[FitParameter, None]:
        """Method for getting FitParameter from db by id

        :param fit_parameter_id: primary key in fit parameter table
        :return: FitParameter instance"""

        fit_parameter = FitParameter.query.filter_by(
            id=fit_parameter_id
        ).first()
        return fit_parameter

    @staticmethod
    def get_all_by_project_id(
        project_id: ProjectID,
    ) -> Union[List[FitParameter], None]:
        """Method for getting FitParameter from db by project id

        :param project_id: primary key in project table
        :return: FitParameter instance"""

        fit_parameters = FitParameter.query.filter_by(project=project_id)
        return fit_parameters

    def save(self, fir_parameter: FitParameter) -> None:
        """Method for adding fit parameter in fit parameter table

        :param fir_parameter: FitParameter entity from domain level
        :return: None"""
        self.db.session.add(fir_parameter)
        try:
            self.db.session.commit()
        except Exception as e:
            self.db.session.rollback()
            raise e

    def bulk_create(self, fit_parameters: [FitParameter]) -> None:
        """Method for adding list of fit parameters in fit parameter table

        :param fit_parameters: List of FitParameter entity from domain level.
        :return: None"""
        self.db.session.add_all(fit_parameters)
        try:
            self.db.session.commit()
        except Exception as e:
            self.db.session.rollback()
            raise e

    def bulk_update(self, fit_parameters: [dict]) -> None:
        """Method for update list of fit parameters in fit parameter table

        :param fit_parameters: List of data to update.
        :return: None"""
        self.db.session.bulk_update_mappings(FitParameter, fit_parameters)
        try:
            self.db.session.commit()
        except Exception as e:
            self.db.session.rollback()
            raise e

    def delete(self, fit_parameter_id) -> None:
        """Method to delete fit parameter from database"""
        try:
            FitParameter.query.filter_by(id=fit_parameter_id).delete()
            self.db.session.commit()
            self.db.session.commit()
        except Exception as e:
            self.db.session.rollback()
            raise e
