from typing import Union, List

from helpers.infrastructure.base_repository import BaseRepository
from projects.domain.project_id import ProjectID
from projects.infrastructure.models import Feed
from projects.domain.feed_id import FeedID


class FeedRepository(BaseRepository):
    """Class with describing communication with Feed table"""

    @staticmethod
    def get_all() -> [Feed]:
        """Method for getting all feeds from db

        :return: list of Feed instances"""

        projects = Feed.query.all()
        return projects

    @staticmethod
    def get_one_by_id(
        feed_id: FeedID,
    ) -> Union[Feed, None]:
        """Method for getting Feed from db by id

        :param feed_id: primary key in Feed table
        :return: Feed instance"""

        fit_parameter = Feed.query.filter_by(id=feed_id).first()
        return fit_parameter

    @staticmethod
    def get_all_by_project_id(
        project_id: ProjectID,
    ) -> Union[List[Feed], None]:
        """Method for getting Feed from db by project id

        :param project_id: primary key in project table
        :return: Feed instance"""

        feeds = Feed.query.filter_by(project=project_id)
        return feeds

    def save(self, feed: Feed) -> None:
        """Method for adding Feed in Feed table

        :param feed: Feed entity from domain level
        :return: None"""
        self.db.session.add(feed)
        try:
            self.db.session.commit()
        except Exception as e:
            self.db.session.rollback()
            raise e

    def bulk_create(self, feed: [Feed]) -> None:
        """Method for adding list of Feeds in Feed table

        :param feed: List of Feed entity from domain level.
        :return: None"""
        self.db.session.add_all(feed)
        try:
            self.db.session.commit()
        except Exception as e:
            self.db.session.rollback()
            raise e

    def bulk_update(self, feeds: [dict]) -> None:
        """Method for update list of Feeds in Feed table

        :param feeds: List of data to update.
        :return: None"""
        self.db.session.bulk_update_mappings(Feed, feeds)
        try:
            self.db.session.commit()
        except Exception as e:
            self.db.session.rollback()
            raise e

    def delete(self, feed_id) -> None:
        """Method to delete Feed from database"""
        try:
            Feed.query.filter_by(id=feed_id).delete()
            self.db.session.commit()
        except Exception as e:
            self.db.session.rollback()
            raise e
