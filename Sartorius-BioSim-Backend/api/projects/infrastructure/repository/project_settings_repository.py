from typing import Union

from helpers.infrastructure.base_repository import BaseRepository
from middlewares.error_types import DoesNotExist
from projects.infrastructure.models import ProjectSettings
from projects.domain.project_id import ProjectID


class ProjectSettingsRepository(BaseRepository):
    """Class with describing communication with projects table"""

    @staticmethod
    def get_one_by_id(project_id: ProjectID) -> Union[ProjectSettings, None]:
        """Method for getting project settings from db by id

        :param project_id: primary key in project settings table.
        :return: ProjectSettings instance"""
        try:
            project = ProjectSettings.query.filter_by(
                project_id=project_id
            ).first()
            return project
        except Exception:
            raise DoesNotExist("Project settings does not exist")

    def save(self, project_settings: ProjectSettings) -> None:
        """Method for adding project in projects table

        :param project_settings: Project entity from domain level
        :return: None"""
        self.db.session.add(project_settings)
        try:
            self.db.session.commit()
        except Exception as e:
            self.db.session.rollback()
            raise e

    def update(self, project_id: ProjectID, data: dict) -> None:
        """Update project data by project id
        :param project_id: ID of project settings to be updated
        :param data: data to be updated
        :return None
        """
        ProjectSettings.query.filter_by(project_id=project_id).update(data)
        self.db.session.commit()

    def delete(self, project_id) -> None:
        """Method to delete project from database"""
        try:
            ProjectSettings.query.filter_by(project_id=project_id).delete()
            self.db.session.commit()
        except Exception as e:
            self.db.session.rollback()
            raise e
