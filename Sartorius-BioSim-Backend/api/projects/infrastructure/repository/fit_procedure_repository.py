from typing import Union, List

from helpers.infrastructure.base_repository import BaseRepository
from projects.infrastructure.models import FitProcedure
from projects.domain.project_id import ProjectID
from projects.domain.fit_procedure_id import FitProcedureID


class FitProcedureRepository(BaseRepository):
    """Class with describing communication with Fit parameter table"""

    @staticmethod
    def get_all() -> [FitProcedure]:
        """Method for getting all projects from db

        :return: list of FitProcedure instances"""

        projects = FitProcedure.query.all()
        return projects

    @staticmethod
    def get_one_by_id(
        fit_procedure_id: FitProcedureID,
    ) -> Union[FitProcedure, None]:
        """Method for getting FitProcedure from db by id

        :param fit_procedure_id: primary key in fit parameter table
        :return: FitProcedure instance"""

        fit_procedure = FitProcedure.query.filter_by(
            id=fit_procedure_id
        ).first()
        return fit_procedure

    @staticmethod
    def get_all_by_project_id(
        project_id: ProjectID,
    ) -> Union[List[FitProcedure], None]:
        """Method for getting FitProcedure from db by project id

        :param project_id: primary key in project table
        :return: FitProcedure instance"""

        fit_procedures = FitProcedure.query.filter_by(project=project_id)
        return fit_procedures

    def save(self, fit_procedure: FitProcedure) -> None:
        """Method for adding fit procedure in fit procedure table

        :param fit_procedure: FitProcedure entity from domain level
        :return: None"""
        self.db.session.add(fit_procedure)
        try:
            self.db.session.commit()
        except Exception as e:
            self.db.session.rollback()
            raise e

    def bulk_create(self, fit_procedures: [FitProcedure]) -> None:
        """Method for adding list of fit procedures in fit procedure table

        :param fit_procedures: List of FitProcedure entity from domain level.
        :return: None"""
        self.db.session.add_all(fit_procedures)
        try:
            self.db.session.commit()
        except Exception as e:
            self.db.session.rollback()
            raise e

    def bulk_update(self, fit_procedures: [dict]) -> None:
        """Method for update list of fit procedures in fit procedure table

        :param fit_procedures: List of data to update.
        :return: None"""
        self.db.session.bulk_update_mappings(FitProcedure, fit_procedures)
        try:
            self.db.session.commit()
        except Exception as e:
            self.db.session.rollback()
            raise e

    def delete(self, fit_procedure_id) -> None:
        """Method to delete fit procedure from database"""
        try:
            FitProcedure.query.filter_by(id=fit_procedure_id).delete()
            self.db.session.commit()
        except Exception as e:
            self.db.session.rollback()
            raise e
