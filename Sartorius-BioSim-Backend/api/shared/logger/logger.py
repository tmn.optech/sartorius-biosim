import traceback
from loguru import logger as loguru_logger


def calc_scope() -> str:
    stack = traceback.extract_stack()
    handler_scope = None
    service_scope = None
    for item in stack:
        item: traceback.FrameSummary = item
        if "handler" in item.filename.lower():
            handler_scope = item.name
        if "service" in item.filename.lower():
            service_scope = item.name
    scope = handler_scope if handler_scope else service_scope
    scope = scope if scope else "System"
    return scope


class Logger:
    def __init__(self):
        self.__logger = loguru_logger.bind(name="system")

    def configure(self, **kwargs):
        self.__logger.configure(**kwargs)

    def context_info(self, msg, context_info=None):
        context_info = context_info if context_info else {}
        self.info(msg, context_info)

    def info(self, msg, context_info):
        with self.__logger.contextualize(
            user=context_info.get("USER-ID", "None"),
            url=context_info.get("URL", "None"),
            correlation_id=context_info.get("HTTP_X_REQUEST_ID", "None"),
            method=context_info.get("METHOD", "None"),
            status=context_info.get("STATUS", "None"),
            scope=context_info.get("SCOPE", calc_scope()),
        ):
            self.__logger.info(msg)

    def warning(self, msg, args, context_info):
        with self.__logger.contextualize(
            user=context_info.get("USER-ID", "None"),
            url=context_info.get("URL", "None"),
            correlation_id=context_info.get("HTTP_X_REQUEST_ID", "None"),
            method=context_info.get("METHOD", "None"),
            status=context_info.get("STATUS", "None"),
            scope=context_info.get("SCOPE", calc_scope()),
        ):
            self.__logger.warning(msg, args)

    def error(self, msg, context_info):
        with self.__logger.contextualize(
            user=context_info.get("USER-ID", "None"),
            url=context_info.get("REQUEST_URI", "None"),
            correlation_id=context_info.get("HTTP_X_REQUEST_ID", "None"),
            method=context_info.get("REQUEST_METHOD", "None"),
            status=context_info.get("STATUS", "None"),
            scope=context_info.get("SCOPE", calc_scope()),
        ):
            self.__logger.error(msg)

    def exception(self, msg):
        self.__logger.exception(msg)

    @property
    def core(self):
        return self.__logger
