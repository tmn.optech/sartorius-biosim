from flask import jsonify


class Response:
    def __init__(self, body, status_code):
        self.body = body
        self.status_code = status_code

    @property
    def json(self):
        return jsonify(data=self.body, status=self.status_code), self.status_code
