from helpers.infrastructure import status
from shared.response import Response


def check_alive():
    return Response(body={}, status_code=status.HTTP_200_OK).json
