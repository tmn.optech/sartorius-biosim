from typing import List

import pandas as pd
import numpy as np


class ErrorSummary:
    """
    Class providing an error summary (RMSE, R2 and MAE) for model evaluation

    Attributes:
        rmse : float
            RMSE, Root-Mean-Square Error

        r2 : float
            R2

        mae : float
            MAE, Mean Absolute Error

    """

    def __init__(
        self, rmse: float = np.nan, r2: float = np.nan, mae: float = np.nan
    ):
        self.rmse = rmse
        self.r2 = r2
        self.mae = mae

    def __repr__(self):
        return "(RMSE: {}; R2: {}, MAE: {})".format(
            self.rmse, self.r2, self.mae
        )


def evaluate_model(
    variables: List[str],
    data_sim: pd.DataFrame,
    data_true: pd.DataFrame,
    batch_id: str,
):
    """
    Function to evaluate a model for a given list of variables calculating RMSE, R2 and MAE averaged over
    the simulated batches.

    Parameters
    ----------
    variables : List[str]
        List of variables, e.g. [rw.VCD, rw.VIABILITY] for a batch model
        or [rw.VCD, rw.VIABILITY, rw.BLEED] for a perfusion model
    data_sim : pd.DataFrame
        Dataframe with simulated results. It must have a compound index [BatchID, Time].
    data_true : pd.DataFrame
        Dataframe with experimental data. It must have a compound index [BatchID, Time].
    batch_id: str
        The batch id to calculate error for.
    Returns
    -------
    Dict[Variable, ErrorSummary]
        Dictionary of the ErrorSummary objects: {Variable: ErrorSummary} where each object represents an error summary
        for a corresponding Variable.

    """
    var_sim_diff = set(variables).difference(data_sim.columns)
    if len(var_sim_diff) > 0:
        raise ValueError(
            "Variables {} are not in the simulated dataset!".format(
                list(var_sim_diff)
            )
        )

    var_true_diff = set(variables).difference(data_true.columns)
    if len(var_true_diff) > 0:
        raise ValueError(
            "Variables {} are not in the experimental dataset!".format(
                list(var_true_diff)
            )
        )

    rmse = {v: [] for v in variables}
    r2 = {v: [] for v in variables}
    mae = {v: [] for v in variables}

    for var in variables:
        y_sim = data_sim[["Time", var]].set_index("Time")

        current_true_batch = data_true["BatchID"] == batch_id

        measured_values = (
            data_true.loc[current_true_batch, ["Time", var]]
            .dropna()
            .set_index("Time")
        )
        y_true = measured_values[var].to_numpy()
        y_pred = y_sim.loc[measured_values.index, var].to_numpy()

        rmse_batch = np.sqrt(np.mean(np.square(y_true - y_pred)))
        rmse[var].append(rmse_batch)

        r2_batch = 1 - np.sum(np.square(y_true - y_pred)) / np.sum(
            np.square(y_true - np.mean(y_true))
        )
        r2[var].append(r2_batch)

        mae_batch = np.mean(np.abs(y_true - y_pred))
        mae[var].append(mae_batch)

    errors = {
        v: ErrorSummary(
            rmse=float(np.mean(rmse[v])),
            r2=float(np.mean(r2[v])),
            mae=float(np.mean(mae[v])),
        )
        for v in variables
    }

    return errors
