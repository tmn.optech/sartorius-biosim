import bioreactormodelling.controllers as brcontroller

import bioreactormodelling.growth_model.reserved_words as rw

class TriggeredPiController(brcontroller.PiController):

    def update(self, time, state, inputs):

        self.storage["time"].append(time)
        if not self.in_perfusion_mode and state[rw.VCD] >= 0.95 * self.vcd_target:
            self.in_perfusion_mode = True
        bleed = self._update_bleed(state, inputs)
        harvest = self._update_harvest(inputs)

        return dict(), {rw.BLEED: bleed, rw.HARVEST: harvest}


    def _update_bleed(self, state, inputs):
        if self.in_perfusion_mode:
           
            pi_error = self.vcd_target - state[rw.VCD]
            delta_bleed = (
                + self.kc * (pi_error - self.last_pi_error)
                + self.kc / self.tau_i * pi_error
            )
            self.last_pi_error = pi_error

            bleed = min(
                max(self.storage["bleed"][-1] + delta_bleed, 0), inputs[rw.FEED]
            )

        else:
            bleed = 0

        self.storage["bleed"].append(bleed)

        return bleed