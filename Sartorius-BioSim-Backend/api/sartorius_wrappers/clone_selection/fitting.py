from typing import Callable
from bioreactormodelling.growth_model import bioreactor as br
from bioreactormodelling.utilities import data_handler as dh

from bioreactormodelling.growth_model import reserved_words as rw
from bioreactormodelling.model_fitting import model_fitter as mf

from bioreactormodelling.growth_model import dataset_simulator as sim
import bioreactormodelling.model_configuration.fit_config_parser as fit_parser
import bioreactormodelling.model_configuration.sim_config_parser as sim_parser

import numpy as np
import pandas as pd

from typing import Dict
from ..validation import validate_and_fix_clone_data
from ..error_evaluation_functions import evaluate_model
from ..utilities import finalize_data


def fit_clone(data_frame: pd.DataFrame, fit_config: dict, batch: str):
    """This function fits a model to the data of a single batch"""

    preprocessed_data = validate_and_fix_clone_data(data_frame, fit_config)

    # Add speed parameter to fit_config
    fit_config["fit_procedure"][0]["speed"] = 0.55
    fit_config["fit_procedure"][1]["speed"] = 0.05

    # Get fit parameters and procedures from project settings.
    fit_profile = fit_parser._construct_result(fit_config)
    fit_config = fit_profile.fit_config

    data = dh.DataHandler(preprocessed_data, fit_config)
    # Instantiate a fitter
    fitter = mf.ModelFitter(fit_profile.fit_config)

    fitted_model = None

    for step in fit_profile.fit_procedure:
        step.batches_to_fit = [batch]
        fitted_model = fitter.fit(data, step, show_progress_bars=False)

    if fitted_model is None:
        raise ValueError("No steps to fit on")

    if rw.TITER in data_frame.columns:
        titer_function = fit_clone_titer_function(
            data, fitted_model.get_fit_parameters_as_dict(), batch
        )
    else:
        titer_function = None

    simulated_data = simulate_clone(
        data,
        fitted_model.get_fit_parameters_as_dict(),
        batch,
        titer_function,
    )
    errors = evaluate_model(
        [rw.VCD, rw.VIABILITY], simulated_data, data_frame, batch
    )

    fit_statistics = {
        rw.VCD: {
            "r_squared": errors[rw.VCD].r2,
            "rmse": errors[rw.VCD].rmse,
        },
        rw.VIABILITY: {
            "r_squared": errors[rw.VIABILITY].r2,
            "rmse": errors[rw.VIABILITY].rmse,
        },
    }
    return (fitted_model, titer_function, fit_statistics)


def simulate_clone(
    data: dh.DataHandler,
    config_dict: Dict[str, Dict],
    batch: str,
    titer_function: Callable = None,
) -> pd.DataFrame:
    """This function simulates a single batch"""
    # Get fitted model coefficients
    sim_config = sim_parser._construct_result(config_dict)

    # Simulate to calculate the biomaterial, which is equal to the iVCD to
    # calculate the specific productivity
    reactor = br.Bioreactor(
        sim_config.props,
        sim_config.coeffs,
        titer_function=titer_function,
        use_lysed_cells=True,
        record_auxiliary_values=True,
    )
    simulator = sim.DatasetSimulator(data, reactor)
    simulator.simulate_batch(batch)
    result = finalize_data(reactor)

    return result


def _titer_function_factory(specific_productivity: float):
    def titer_function(time, reactor_state, reactor_inputs):
        return specific_productivity

    return titer_function


def fit_clone_titer_function(
    data: dh.DataHandler, configuration: Dict, batch: str
) -> Callable:
    """
    Fits a titer function to the data.

    This titer function is to be used only in the clone selection case and
    not for the digital twin case.

    Parameters
    ----------

    data : dh.DataHandler
        An instatiated data handler. It must use the same configuration
        as passed into this function
    configuration : Dict
        The result of fitting a model to a single batch
    batch : str
        The batch id to fit the titer function for

    Returns
    -------
    Callable
        A titer function, which can be passed to the setup_reactor
        function.
    """
    # Simulate to get biomaterial
    result = simulate_clone(data, configuration, batch)
    print(result)
    # Calculate the productivity
    data.set_cur_batch(batch)
    specific_productivity = (
        np.nanmax(data.cur_batch_data[rw.TITER])
        / result.loc[len(result) - 1][rw.BIO_MATERIAL]
    )

    # Create a function to return the specific productivity,
    # which in this case is a constant
    return _titer_function_factory(specific_productivity)
