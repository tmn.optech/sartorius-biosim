from typing import Any, Callable, Tuple
from enum import Enum
from bioreactormodelling.growth_model import bioreactor as br
from bioreactormodelling.utilities import data_handler as dh

from bioreactormodelling.growth_model import reserved_words as rw
from bioreactormodelling.model_fitting import model_fitter as mf

from bioreactormodelling.growth_model import dataset_simulator as sim
import bioreactormodelling.model_configuration.fit_config_parser as fit_parser
import bioreactormodelling.model_configuration.sim_config_parser as sim_parser
import bioreactormodelling.controllers as controllers
from sartorius_wrappers.controllers import TriggeredPiController

import numpy as np
import pandas as pd
import random

from typing import Dict
from sartorius_wrappers.validation import validate_and_fix_clone_data
from sartorius_wrappers.utilities import (
    finalize_data,
    calculate_summary_statistics,
)

CONTROL_FREQUENCY = 1 / 6
CONTROL_START = 0

LOWER_VCD_LIMIT = 10
UPPER_VCD_LIMIT = 200

"""
Functions for fitting the clones and the growth kinetics page
"""


def fit_clone(data_frame: pd.DataFrame, fit_config: dict, batch: str):
    """This function fits a model to the data of a single batch"""

    preprocessed_data = validate_and_fix_clone_data(data_frame, fit_config)

    # Add speed parameter to fit_config
    fit_config["fit_procedure"][0]["speed"] = 0.55
    fit_config["fit_procedure"][1]["speed"] = 0.05

    # Get fit parameters and procedures from project settings.
    fit_profile = fit_parser._construct_result(fit_config)
    fit_config = fit_profile.fit_config

    data = dh.DataHandler(preprocessed_data, fit_config)
    # Instantiate a fitter
    fitter = mf.ModelFitter(fit_profile.fit_config)

    fitted_model = None

    for step in fit_profile.fit_procedure:
        step.batches_to_fit = [batch]
        fitted_model = fitter.fit(data, step, show_progress_bars=False)

    if fitted_model is None:
        raise ValueError("No steps to fit on")

    if rw.TITER in data_frame.columns:
        titer_function = fit_clone_titer_function(
            data, {batch: [fitted_model.get_fit_parameters_as_dict()]}, batch
        )
    else:
        titer_function = None

    fit_statistics = {"r_squared": random.random(), "rmse": random.random()}
    return fitted_model, titer_function, fit_statistics


def simulate_clone(
    data: dh.DataHandler,
    config_dict: Dict[str, Dict],
    batch: str,
    titer_function: Callable = None,
) -> pd.DataFrame:
    """This function simulates a single batch"""
    # Get fitted model coefficients
    sim_config = sim_parser._construct_result(config_dict[batch][0])

    # Simulate to calculate the biomaterial, which is equal to the iVCD to
    # calculate the specific productivity
    reactor = br.Bioreactor(
        sim_config.props,
        sim_config.coeffs,
        titer_function=titer_function,
        use_lysed_cells=True,
        record_auxiliary_values=True,
    )
    simulator: sim.DatasetSimulator = sim.DatasetSimulator(data, reactor)
    result = simulator.simulate_batch(batch)

    return result


def _titer_function_factory(specific_productivity: float):
    def titer_function(time, reactor_state, reactor_inputs):
        return specific_productivity

    return titer_function


def fit_clone_titer_function(
    data: dh.DataHandler, configuration: Dict, batch: str
) -> Callable:
    """
    Fits a titer function to the data.

    This titer function is to be used only in the clone selection case and
    not for the digital twin case.

    Parameters
    ----------

    data : dh.DataHandler
        An instatiated data handler. It must use the same configuration
        as passed into this function
    configuration : Dict
        The result of fitting a model to a single batch
    batch : str
        The batch id to fit the titer function for

    Returns
    -------
    Callable
        A titer function, which can be passed to the setup_reactor
        function.
    """
    # Simulate to get biomaterial
    result = simulate_clone(data, configuration, batch)

    # Calculate the productivity
    data.set_cur_batch(batch)
    specific_productivity = (
        np.nanmax(data.cur_batch_data[rw.TITER])
        / result.loc[len(result) - 1][rw.BIO_MATERIAL]
    )

    # Create a function to return the specific productivity,
    # which in this case is a constant
    return _titer_function_factory(specific_productivity)


"""
Functions for simulating clones
"""


class SimulationModes(Enum):
    perfusion_optimizer = 1
    intensified_growth = 2
    centrifuge_perfusion = 3
    general_simulation = 4


def run_simulation(
    simulation_config: Dict[str, Any],
    initial_condition: Dict[str, float],
    titer_function: Callable,
    simulation_mode: SimulationModes,
):
    """Runs and returns the result in a dict.

    This funcition will call different functions depending on the selected
    simulation mode.

    Parameters
    ----------
    simulation_config : Dict[str, Any]
        The dictionary form of a fitted model extended with the
        simulation-specific keys and the bleed_pid keyes from
        the project settings
    initial_condition : Dict[str, float]
        The initial condition of the simulation
    titer_function : Callable
        The titer function fitted for this model. This function is
        expected to take three arguments
    simulation_mode : SimulationModes
        The mode of the simulation

    Returns
    -------
    Dict
        For centrifuge_perfusion the keyes of the returned dict are:
        "data" and "summary_stats".

        For intensified_growth the keyes of the returned dict are:
        "data", "duration" and "summary_stats"

        For centrifuge_perfusion the keyes of the returned dict are:
        "data" and "summary_stats"

        For perfusion_optimizer the keyes of the returned dict are:
        "vcd_target", "data", and "summary_stats"

        For general_simulation the keyes of the returned dict are:
        "data" and "summary_stats"
    """
    initial_inputs = {rw.VOLUME: 1}
    if simulation_mode == SimulationModes.perfusion_optimizer:
        return perfusion_optimizer(
            simulation_config,
            initial_condition,
            initial_inputs,
            titer_function,
        )
    elif simulation_mode == SimulationModes.intensified_growth:
        return intensified_growth(
            simulation_config, initial_condition, titer_function
        )
    elif simulation_mode == SimulationModes.centrifuge_perfusion:
        return centrifuge_perfusion(
            simulation_config,
            initial_condition,
            initial_inputs,
            titer_function,
        )
    elif simulation_mode == SimulationModes.general_simulation:
        return general_simulation(
            simulation_config,
            initial_condition,
            initial_inputs,
            titer_function,
        )


def centrifuge_perfusion(
    simulation_config, initial_state, initial_inputs, titer_function
):
    controller = controllers.Centrifuge(
        simulation_config["target_viable_cell_denisty"],
        simulation_config["media_exchange_ratio"],
    )
    reactor = setup_reactor(
        simulation_config, initial_state, initial_inputs, titer_function
    )
    result, summary_stats = simulate_reactor(
        reactor,
        simulation_config["simulation_duration"],
        controller,
        simulation_config["media_exchange_start_time"],
        simulation_config["media_exchange_frequency"],
    )

    return {"data": result.to_dict(), "summary stats": summary_stats}


def general_simulation(
    simulation_config, initial_state, initial_inputs, titer_function
):
    controller = make_constant_volume_controller(
        simulation_config["volumetric_exchange_rate"],
        simulation_config["bleed_pid_gain"],
        simulation_config["bleed_pid_tau"],
        simulation_config["target_viable_cell_denisty"],
        CONTROL_FREQUENCY,
    )

    reactor = setup_reactor(
        simulation_config, initial_state, initial_inputs, titer_function
    )
    result, summary_stats = simulate_reactor(
        reactor,
        simulation_config["simulation_duration"],
        controller,
        CONTROL_START,
        CONTROL_FREQUENCY,
    )

    return {"data": result.to_dict(), "summary stats": summary_stats}


def perfusion_optimizer(
    simulation_config, initial_state, initial_inputs, titer_function
):
    # Find the vcd target to run at
    vcd_optimizer = VcdOptimizer(
        simulation_config["target_viability"],
        simulation_config["simulation_duration"],
    )

    target_vcd = vcd_optimizer.optimize_vcd(
        simulation_config,
        initial_state,
        initial_inputs,
        CONTROL_START,
        CONTROL_FREQUENCY,
    )

    controller = make_constant_volume_controller(
        simulation_config["volumetric_exchange_rate"],
        simulation_config["bleed_pid_gain"],
        simulation_config["bleed_pid_tau"],
        target_vcd,
        CONTROL_FREQUENCY,
    )

    reactor = setup_reactor(
        simulation_config, initial_state, initial_inputs, titer_function
    )
    result, summary_stats = simulate_reactor(
        reactor,
        simulation_config["simulation_duration"],
        controller,
        CONTROL_START,
        CONTROL_FREQUENCY,
    )

    return {
        "vcd_target": target_vcd,
        "data": result.to_dict(),
        "summary stats": summary_stats,
    }


def intensified_growth(simulation_config, initial_condition, titer_function):
    cspr = simulation_config["cell_specific_exchange_rate"]
    target_vcd = simulation_config["target_viable_cell_denisty"]
    maximum_duration = simulation_config["maximum_growth_time"]

    initial_state = initial_condition
    initial_input = {rw.VOLUME: 1}

    # Get the time it takes for the reactor to reach target vcd
    reactor = setup_reactor(
        simulation_config, initial_state, initial_input, titer_function
    )
    cspr_controller = make_cspr_controller(
        cspr,
        target_vcd,
        simulation_config["bleed_pid_gain"],
        simulation_config["bleed_pid_tau"],
        CONTROL_FREQUENCY,
    )
    duration_to_target = simulate_to_target(
        reactor,
        target_vcd,
        maximum_duration,
        cspr_controller,
        CONTROL_START,
        CONTROL_FREQUENCY,
    )

    # Run a simulation until the time it took the reactor to reach the
    # target
    reactor = setup_reactor(
        simulation_config, initial_state, initial_input, titer_function
    )
    cspr_controller = make_cspr_controller(
        cspr,
        target_vcd,
        simulation_config["bleed_pid_gain"],
        simulation_config["bleed_pid_tau"],
        CONTROL_FREQUENCY,
    )
    data, summary_stats = simulate_reactor(
        reactor, duration_to_target, cspr_controller, 0, CONTROL_FREQUENCY
    )

    summary_stats["Time to target"] = [duration_to_target]

    return {
        "duration": duration_to_target,
        "data": data.to_dict(),
        "summary stats": summary_stats,
    }


def make_constant_volume_controller(
    exchange_rate: float,
    kc: float,
    taui: float,
    vcd_target: float,
    control_frequency: float,
):
    """Creates a constant volume exchange controller.

    The created CVE (constant volume exchange) controller will use a CVE
    strategy to determine the feed rate. To work, this controller needs
    reactor[rw.VOLUME] to be passed in the input to update.

    The created CVE controller will use a PI controller to determine
    harvest and bleed rates.

    Parameters
    ----------
    exchange_rate : float
        The feed rate is (exchange rate * reactor volume)
    kc : float
        The gain, shoud be taken from settings
    taui : float
        Can't remember what this is, should be taken from settings
    vcd_target : float
        Target vcd for the bleed controller to use

    Returns
    -------
    controllers.StandardController
        A constant volume exchange controller
    """
    inflow = controllers.ConstantVolume(exchange_rate)
    outflow = TriggeredPiController(-kc, taui / control_frequency)
    outflow.vcd_target = vcd_target

    return controllers.StandardController(inflow, outflow)


def make_cspr_controller(
    perfusion_rate: float,
    vcd_target: float,
    kc: float,
    taui: float,
    control_frequency: float,
):
    """Creates a cspr controller

    The created CSPR controller will use CSPR to determine the feed rate
    until it reaches the vcd target. Then it will switch to a constant
    feed rate.

    The created CSPR controller will use a PI controller to determine feed
    and bleed rates.

    Parameters
    ----------
    perfusion_rate : float
        Feed rate is perfusion_rate * viable cell density
    vcd_target : float
        The viable cell density the controller will try to reach/keep
    kc : float
        The gain, shoud be taken from settings
    taui : float
        Can't remember what this is, should be taken from settings

    Returns
    -------
    controller.StandardController
        A CSPR controller
    """

    inflow = controllers.CellSpecificPerfusionRate(
        perfusion_rate * 0.001, vcd_target
    )
    outflow = TriggeredPiController(-kc, taui / control_frequency)
    outflow.vcd_target = vcd_target

    return controllers.StandardController(inflow, outflow)


class VcdOptimizer:
    def __init__(self, target_viability: float, maximum_duration: float):
        """The VCdOptimizer is used to find the highest viable cell density
        the model can target, while still sustaining the target viability.

        Parameters
        ----------
        target_viability : float
            Viability that the reactor is not allowed to go below
        maximum_duration : float
            The time the reactor should run.
        lower_vcd : float
            The lower VCD bound on what's checked
        upper_vcd : float
            The upper VCD bound on what's checked
        """
        self.MAX_ITERATIONS = 1024
        self.TOLERANCE = 0.001

        self.maximum_duration = maximum_duration
        self.lower_vcd = LOWER_VCD_LIMIT
        self.upper_vcd = UPPER_VCD_LIMIT

        self.target_viability = target_viability

    def is_lower_vcd_reachable(
        self,
        configuration: Dict,
        initial_state: Dict,
        initial_inputs: Dict,
        controller_start: float,
        controller_frequency: float,
    ):
        """Checks if the model can sustan the viability at the lowest viable
        cell density level.

        Parameters
        ----------
        configuration : Dict
            The simulation configuration generated by fitting a model
        initial_state : Dict[str, float]
            name-value pairs of the reactor state to use as initial values. Must
            include:
            "Viable cell density",
            "Dead cell density",
            "Bio material",
            "Lysed cell density".
        initial_inputs : Dict[str, float]
            name-value pairs of the inputs to use as initial values. Must include:
            "Volume".
        controller controllers.ControllerABC
            An instantiated reactor controller
        controller_start : float
            Time when the control should be activated
        control_frequency : float
            How often the controller is called

        Returns
        -------
        bool
            True if the model can sustan the viability at the lowest viable
            cell density level.
        """
        controller = make_constant_volume_controller(
            configuration["volumetric_exchange_rate"],
            configuration["bleed_pid_gain"],
            configuration["bleed_pid_tau"],
            LOWER_VCD_LIMIT,
            CONTROL_FREQUENCY,
        )

        reactor = setup_reactor(configuration, initial_state, initial_inputs)
        result, _ = simulate_reactor(
            reactor,
            self.maximum_duration,
            controller,
            controller_start,
            controller_frequency,
        )

        if float(result[rw.VIABILITY][-1:]) < self.target_viability:
            return False

        return True

    def optimize_vcd(
        self,
        configuration: Dict,
        initial_state: Dict[str, float],
        initial_inputs: Dict[str, float],
        controller_start: float,
        controller_frequency: float,
    ):
        """Applies bisection to find the highest viable cell density the
        model can keep, while sustaining the target viability.

        If the model is unable to hold the viability at the lowest viable
        cell density, then the lowest viable cell density is returned. This
        does not mean that viable cell density is viable

        Parameters
        ----------
        configuration : Dict
            The simulation configuration generated by fitting a model
        initial_state : Dict[str, float]
            name-value pairs of the reactor state to use as initial values. Must
            include:
            "Viable cell density",
            "Dead cell density",
            "Bio material",
            "Lysed cell density".
        initial_inputs : Dict[str, float]
            name-value pairs of the inputs to use as initial values. Must include:
            "Volume".
        controller controllers.ControllerABC
            An instantiated reactor controller
        controller_start : float
            Time when the control should be activated
        control_frequency : float
            How often the controller is called

        Returns
        -------
        float
            The highest viable cell density able to maintain the viability
        """

        if not self.is_lower_vcd_reachable(
            configuration,
            initial_state,
            initial_inputs,
            controller_start,
            controller_frequency,
        ):
            return self.lower_vcd

        return self._bisect(
            configuration,
            initial_state,
            initial_inputs,
            controller_start,
            controller_frequency,
        )

    def _bisect(
        self,
        configuration: Dict,
        initial_state: Dict[str, float],
        initial_inputs: Dict[str, float],
        controller_start: float,
        controller_frequency: float,
    ):
        """Applies bisection to find the highest viable cell density the
        model can keep, while sustaining the target viability

        Parameters
        ----------
        configuration : Dict
            The simulation configuration generated by fitting a model
        initial_state : Dict[str, float]
            name-value pairs of the reactor state to use as initial values. Must
            include:
            "Viable cell density",
            "Dead cell density",
            "Bio material",
            "Lysed cell density".
        initial_inputs : Dict[str, float]
            name-value pairs of the inputs to use as initial values. Must include:
            "Volume".
        controller controllers.ControllerABC
            An instantiated reactor controller
        controller_start : float
            Time when the control should be activated
        control_frequency : float
            How often the controller is called

        Returns
        -------
        float
            The highest viable cell density able to maintain the viability
        """

        for _ in range(self.MAX_ITERATIONS):
            vcd_target = (self.lower_vcd + self.upper_vcd) / 2

            reactor = setup_reactor(
                configuration, initial_state, initial_inputs
            )
            controller = make_constant_volume_controller(
                configuration["volumetric_exchange_rate"],
                configuration["bleed_pid_gain"],
                configuration["bleed_pid_tau"],
                vcd_target,
                CONTROL_FREQUENCY,
            )

            result, _ = simulate_reactor(
                reactor,
                self.maximum_duration,
                controller,
                controller_start,
                controller_frequency,
            )

            actual_viability = float(result[rw.VIABILITY][-1:])
            if actual_viability < self.target_viability - self.TOLERANCE:
                self.upper_vcd = vcd_target
            elif actual_viability > self.target_viability + self.TOLERANCE:
                self.lower_vcd = vcd_target
            else:
                return vcd_target

        return vcd_target


def simulate_to_target(
    reactor: br.Bioreactor,
    target_vcd: float,
    maximum_duration: float,
    controller,
    controller_start: float,
    control_frequency: float,
):
    """Simulates the reactor until it has reached the target viable cell
    density, returns the time it took.

    This is the intensified growth mode.

    Parameters
    ----------
    reactor : br.Bioreactor
        The bioreactor to use in simulation
    target_vcd : float
        The target to reach
    maximum_duration : float
        The maximum time it is allowed to take
    controller controllers.ControllerABC
        An instantiated reactor controller
    controller_start : float
        Time when the control should be activated
    control_frequency : float
        How often the controller is called

    Returns
    -------
    float
        Time it took to reach target
    """

    if reactor.current_state[rw.VCD] >= target_vcd:
        return 0.0

    times = [
        control_frequency * i
        for i in range(int(maximum_duration / control_frequency))
    ]
    for start_time, end_time in zip(times[:-1], times[1:]):
        if reactor.current_state[rw.VCD] >= target_vcd:
            result = reactor.history_as_df()
            vcd_list = list(result[rw.VCD])
            time_list = list(result[rw.TIME])
            x = [0, 0]
            y = [0, 0]
            num_states = len(vcd_list)
            for i, vcd in enumerate(reversed(vcd_list)):
                if vcd >= target_vcd:
                    y[1] = vcd
                    x[1] = time_list[num_states - i - 1]
                else:
                    y[0] = vcd
                    x[0] = time_list[num_states - i - 1]

                    return _interpolate(x, y, target_vcd)

        if start_time >= controller_start:
            _, controller_inputs = controller.update(
                start_time,
                reactor.current_state,
                {rw.VOLUME: reactor.dynamics.V},
            )
            reactor.set_inputs(controller_inputs)

        reactor.run(start_time, end_time)

    return maximum_duration


def setup_reactor(
    configuration: Dict,
    initial_state: Dict[str, float],
    initial_inputs: Dict[str, float],
    titer_function: Callable = None,
) -> br.Bioreactor:
    """This function returns a virtual bioreactor, with the initial state
    configured.

    Parameters
    ----------
    clone : CloneModel
        the clone model contans the fit configuration and functions associated
        with the clone in question.
    initial_state : Dict[str, float]
        name-value pairs of the reactor state to use as initial values. Must
        include:
        "Viable cell density",
        "Dead cell density",
        "Bio material",
        "Lysed cell density".
    initial_inputs : Dict[str, float]
        name-value pairs of the inputs to use as initial values. Must include:
        "Volume".

    Returns
    -------
    Bioreactor
        an instance of a virtual bioreactor, with initial values set.
    """
    sim_profile = sim_parser._construct_result(configuration)

    reactor = br.Bioreactor(
        sim_profile.props,
        sim_profile.coeffs,
        titer_function=titer_function,
        use_lysed_cells=True,
        record_auxiliary_values=True,
    )

    reactor.set_inputs(initial_inputs)

    if rw.VIABILITY in initial_state:
        del initial_state[rw.VIABILITY]

    reactor.override_state(initial_state)

    return reactor


def simulate_reactor(
    reactor: br.Bioreactor,
    duration: float,
    controller=None,
    control_start: float = None,
    control_frequency: float = None,
) -> Tuple[pd.DataFrame, Dict[str, float]]:
    """Returns the result of running the reactor, either with or without
    a controller.

    Parameters
    ----------
    reactor : br.Bioreactor
        an instance of the Bioreactor class, with initial values set
    duration : float
        the amount of time the simulation will run
    controller : ReactorController, optional
        will be used by the simulation to update the inputs to the reactor
        while running, by default None
    control_start : float, optional
        time at which the reactor controller will start providing updates
        to the reactor, by default None
    control_frequency : float, optional
        this is the frequency at which the controller will be applied,
        by default None
    """
    if controller is not None:

        if control_start is None or control_frequency is None:
            raise TypeError(
                "Both control_start and control_frequency must be defined when a controller is used"
            )
        _simulate_controlled_reactor(
            reactor, duration, controller, control_start, control_frequency
        )

    else:
        _simulate_uncontrolled_reactor(reactor, duration)

    result = finalize_data(reactor, controller)

    stats = calculate_summary_statistics(result)
    return result.replace(np.nan, None), stats


def _simulate_controlled_reactor(
    reactor: br.Bioreactor,
    duration: float,
    controller,
    control_start: float,
    control_frequency: float,
) -> pd.DataFrame:
    """Simulate the reactor, using a controller

    Parameters
    ----------
    reactor : Bioreactor
        an instance of the Bioreactor class, with initial values set
    duration : float
        the amount of time the simulation will run
    controller : ReactorController, optional
        will be used by the simulation to update the inputs to the reactor
        while running, by default None
    control_start : float, optional
        time at which the reactor controller will start providing updates
        to the reactor, by default None
    control_frequency : float, optional
        this is the frequency at which the controller will be applied,
        by default None

    Returns
    -------
    pandas.DataFrame
        contains the result of the simulation
    """

    times = np.arange(0, duration, control_frequency)
    if times[-1] != duration:
        times = np.append(times, duration)

    for start_time, end_time in zip(times[:-1], times[1:]):
        if start_time >= control_start:
            controller_state, controller_input = controller.update(
                start_time, reactor.current_state, {rw.VOLUME: 1}
            )
            if controller_state is not None:
                reactor.override_state(controller_state)
            if controller_input is not None:
                reactor.set_inputs(controller_input)
        reactor.run(start_time, end_time)

    return reactor


def _simulate_uncontrolled_reactor(
    reactor: br.Bioreactor, duration: float, frequency: float
) -> pd.DataFrame:
    """Simulate the reactor without a controller.

    Parameters
    ----------
    reactor : Bioreactor
        an instance of the Bioreactor class, with initial values set
    duration : float
        the amount of time the simulation will run
    controller : ReactorController, optional
        will be used by the simulation to update the inputs to the reactor
        while running, by default None
    control_start : float, optional
        time at which the reactor controller will start providing updates
        to the reactor, by default None
    control_frequency : float, optional
        this is the frequency at which the controller will be applied,
        by default None

    Returns
    -------
    pd.DataFrame
        contains the result of the simulation
    """
    times = np.arange(0, duration, frequency)
    if times[-1] != duration:
        times = np.append(times, duration)

    for start_time, end_time in zip(times[:-1], times[1:]):
        reactor.run(start_time, end_time)

    return reactor


def _calculate_vpr(result: pd.DataFrame, inplace: bool = False):
    """Calculates the volumetric production rate, from the simulation
    result dataframe.

    If the input dataframe is missing either the 'rw.TITER' or the
    'rw.HARVEST' columns, then the function will return None.

    Parameters
    ----------
    result : pandas.DataFrame
        The result of a simulation
    inplace : bool, optional
        True adds a column to the dataframe and False returns a list of
        values, by default False

    Returns
    -------
    Union[List[float], None]
        The volumetric production rate, or None if inplace is False or
        a necessary column is missing.
    """
    if rw.TITER in result.columns and rw.HARVEST in result.columns:
        vpr = [t * r for t, r in zip(result[rw.TITER], result[rw.HARVEST])]

        if not inplace:
            return vpr
        result["Volumetric production rate"] = vpr
        return
    else:
        return None


def _interpolate(x, y, target):
    delta = (y[1] - y[0]) / (y[1] - target)
    return x[1] - (x[1] - x[0]) / delta
