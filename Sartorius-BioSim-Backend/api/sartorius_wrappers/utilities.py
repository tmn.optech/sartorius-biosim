import pandas as pd
import numpy as np

from typing import Dict

from bioreactormodelling.growth_model import reserved_words as rw
from bioreactormodelling.growth_model.bioreactor import Bioreactor
from bioreactormodelling.controllers.base_classes import ControllerABC


def construct_dataframe(initial_data, columns_map):
    data_frame = pd.DataFrame.from_dict(initial_data).replace("", np.nan)
    reverted_col_map = {v: k for k, v in columns_map.items()}
    data_frame.rename(columns=reverted_col_map, inplace=True)

    return data_frame


def calculate_summary_statistics(data: pd.DataFrame) -> Dict:
    """Calculates the summary stats for a simulation result

    Parameters
    ----------
    result : pd.DataFrame
        The result of a simulation
    reactor : br.Bioreactor
        The bioreactor used in the simulation

    Returns
    -------
    Dict[str, List[float]]
        A dictionary with the statistics
    """

    stats = {
        "Cell viability": [data[rw.VIABILITY].iloc[-1]],
        "Viable cell density": [data[rw.VCD].iloc[-1]],
        "Specific growth rate": [data["Specific growth rate"].iloc[-1]],
        "Specific death rate": [data["Specific death rate"].iloc[-1]],
        "Toxicity": [data["Toxicity"].iloc[-1]],
        "Inhibition": [data["Inhibition"].iloc[-1]],
        rw.BIO_MATERIAL: [data[rw.BIO_MATERIAL].iloc[-1]],
        "Lysed cell density": [data["Lysed cell density"].iloc[-1]],
    }

    if rw.TITER in data.columns:
        stats["Titer"] = [data[rw.TITER].iloc[-1]]

    if "Specific productivity" in data.columns:
        stats["Specific productivity"] = [
            data["Specific productivity"].iloc[-1]
        ]

    if "Harvest Flow" in data.columns:
        stats["Harvest Flow"] = [data["Harvest Flow"].iloc[-1]]

    if "Bleed Flow" in data.columns:
        stats["Bleed Flow"] = [data["Bleed Flow"].iloc[-1]]

    if "Feed Flow" in data.columns:
        stats["Feed Flow"] = [data["Feed Flow"].iloc[-1]]

    if "Volumetric productivity" in data.columns:
        stats["Volumetric productivity"] = [
            data["Volumetric productivity"].iloc[-1]
        ]

    return stats


def finalize_data(
    reactor: Bioreactor, controller: ControllerABC = None
) -> pd.DataFrame:
    """Takes data from a simulation and transforms it to be used in
    frontend

    Parameters
    ----------
    reactor : Bioreactor
        reactor used in the simulation
    data : pd.DataFrame
        the result of the simulation
    """
    data = _extract_data(reactor, controller)

    _drop_unserializable_columns(data)

    _add_untracked_variables(reactor, data)

    _rename_columns(data)

    return data


def _extract_data(reactor, controller) -> pd.DataFrame:

    result = reactor.history_as_df()
    if not controller:
        return result
    controller_data = controller.return_history()

    controller_columns = ["Bleed rate", "Harvest", "Feed rate"]
    try:
        result = pd.merge(result, controller_data, on=rw.TIME, how="outer")
    except KeyError:
        return result

    for column in controller_columns:
        last_good_value = 0
        for index, _ in result.iterrows():
            if np.isnan(result[column][index]):
                result.at[index, column] = last_good_value
            else:
                last_good_value = result[column][index]
    return result


def _drop_unserializable_columns(data: pd.DataFrame):
    data.drop(
        ["State growth", "Input growth", "User calculation", "User growth"],
        axis=1,
        inplace=True,
        errors="ignore",
    )


def _rename_columns(data: pd.DataFrame) -> None:
    data.rename(
        columns={
            rw.EFF_GROWTH_RATE: "Specific growth rate",
            rw.DEATH_RATE: "Specific death rate",
        },
        inplace=True,
    )
    if "Bleed rate" in data.columns:
        data.rename(columns={"Bleed rate": "Bleed Flow"}, inplace=True)
    if "Harvest" in data.columns:
        data.rename(columns={"Harvest": "Harvest Flow"}, inplace=True)
    if "Feed rate" in data.columns:
        data.rename(columns={"Feed rate": "Feed Flow"}, inplace=True)


def _add_untracked_variables(reactor: Bioreactor, data: pd.DataFrame):
    """Adds untracked parameters, such as toxicity to the dataframe

    Parameters
    ----------
    reactor : Bioreactor
        the reacted used in simulation
    data : pd.DataFrame
        the dataframe holding results of the simulation
    """

    data["Toxicity"] = _calculate_toxicity(reactor, data)
    data["Inhibition"] = _calculate_inhibition(reactor, data)

    if rw.TITER in data.columns and rw.HARVEST in data.columns:
        data[
            "Volumetric productivity"
        ] = _calculate_volumetric_production_rate(data)


def _calculate_toxicity(reactor: Bioreactor, data: pd.DataFrame) -> pd.Series:
    toxicity_rate = reactor.dynamics.coefficients.toxicity_rate
    primary_death_rate = reactor.dynamics.coefficients.primary_death_rate

    toxicity = primary_death_rate + toxicity_rate * data[rw.LYSED]
    return toxicity


def _calculate_inhibition(
    reactor: Bioreactor, data: pd.DataFrame
) -> pd.Series:
    inhibition_threshold = reactor.dynamics.state_properties[
        rw.BIO_MATERIAL
    ].inhibitor.threshold

    inhibition = 1 - 1 / (
        (data[rw.BIO_MATERIAL] / inhibition_threshold) ** 3 + 1
    )
    return inhibition


def _calculate_volumetric_production_rate(data: pd.DataFrame) -> pd.Series:
    vpr = [t * r for t, r in zip(data[rw.TITER], data[rw.HARVEST])]
    return vpr
