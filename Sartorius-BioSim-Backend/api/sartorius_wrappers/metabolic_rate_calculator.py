from sartorius_wrappers.validation import validate_and_fix_clone_data
from bioreactormodelling.growth_model import reserved_words as rw
from bioreactormodelling.metabolic_rate_calculator.metabolic_rate_calculator import (
    derive_aligned_timevec,
    calculate_aligned_metabolic_rates,
    average_metabolic_rates_over_batches,
)
from bioreactormodelling.model_configuration.fit_config_parser import (
    _construct_result as construct_fit_config,
)
import bioreactormodelling.utilities.data_handler as dh
from sartorius_wrappers.utilities import construct_dataframe

from typing import Dict, List

import numpy as np
import pandas as pd
import re


def calculate_metabolic_rates(
    data_dict: Dict,
    metabolites: List[str],
    columns_map: Dict[str, str],
    config: Dict,
    feed_concentrations=None,
):
    """Calculates the metabolic consumption rate

    Parameters
    ----------
    data_dict : Dict
        The project data
    metabolites : List[str]
        List of metabolites to calculate consumption for
    columns_map : Dict[str, str]
        Eventuaol column map to use for data
    config : Dict
        Fit configuration for the project
    feed_concentrations : [type], optional
        Feed concentration information, by default None

    Returns
    -------
    Tuple[Dict, Dict]
        Consumption rates for each batch (in single file),
        and average consumption rates for all batches
    """
    initial_data = construct_dataframe(data_dict, columns_map)
    initial_data = validate_and_fix_clone_data(initial_data, config)
    batches = initial_data["BatchID"].unique()

    consumption_data = pd.DataFrame()
    consumption_dict = dict()
    for batch in batches:
        data = initial_data.loc[initial_data["BatchID"] == batch].reset_index(
            drop=True
        )
        calculator = MetabolicRateCalculator(data, feed_concentrations)
        rate = {}
        for metabolite in metabolites:
            rate[metabolite] = calculator.calculate_for(metabolite)

        consumption_dict[batch] = _make_data_frame(rate)
        data_frame = pd.DataFrame(columns=["BatchID"])
        data_frame = data_frame.append(consumption_dict[batch])
        data_frame["BatchID"] = batch
        consumption_data = consumption_data.append(data_frame)

    consumption_data.reset_index(inplace=True, drop=True)

    data_handler = dh.DataHandler(
        initial_data, construct_fit_config(config).fit_config, {}
    )
    aligned_timevec = derive_aligned_timevec(data_handler, consumption_data)

    aligned_rates = calculate_aligned_metabolic_rates(
        data_handler, consumption_data, aligned_timevec
    )

    average_rates = average_metabolic_rates_over_batches(aligned_rates)

    return (
        consumption_data.reset_index(drop=True).replace(np.nan, "").to_dict(),
        average_rates.replace(np.nan, "").to_dict(),
    )


def _calculate_average(data: Dict[str, pd.DataFrame], metabolite):

    compiled_data = None
    for batch in data:
        batch_data = pd.DataFrame.from_dict(
            {rw.TIME: data[batch][rw.TIME], batch: data[batch][metabolite]}
        )
        if compiled_data is None:
            compiled_data = batch_data
            continue
        compiled_data = pd.merge_asof(
            compiled_data,
            batch_data,
            on=rw.TIME,
            tolerance=0.1,
            direction="nearest",
        )
    averaged_data = pd.concat(
        [
            compiled_data[rw.TIME],
            compiled_data.drop(rw.TIME, axis=1).mean(axis=1),
        ],
        axis=1,
    )
    averaged_data.rename(columns={0: metabolite}, inplace=True)
    return averaged_data


def _make_data_frame(rates):

    positions = {m: 0 for m in rates}
    data_frame = pd.DataFrame()
    while True:
        time = np.inf
        for metabolite, pos in positions.items():
            if (
                positions[metabolite] < len(rates[metabolite]["Time"])
                and rates[metabolite]["Time"][pos] < time
            ):
                time = rates[metabolite]["Time"][pos]
        if time == np.inf:
            break

        the_dict = {"Time": time}
        for metabolite, pos in positions.items():
            if positions[metabolite] == len(rates[metabolite]["Time"]):
                continue
            if rates[metabolite]["Time"][pos] == time:
                the_dict[metabolite] = [
                    rates[metabolite][metabolite + " consumption"][pos]
                ]
                positions[metabolite] += 1
            else:
                the_dict[metabolite] = [np.nan]
        data_frame = data_frame.append(pd.DataFrame.from_dict(the_dict))

    return data_frame


def _zero_order_hold(data, column):
    if rw.VOLUME not in data.columns:
        return None
    last_good_value = np.nan
    for i, value in enumerate(data[column]):
        if np.isnan(value):
            data.at[i, column] = last_good_value
        else:
            last_good_value = data[column][i]
    return None


class MetabolicRateCalculator:
    def __init__(self, data, feed_concentrations=None) -> None:
        self.data = data
        _zero_order_hold(self.data, rw.VOLUME)
        self.vcd_provider = VcdProvider(data)

        if feed_concentrations is not None:
            self.bolus_tracker = BolusFeedTracker(data, feed_concentrations)
            self.feed_tracker = ContinuousFeedTracker(
                data, feed_concentrations
            )
            self.outflow_tracker = OutflowTracker(data)
        else:
            self.bolus_tracker = None
            self.feed_tracker = None
            self.outflow_tracker = None

        self.metabolite_iterator = None
        self.calculated_rate = dict()

    def calculate_for(self, metabolite):
        self.metabolite_iterator = MetaboliteTracker(
            self.data, metabolite
        ).__iter__()
        self.calculated_rate = {rw.TIME: [], metabolite + " consumption": []}
        self.metabolite = metabolite

        for _ in range(len(self.data)):
            try:
                self._calculate_current_step()
            except StopIteration:
                break

        return self.calculated_rate

    def _calculate_current_step(self):
        # get current metabolite measurement
        current_index, _ = next(self.metabolite_iterator)

        next_index, _ = self.metabolite_iterator.peak()
        if next_index is None:
            raise StopIteration

        if self.bolus_tracker:
            bolus_addition = self.bolus_tracker.get_additions_between(
                current_index, next_index, self.metabolite
            )
            continuous_feed_addition = self.feed_tracker.get_change_between(
                current_index, next_index, self.metabolite
            )
            adjusted_metabolite = (
                self.data[self.metabolite][current_index]
                + (bolus_addition + continuous_feed_addition)
                / self.data[rw.VOLUME][next_index]
            )

            adjusted_metabolite -= (
                self.outflow_tracker.calculate_outflow_change(
                    current_index,
                    next_index,
                    self.metabolite,
                    self.bolus_tracker,
                    self.feed_tracker,
                )
                / self.data[rw.VOLUME][next_index]
            )
        else:
            adjusted_metabolite = self.data[self.metabolite][current_index]

        metabolite_change = (
            adjusted_metabolite - self.data[self.metabolite][next_index]
        )

        self.calculated_rate[self.metabolite + " consumption"].append(
            metabolite_change
            / self.vcd_provider.get_ivcd(current_index, next_index)
        )
        self.calculated_rate[rw.TIME].append(self.data[rw.TIME][current_index])


class MetaboliteTracker:
    def __init__(self, data, metabolite):
        self.data = data
        self.metabolite = metabolite

        if metabolite in self.data.columns:
            self.data = data[metabolite].loc[data[metabolite].notnull()]
            self.index = self.data.index
        else:
            raise KeyError(f"No metabolite {metabolite} in data")

        self.i = 0

    def peak(self):
        if self.i < len(self.index):
            current_index = self.index[self.i]
            return current_index, self.data[current_index]

        return None, None

    def __iter__(self):
        self.i = 0
        return self

    def __next__(self):
        try:
            current_index = self.index[self.i]
            self.i += 1
            return current_index, self.data[current_index]
        except IndexError:
            raise StopIteration


class VcdProvider:
    def __init__(self, data) -> None:
        self.data = data

        self.vcd_indicies = self._get_vcd_indicies()

    def _get_vcd_indicies(self):
        return self.data[rw.VCD].loc[self.data[rw.VCD].notnull()].to_dict()

    def get_ivcd(self, from_index, to_index):
        return (
            0.6 * self._get_interpolated_vcd(from_index)
            + 0.4 * self._get_interpolated_vcd(to_index)
        ) * (self.data[rw.TIME][to_index] - self.data[rw.TIME][from_index])

    def _get_interpolated_vcd(self, index_to_check):
        if index_to_check in self.vcd_indicies:
            return self.data[rw.VCD][index_to_check]

        previous_index = None
        for index in self.vcd_indicies:

            if index >= index_to_check:
                if previous_index is not None:
                    return self._interpolate(
                        index_to_check, previous_index, index
                    )

                raise IndexError("No vcd index to interpolate from")

            previous_index = index

        # No more VCD measurements in batch
        raise StopIteration

    def _interpolate(self, index_at, index_from, index_to):
        return self.data[rw.VCD][index_to] - (
            self.data[rw.TIME][index_to] - self.data[rw.TIME][index_at]
        ) / (self.data[rw.TIME][index_to] - self.data[rw.TIME][index_from]) * (
            self.data[rw.VCD][index_to] - self.data[rw.VCD][index_from]
        )


class BolusFeedTracker:
    def __init__(self, data, feed_concentrations) -> None:
        self.data = data
        self.feed_concentrations = feed_concentrations
        self.bolus_columns = self._get_bolus_columns()

    def _get_bolus_columns(self):
        pattern = "(?<=Bolus Feed)"
        return [
            column
            for column in self.data.columns
            if re.search(pattern, column)
        ]

    def get_addition_at(self, index, metabolite):

        bolus_added = 0
        for feed in self.bolus_columns:
            bolus_addition = (
                self.data[feed][index]
                if not np.isnan(self.data[feed][index])
                else 0
            )
            try:
                bolus_addition *= self.feed_concentrations[feed][metabolite]
            except KeyError:
                self.feed_concentrations[feed][metabolite] = 0
                bolus_addition = 0
            bolus_added += bolus_addition
        return bolus_added

    def get_additions_between(self, from_index, to_index, metabolite):
        bolus_added = 0
        for index in range(from_index, to_index):
            bolus_added += self.get_addition_at(index, metabolite)

        return bolus_added


class ContinuousFeedTracker:
    def __init__(self, data, feed_concentrations) -> None:
        self.data = data

        self.feed_concentrations = feed_concentrations
        self.feed_columns = self._get_continuous_columns()
        self.non_nan_feeds = self._get_non_nan_feeds()
        self.last_feeds = {feed: 0 for feed in self.feed_columns}

    def _get_non_nan_feeds(self):
        non_nan_feeds = dict()
        for feed in self.feed_columns:
            non_nan_feeds[feed] = self.data[feed].dropna().index
        return non_nan_feeds

    def get_change_between(self, from_index, to_index, metabolite):
        added_amount = 0
        for feed in self.feed_columns:
            for i in range(from_index, to_index):
                if not np.isnan(self.data[feed][i]):
                    self.last_feeds[feed] = self.data[feed][i]
                delta_t = self.data[rw.TIME][i + 1] - self.data[rw.TIME][i]

                try:
                    added_amount += (
                        self.feed_concentrations[feed][metabolite]
                        * self.last_feeds[feed]
                        * delta_t
                    )
                except KeyError:
                    self.feed_concentrations[feed][metabolite] = 0

        return added_amount

    def get_flow_rates_at(self, index):
        feed_rate = 0

        for feed in self.feed_columns:
            if (
                len(self.non_nan_feeds[feed]) < 1
                or index < self.non_nan_feeds[feed][0]
            ):
                continue

            smaller_than_index = [
                i for i in self.non_nan_feeds[feed] if i <= index
            ]
            feed_rate += self.data[feed][smaller_than_index[-1]]
        return feed_rate

    def _get_continuous_columns(self):
        pattern = "(?<=Continuous Feed)"
        return [
            column
            for column in self.data.columns
            if re.search(pattern, column)
        ]


class OutflowTracker:
    def __init__(self, data) -> None:
        self.data = data

    def calculate_outflow_change(
        self,
        from_index,
        to_index,
        metabolite,
        bolus_addition_tracker,
        continuous_tracker,
    ):
        bolus_addition = bolus_addition_tracker.get_additions_between(
            from_index, to_index, metabolite
        )

        slope = self._calculate_slope(
            from_index, to_index, metabolite, bolus_addition
        )

        volume = self.data[rw.VOLUME][to_index]
        pre_bolus_metabolites = []
        post_bolus_metabolites = []

        pre_bolus_metabolites = [self.data[metabolite][from_index]]
        for i in range(from_index, to_index):

            post_bolus_metabolites.append(
                pre_bolus_metabolites[-1]
                + bolus_addition_tracker.get_addition_at(i, metabolite)
                / volume
            )

            delta_t = self.data[rw.TIME][i + 1] - self.data[rw.TIME][i]
            pre_bolus_metabolites.append(
                post_bolus_metabolites[-1] + slope * delta_t
            )

        average_metabolites = []
        for pre, post in zip(
            pre_bolus_metabolites[1:], post_bolus_metabolites
        ):
            average_metabolites.append((pre + post) / 2)

        total_removed = 0
        for i, average in zip(
            range(from_index, to_index + 1), average_metabolites
        ):
            total_removed += (
                average
                * continuous_tracker.get_flow_rates_at(i)
                * (self.data[rw.TIME][i + 1] - self.data[rw.TIME][i])
            )
        return total_removed

    def _calculate_slope(
        self, from_index, to_index, metabolite, bolus_addition
    ):
        return (
            self.data[metabolite][to_index]
            - (
                self.data[metabolite][from_index]
                + bolus_addition / self.data[rw.VOLUME][to_index]
            )
        ) / (self.data[rw.TIME][to_index] - self.data[rw.TIME][from_index])
