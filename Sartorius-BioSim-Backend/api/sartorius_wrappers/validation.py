from typing import Dict
import pandas as pd
import numpy as np


VCD_TYPE_KEY = "viable_cell_density_type"
VCD_KEY = "Viable cell density"
VIABILITY_KEY = "Cell viability"
TOTAL_CELL_KEY = "Total cell density"
DEAD = "Dead cell density"
VOLUME_KEY = "Volume"
DATE_TYPE_KEY = "date_type"
TIME = "Time"


def validate_and_fix_clone_data(
    data: pd.DataFrame, config_dict: Dict[str, float]
) -> pd.DataFrame:
    data.replace("", np.nan, inplace=True)
    data.dropna(subset=config_dict["fit_variables"], inplace=True)

    if len(data.columns) != len(set(data.columns)):
        raise ValueError("Dataset contains duplicate columns after renaming")

    if VCD_TYPE_KEY not in config_dict:
        raise Exception(
            "VCD type key must be provided in the config dictionary"
        )

    if VCD_KEY not in data:
        raise Exception("Data must contain a column with VCDs")

    if TIME not in data:
        raise Exception("Data must contain a column with Times")

    vcd_type = config_dict[VCD_TYPE_KEY]

    # converts to 1E6 cells/ml
    if vcd_type == "1e6 cells/ml":
        pass
    elif vcd_type == "1e5 cells/ml":
        data[VCD_KEY] /= 10

    elif vcd_type == "cells/ml":
        data[VCD_KEY] /= 1e6

    else:
        raise Exception("Unrecognized viable cell density type provided")

    # make sure that if viability is provided, it is in a scale of 0.0 to 1.0 (ie not 0 to 100%)
    if VIABILITY_KEY in data and any(data[VIABILITY_KEY] > 5.0):
        data[VIABILITY_KEY] /= 100.0

    # make sure date is converted to days
    date_type = config_dict[DATE_TYPE_KEY]
    if date_type == "Days":
        pass
    elif date_type == "Hours":
        data[TIME] /= 24.0
    else:
        raise Exception(
            "Date type key must be provided in the config dictionary"
        )

    # make sure that we don't calculate a negative dead cell density
    if any(data[VIABILITY_KEY] > 1.0) or any(data[VIABILITY_KEY] < 0.0):
        raise Exception(
            "Viability was outside of the expected range of 0% to 100%"
        )

    # if the user provides a viability, use it to calculate the total cells and subract vcd to get dead cell density
    #  - note in this case that if the user provides a total cell density column, it won't be used
    # if the user does not provide a viability but they do provide a total cell density, use this value to back calculate viability
    if VIABILITY_KEY in data:
        data[DEAD] = data[VCD_KEY] / (data[VIABILITY_KEY]) - data[VCD_KEY]
    elif TOTAL_CELL_KEY in data:
        data[DEAD] = data[TOTAL_CELL_KEY] - data[VCD_KEY]
        data[VIABILITY_KEY] = data[VCD_KEY] / data[TOTAL_CELL_KEY]
    else:
        raise Exception(
            "Data must contain either a viable cell density or a total cell density column"
        )

    # Make sure that there is some volume column in the data so that the HSSM code library works :)
    if VOLUME_KEY not in data:
        data[VOLUME_KEY] = 1.0

    return data
