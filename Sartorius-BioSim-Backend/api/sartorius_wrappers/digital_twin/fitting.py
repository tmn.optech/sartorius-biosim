from typing import Callable, List, Optional
import pandas as pd
from bioreactormodelling.controllers.controllers import PiController
from bioreactormodelling.model_fitting.perfusion_model_fitter import (
    PerfusionModelFitter,
)
from bioreactormodelling.model_configuration.fit_config_parser import (
    _construct_result as cofigdict_to_fitconfig,
)
from bioreactormodelling.utilities.multifeed_data_handling_functions import (
    combine_feed_data,
)
from .digital_twin_tuples import BolusFeed, ContinuousFeed


def perfusion_fit(
    data: pd.DataFrame,
    fit_config: dict,
    feed_compositions: dict,
    metabolic_rates: pd.DataFrame,
    metabolic_rate_function: Optional[Callable] = None,
    titer_function: Optional[Callable] = None,
) -> dict:

    pi_controller = PiController(
        kc=fit_config["bleed_controller"]["gain"],
        tau_i=fit_config["bleed_controller"]["rest_time"],
    )
    control_freq = fit_config["bleed_controller"]["control_interval"]

    parsed_config = cofigdict_to_fitconfig(fit_config)

    # get the unique feeds from the feed composition (including types)
    feed_names = []
    for i in range(len(feed_compositions)):
        cur_feed_name = feed_compositions[i]["name"]
        if cur_feed_name not in feed_names:
            feed_names.append(cur_feed_name)

    continuous_feeds: List[ContinuousFeed] = []
    bolus_feeds: List[BolusFeed] = []

    # populate feed lists
    next_cont_feed = 0
    for feed_name in feed_names:
        feed_comps = [
            comp for comp in feed_compositions if comp["name"] == feed_name
        ]

        if feed_comps[0]["type"] == "FEED_FLOW":
            to_add = ContinuousFeed(
                name=feed_name,
                hssm_name=f"f{next_cont_feed}",
                feed_rate_column=f"Feed rate f{next_cont_feed}",
                composition={},
            )

            next_cont_feed += 1

            for comp in feed_comps:
                to_add.composition[comp["metabolite_name"]] = comp["value"]

            continuous_feeds.append(to_add)

        else:
            to_add = BolusFeed(
                name=feed_name, volume_column="dummy", composition={}
            )
            bolus_feeds.append(to_add)

    # rename columns for feed
    column_rename_map = {}
    for cur_feed in continuous_feeds:
        column_rename_map[cur_feed.name] = f"Feed rate {cur_feed.hssm_name}"

    data.rename(columns=column_rename_map, inplace=True)

    # reindex data
    data.set_index(["BatchID", "Time"], inplace=True)

    # add columns for composition
    for cur_feed in continuous_feeds:
        for metabolite in cur_feed.composition:
            data[
                f"{metabolite} feed conc {cur_feed.hssm_name}"
            ] = cur_feed.composition[metabolite]

    data = combine_feed_data(data, parsed_config.fit_config.props)

    fitter = PerfusionModelFitter(
        parsed_config.fit_config,
        metabolic_rates=metabolic_rates,
        pi_controller=pi_controller,
        bleed_control_freq=control_freq,
    )
    fit_result = {}
    for step in parsed_config.fit_procedure:
        fit_result = fitter.fit(data=data, fit_procedure_step=step)

    return fit_result.get_fit_parameters_as_dict()
