from collections import namedtuple


ContinuousFeed = namedtuple(
    "ContinuousFeed", ["name", "hssm_name", "feed_rate_column", "composition"]
)

BolusFeed = namedtuple("BolusFeed", ["name", "volume_column", "composition"])
