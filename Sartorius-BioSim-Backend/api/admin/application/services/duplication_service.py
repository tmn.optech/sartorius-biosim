"""
Services for duplicate data according to specific work flow.
"""
import uuid

from flask import current_app

from helpers.application.s3_reader import DataHandler
from helpers.infrastructure.model_serializer import ModelSerializer
from projects.application.services import RemoteStorageService
from projects.application.services.feed_service import FeedService
from projects.application.services.project_settings_service import (
    ProjectSettingsService,
)
from projects.infrastructure.models import Project
from projects.infrastructure.repository import ProjectRepository
from simulation.application.services.event_service import EventService
from simulation.application.services.initial_condition_service import (
    InitialConditionsService,
)


class ProjectDuplicationService:
    """
    Duplicate project information and all connected data.
    Include add project connected services and typing.
    """

    def __init__(
        self,
        serializer: ModelSerializer,
        remote_storage: RemoteStorageService,
        project_service: ProjectRepository,
        project_setting_service: ProjectSettingsService,
        feed_service: FeedService,
        event_service: EventService,
        initial_condition_service: InitialConditionsService,
    ):
        self.__serializer = serializer
        self.__remote_storage = remote_storage
        self.__project_repository = project_service
        self.__project_setting_service = project_setting_service
        self.__feed_service = feed_service
        self.__event_service = event_service
        self.__initial_condition_service = initial_condition_service

    def get_project_by_id(self, project_id: str) -> Project:
        """
        Proxy method of project service method.
        """
        return self.__project_repository.get_one_by_id(project_id)

    def copy_project(self, project: Project) -> Project:
        """
        Copy project with all project connected objects.
        """
        serialized_project = self.__serializer.serialize(
            project, convert_enum=False
        )
        serialized_project["id"] = uuid.uuid4()
        serialized_project["name"] = f'{serialized_project["name"]} Duplicate'
        bucket_type = current_app.config.get("STORAGE_TYPE")
        bucket_name = current_app.config.get("BUCKET_NAME")
        result = self.__remote_storage.get_file(
            bucket_type, bucket_name, project.initial_file_path
        )
        processed_data = DataHandler(result)
        new_project = Project(**serialized_project)
        self.__remote_storage.upload_file(
            bucket_type,
            bucket_name,
            new_project.initial_file_path,
            processed_data.bytes_file_content,
            processed_data.content_type,
        )
        self.__project_repository.save(new_project)
        self.copy_settings(project.project_id, new_project.project_id)
        return new_project

    def copy_settings(self, project_id: str, new_project_id: str) -> None:
        settings = self.__project_setting_service.get_project_settings_by_id(
            project_id
        )
        serialized_settings = self.__serializer.serialize(
            settings, convert_enum=False
        )
        serialized_settings["project_id"] = new_project_id
        self.__project_setting_service.create_project_settings(
            serialized_settings
        )

        fit_parameters = (
            self.__project_setting_service.get_parameters_by_project_id(
                project_id
            )
        )
        serialized_parameters = self.__serializer.serialize_list(
            fit_parameters, convert_enum=False
        )
        for fit_parameter in serialized_parameters:
            fit_parameter["id"] = uuid.uuid4()
            fit_parameter["project"] = new_project_id
        self.__project_setting_service.bulk_create_fit_parameter(
            serialized_parameters
        )

        fit_procedures = (
            self.__project_setting_service.get_procedures_by_project_id(
                project_id
            )
        )
        serialized_procedures = self.__serializer.serialize_list(
            fit_procedures, convert_enum=False
        )
        for fit_parameter in serialized_procedures:
            fit_parameter["id"] = uuid.uuid4()
            fit_parameter["project"] = new_project_id
        self.__project_setting_service.bulk_create_fit_procedure(
            serialized_procedures
        )

        feeds = self.__feed_service.get_feeds_by_project_id(project_id)
        serialized_feeds = self.__serializer.serialize_list(
            feeds, convert_enum=False
        )
        for fit_parameter in serialized_feeds:
            fit_parameter["id"] = uuid.uuid4()
            fit_parameter["project"] = new_project_id
        self.__feed_service.bulk_create_feeds(serialized_feeds)

        events = self.__event_service.get_events_by_project_id(project_id)
        serialized_events = self.__serializer.serialize_list(
            events, convert_enum=False
        )
        for fit_parameter in serialized_events:
            fit_parameter["id"] = uuid.uuid4()
            fit_parameter["project_id"] = new_project_id
        self.__event_service.bulk_create_events(serialized_events)

        conditions = self.__initial_condition_service.get_initial_condition_by_project_id(
            project_id
        )
        serialized_conditions = self.__serializer.serialize_list(
            conditions, convert_enum=False
        )
        for fit_parameter in serialized_conditions:
            fit_parameter["id"] = uuid.uuid4()
            fit_parameter["project_id"] = new_project_id
        self.__initial_condition_service.bulk_create_initial_condition(
            serialized_conditions
        )
