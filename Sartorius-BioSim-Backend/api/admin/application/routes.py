from admin.application.handlers import ProjectDuplicationHandler
from containers.handlers.admin_handlers import AdminContextHandlers


def init_admin_routes(api, app):
    handler: ProjectDuplicationHandler = (
        AdminContextHandlers.project_duplication_handler()
    )
    app.add_url_rule(
        "/api/v1/admin/duplicate/project/<project_id>",
        "copy-project",
        handler.duplicate_project,
        methods=["POST"],
    )
