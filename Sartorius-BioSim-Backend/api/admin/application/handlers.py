from admin.application.services.duplication_service import (
    ProjectDuplicationService,
)
from helpers.infrastructure import status
from helpers.infrastructure.model_serializer import ModelSerializer
from shared.response import Response
from users.application.decorators.auth_decorators import (
    jwt_requires,
    admin_permission_required,
)


class ProjectDuplicationHandler:
    """
    Handler process admin actions for duplicating projects.
    Includes serializer class to convert objects to JSON and
    duplication service. Service implements all duplication logic.
    """

    def __init__(self, serializer, duplicate_service):
        self.__serializer: ModelSerializer = serializer
        self.__duplicate_service: ProjectDuplicationService = duplicate_service

    @jwt_requires
    @admin_permission_required
    def duplicate_project(self, *args, **kwargs) -> Response:
        project_id = kwargs["project_id"]
        project = self.__duplicate_service.get_project_by_id(project_id)
        if not project:
            return Response(
                body={"error": "project does not exists"},
                status_code=status.HTTP_404_NOT_FOUND,
            ).json
        copied_project = self.__duplicate_service.copy_project(project)
        serialized_data = self.__serializer.serialize(copied_project)
        return Response(
            body=serialized_data, status_code=status.HTTP_201_CREATED
        ).json
