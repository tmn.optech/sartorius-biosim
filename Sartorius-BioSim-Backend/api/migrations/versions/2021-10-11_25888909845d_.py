"""empty message

Revision ID: 25888909845d
Revises: 07d905e260eb
Create Date: 2021-10-11 12:23:25.638881

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.orm import sessionmaker

Session = sessionmaker()


# revision identifiers, used by Alembic.
revision = '25888909845d'
down_revision = '07d905e260eb'
branch_labels = None
depends_on = None


def upgrade():
    bind = op.get_bind()
    session = Session(bind=bind)
    session.execute('''
    update project set is_rendering = false
    where is_rendering = true and date_modified < now() - interval '3 hours';
    ''')


def downgrade():
    pass
