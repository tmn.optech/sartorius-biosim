"""empty message

Revision ID: 07d905e260eb
Revises: 56ec082f6836
Create Date: 2021-10-11 11:57:27.437632

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '07d905e260eb'
down_revision = '56ec082f6836'
branch_labels = None
depends_on = None


def upgrade():
    op.drop_constraint('feed_metabolite_uc', 'feed', type_='unique')
    op.create_unique_constraint('feed_metabolite_uc', 'feed', ['name', 'metabolite_name', 'project_id'])


def downgrade():
    op.drop_constraint('feed_metabolite_uc', 'feed', type_='unique')
    op.create_unique_constraint('feed_metabolite_uc', 'feed', ['name', 'metabolite_name'])
