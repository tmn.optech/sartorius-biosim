"""empty message

Revision ID: 56ec082f6836
Revises: 2d6b8013d6f8
Create Date: 2021-09-23 15:49:17.557648

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = '56ec082f6836'
down_revision = '2d6b8013d6f8'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('feed', sa.Column('project_id', postgresql.UUID(as_uuid=True), nullable=True))
    op.add_column('feed', sa.Column('value', sa.Float(), nullable=True))
    op.add_column('feed', sa.Column('name', sa.String(length=50), nullable=False))
    op.add_column('feed', sa.Column('metabolite_name', sa.String(length=50), nullable=False))
    op.add_column('feed', sa.Column('type', sa.String(length=50), nullable=False))
    op.create_unique_constraint('feed_metabolite_uc', 'feed', ['name', 'metabolite_name'])
    op.drop_constraint('feed_project_fkey', 'feed', type_='foreignkey')
    op.create_foreign_key('feed_project_id_fkey', 'feed', 'project', ['project_id'], ['id'])
    op.drop_column('feed', 'project')
    op.drop_column('feed', 'ammonia')
    op.drop_column('feed', 'lactate')
    op.drop_column('feed', 'glutamine')
    op.drop_column('feed', 'glutamate')
    op.drop_column('feed', 'glucose')
    op.add_column('fit_parameters', sa.Column('is_additional', sa.Boolean(), server_default='false', nullable=True))


def downgrade():
    op.drop_column('fit_parameters', 'is_additional')
    op.add_column('feed', sa.Column('glucose', postgresql.DOUBLE_PRECISION(precision=53), autoincrement=False, nullable=True))
    op.add_column('feed', sa.Column('glutamate', postgresql.DOUBLE_PRECISION(precision=53), autoincrement=False, nullable=True))
    op.add_column('feed', sa.Column('glutamine', postgresql.DOUBLE_PRECISION(precision=53), autoincrement=False, nullable=True))
    op.add_column('feed', sa.Column('lactate', postgresql.DOUBLE_PRECISION(precision=53), autoincrement=False, nullable=True))
    op.add_column('feed', sa.Column('ammonia', postgresql.DOUBLE_PRECISION(precision=53), autoincrement=False, nullable=True))
    op.add_column('feed', sa.Column('project', postgresql.UUID(), autoincrement=False, nullable=True))
    op.drop_constraint('feed_project_id_fkey', 'feed', type_='foreignkey')
    op.create_foreign_key('feed_project_fkey', 'feed', 'project', ['project'], ['id'])
    op.drop_constraint('feed_metabolite_uc', 'feed', type_='unique')
    op.drop_column('feed', 'type')
    op.drop_column('feed', 'metabolite_name')
    op.drop_column('feed', 'name')
    op.drop_column('feed', 'value')
    op.drop_column('feed', 'project_id')
