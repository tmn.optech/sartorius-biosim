"""empty message

Revision ID: 7c025de60455
Revises: 7a6b748327eb
Create Date: 2021-11-13 11:52:20.592255

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '7c025de60455'
down_revision = '7a6b748327eb'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('project', sa.Column('render_started', sa.DateTime(), nullable=True))
    op.add_column('project', sa.Column('render_finished', sa.DateTime(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('project', 'render_finished')
    op.drop_column('project', 'render_started')
    # ### end Alembic commands ###
