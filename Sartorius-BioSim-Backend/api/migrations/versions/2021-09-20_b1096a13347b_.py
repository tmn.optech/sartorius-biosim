"""empty message

Revision ID: b1096a13347b
Revises: 4b46e96b82af
Create Date: 2021-09-20 11:54:27.194133

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'b1096a13347b'
down_revision = '4b46e96b82af'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('project', sa.Column('is_rendering', sa.Boolean(), server_default='false', nullable=True))


def downgrade():
    op.drop_column('project', 'is_rendering')
