"""empty message

Revision ID: 67098d86dd63
Revises: ac37ee2c8d2b
Create Date: 2021-11-08 12:52:01.925366

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = "67098d86dd63"
down_revision = "ac37ee2c8d2b"
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column(
        "feed",
        sa.Column("project", postgresql.UUID(as_uuid=True), nullable=True),
    )
    op.drop_constraint("feed_metabolite_uc", "feed", type_="unique")
    op.create_unique_constraint(
        "feed_metabolite_uc", "feed", ["name", "metabolite_name", "project"]
    )
    op.drop_constraint("feed_project_id_fkey", "feed", type_="foreignkey")
    op.create_foreign_key(
        "feed_project_id_fkey", "feed", "project", ["project"], ["id"]
    )
    op.drop_column("feed", "project_id")
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column(
        "feed",
        sa.Column(
            "project_id", postgresql.UUID(), autoincrement=False, nullable=True
        ),
    )
    op.drop_constraint("feed_project_id_fkey", "feed", type_="foreignkey")
    op.create_foreign_key(
        "feed_project_id_fkey", "feed", "project", ["project_id"], ["id"]
    )
    op.drop_constraint("feed_metabolite_uc", "feed", type_="unique")
    op.create_unique_constraint(
        "feed_metabolite_uc", "feed", ["name", "metabolite_name", "project_id"]
    )
    op.drop_column("feed", "project")
    # ### end Alembic commands ###
