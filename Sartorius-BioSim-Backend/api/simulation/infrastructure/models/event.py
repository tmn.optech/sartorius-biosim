import uuid

from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy import ForeignKey
from helpers.infrastructure.base_repository import database as db


class Event(db.Model):
    __tablename__ = "event"
    id = db.Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    project_id = db.Column(UUID(as_uuid=True), ForeignKey("project.id"))
    time = db.Column(db.Float())
    type = db.Column(db.String(255), nullable=False)

    input = db.Column(db.String(255), nullable=True)
    value = db.Column(db.Float())

    state = db.Column(db.String(255), nullable=True)

    feed = db.Column(UUID(as_uuid=True), ForeignKey("feed.id"), nullable=True)

    metabolite = db.Column(db.String(255), nullable=True)

    csrp_growth = db.Column(db.Boolean, nullable=True)
    csrp_value = db.Column(db.Float())
    volumetric_exchange_rate = db.Column(db.Float())
    target_viable_cell_dencity = db.Column(db.Float())

    exchange_ratio = db.Column(db.Float(), default=0.7)

    def __repr__(self):
        return f"<Event {self.id}>"
