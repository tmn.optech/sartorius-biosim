import uuid

from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy import ForeignKey
from helpers.infrastructure.base_repository import database as db
from simulation.domain.enums import InitialConditionGroup


class InitialCondition(db.Model):
    __tablename__ = "initial_conditions"
    id = db.Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    project_id = db.Column(UUID(as_uuid=True), ForeignKey("project.id"))
    group = db.Column(db.Enum(InitialConditionGroup), nullable=False)
    name = db.Column(db.String(55), nullable=False)
    value = db.Column(db.Float())

    def __repr__(self):
        return f"<Event {self.id}>"
