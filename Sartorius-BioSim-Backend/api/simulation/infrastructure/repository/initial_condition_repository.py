from typing import Union, List

from helpers.infrastructure.base_repository import BaseRepository
from projects.domain.project_id import ProjectID
from simulation.infrastructure.models import InitialCondition
from simulation.domain.initial_condition_id import InitialConditionID


class InitialConditionRepository(BaseRepository):
    """Class with describing communication with InitialCondition table"""

    @staticmethod
    def get_all() -> [InitialCondition]:
        """Method for getting all initial_conditions from db

        :return: list of InitialCondition instances"""

        initial_conditions = InitialCondition.query.all()
        return initial_conditions

    @staticmethod
    def get_one_by_id(
        initial_condition_id: InitialConditionID,
    ) -> Union[InitialCondition, None]:
        """Method for getting InitialCondition from db by id

        :param initial_condition_id: primary key in InitialCondition table
        :return: InitialCondition instance"""

        initial_condition = InitialCondition.query.filter_by(
            id=initial_condition_id
        ).first()
        return initial_condition

    @staticmethod
    def get_all_by_project_id(
        project_id: ProjectID,
    ) -> Union[List[InitialCondition], None]:
        """Method for getting InitialCondition from db by project id

        :param project_id: primary key in project table
        :return: InitialCondition instance"""

        initial_conditions = InitialCondition.query.filter_by(
            project_id=project_id
        )
        return initial_conditions

    def save(self, initial_condition: InitialCondition) -> None:
        """Method for adding InitialCondition in InitialCondition table

        :param initial_condition: InitialCondition entity from domain level
        :return: None"""
        self.db.session.add(initial_condition)
        try:
            self.db.session.commit()
        except Exception as e:
            self.db.session.rollback()
            raise e

    def bulk_create(self, initial_condition: [InitialCondition]) -> None:
        """Method for adding list of InitialConditions in InitialCondition table

        :param initial_condition: List of InitialCondition entity from domain level.
        :return: None"""
        self.db.session.add_all(initial_condition)
        try:
            self.db.session.commit()
        except Exception as e:
            self.db.session.rollback()
            raise e

    def update(self, condition_id: str, data: dict) -> None:
        """Update project data by event id
        :param condition_id: ID of event to be updated
        :param data: data to be updated
        :return None
        """
        InitialCondition.query.filter_by(id=condition_id).update(data)
        self.db.session.commit()

    def bulk_update(self, initial_conditions: [dict]) -> None:
        """Method for update list of InitialConditions in InitialCondition table

        :param initial_conditions: List of data to update.
        :return: None"""
        self.db.session.bulk_update_mappings(
            InitialCondition, initial_conditions
        )
        try:
            self.db.session.commit()
        except Exception as e:
            self.db.session.rollback()
            raise e

    def delete(self, initial_condition_id) -> None:
        """Method to delete InitialCondition from database"""
        try:
            InitialCondition.query.filter_by(id=initial_condition_id).delete()
            self.db.session.commit()
        except Exception as e:
            self.db.session.rollback()
            raise e
