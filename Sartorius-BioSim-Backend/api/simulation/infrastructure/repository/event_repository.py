from typing import Union, List

from helpers.infrastructure.base_repository import BaseRepository
from projects.domain.project_id import ProjectID
from simulation.infrastructure.models import Event
from simulation.domain.event_id import EventID


class EventRepository(BaseRepository):
    """Class with describing communication with Event table"""

    @staticmethod
    def get_all() -> [Event]:
        """Method for getting all events from db

        :return: list of Event instances"""

        events = Event.query.all()
        return events

    @staticmethod
    def get_one_by_id(
        event_id: EventID,
    ) -> Union[Event, None]:
        """Method for getting Event from db by id

        :param event_id: primary key in Event table
        :return: Event instance"""

        event = Event.query.filter_by(id=event_id).first()
        return event

    @staticmethod
    def get_all_by_project_id(
        project_id: ProjectID,
    ) -> Union[List[Event], None]:
        """Method for getting Event from db by project id

        :param project_id: primary key in project table
        :return: Event instance"""

        events = Event.query.filter_by(project_id=project_id)
        return events

    def save(self, event: Event) -> None:
        """Method for adding Event in Event table

        :param event: Event entity from domain level
        :return: None"""
        self.db.session.add(event)
        try:
            self.db.session.commit()
        except Exception as e:
            self.db.session.rollback()
            raise e

    def bulk_create(self, event: [Event]) -> None:
        """Method for adding list of Events in Event table

        :param event: List of Event entity from domain level.
        :return: None"""
        self.db.session.add_all(event)
        try:
            self.db.session.commit()
        except Exception as e:
            self.db.session.rollback()
            raise e

    def update(self, event_id: str, data: dict) -> None:
        """Update project data by event id
        :param event_id: ID of event to be updated
        :param data: data to be updated
        :return None
        """
        Event.query.filter_by(id=event_id).update(data)
        self.db.session.commit()

    def bulk_update(self, events: [dict]) -> None:
        """Method for update list of Events in Event table

        :param events: List of data to update.
        :return: None"""
        self.db.session.bulk_update_mappings(Event, events)
        try:
            self.db.session.commit()
        except Exception as e:
            self.db.session.rollback()
            raise e

    def delete(self, event_id) -> None:
        """Method to delete Event from database"""
        try:
            Event.query.filter_by(id=event_id).delete()
            self.db.session.commit()
        except Exception as e:
            self.db.session.rollback()
            raise e
