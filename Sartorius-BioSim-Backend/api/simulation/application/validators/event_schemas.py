class EventSchemas:
    """
    Schemas for user GET / POST requests for event entity
    """

    create_event_request = {
        "type": "object",
        "properties": {
            "project_id": {"type": "string", "maxLength": 225},
            "type": {"type": "string", "maxLength": 225},
            "time": {"type": "number"},
            "input": {"type": "string", "maxLength": 225},
            "value": {"type": "number"},
            "state": {"type": "string", "maxLength": 225},
            "metabolite": {"type": "string", "maxLength": 225},
            "csrp_growth": {"type": "boolean"},
            "csrp_value": {"type": "number"},
            "volumetric_exchange_rate": {"type": "number"},
            "target_viable_cell_dencity": {"type": "number"},
            "exchange_ratio": {"type": "number"},
            "feed": {"type": "string", "maxLength": 225},
        },
        "required": ["project_id", "type"],
        "additionalProperties": False,
    }
