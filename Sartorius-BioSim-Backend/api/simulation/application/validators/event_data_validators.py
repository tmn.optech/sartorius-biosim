from helpers.validators.request_validator import RequestValidator
from simulation.application.validators.event_schemas import EventSchemas


class EventsDataValidator:
    @staticmethod
    def event_data_create_validate(params: dict) -> dict:
        """Method validator for body parameters in post request.

        :param params: body parameters in dict representation
        :return: raise Bad Request if invalid schema or return clean params"""

        validator = RequestValidator(EventSchemas.create_event_request, params)
        validator.is_valid(raise_exception=True)
        data = validator.validated_data
        return data
