from helpers.application.enum_processor import get_names
from simulation.domain.enums import InitialConditionGroup


class InitialConditionsSchemas:
    """
    Schemas for user GET / POST requests for event entity
    """

    create_event_request = {
        "type": "object",
        "properties": {
            "project_id": {"type": "string", "max_value": 255},
            "group": {
                "type": "string",
                "enum": get_names(InitialConditionGroup),
            },
            "name": {"type": "string", "maxLength": 225},
            "value": {"type": "number"},
        },
        "required": ["group", "name", "project_id"],
    }

    bulk_create_schema = {"type": "array", "items": create_event_request}
