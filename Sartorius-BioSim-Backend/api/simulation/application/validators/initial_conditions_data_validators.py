from helpers.validators.request_validator import RequestValidator
from simulation.application.validators.initial_conditions_schemas import (
    InitialConditionsSchemas,
)


class InitialConditionsDataValidator:
    @staticmethod
    def condition_data_create_validate(params: dict) -> list:
        """Method validator for body parameters in post request.

        :param params: body parameters in dict representation
        :return: raise Bad Request if invalid schema or return clean params"""

        validator = RequestValidator(
            InitialConditionsSchemas.bulk_create_schema, params
        )
        validator.is_valid(raise_exception=True)
        data = validator.validated_data
        return data
