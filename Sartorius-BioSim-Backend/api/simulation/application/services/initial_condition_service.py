from simulation.infrastructure.models import InitialCondition
from simulation.infrastructure.repository.initial_condition_repository import (
    InitialConditionRepository,
)


class InitialConditionsService:
    def __init__(self, condition_repository: InitialConditionRepository):
        self.__condition_repository = condition_repository

    def get_initial_condition_by_project_id(
        self, project_id: str
    ) -> [InitialCondition]:
        """
        Get all initial conditions for project.
        :param project_id: current project id.
        :return: list of initial conditions.
        """
        result = self.__condition_repository.get_all_by_project_id(project_id)
        return result

    def create_initial_condition(self, data) -> InitialCondition:
        """
        Using cleaned data create new initial condition for project.
        :param data: initial condition data.
        :return: Initial condition model.
        """
        condition = InitialCondition(**data)
        self.__condition_repository.save(condition)
        return condition

    def update_initial_condition(self, condition_id, data) -> None:
        """
        Update data for specific initial condition by id.
        :param condition_id: initial condition id.
        :param data: data for update.
        :return: None.
        """
        self.__condition_repository.update(condition_id, data)

    def bulk_create_initial_condition(
        self, bulk_list: list
    ) -> [InitialCondition]:
        """
        Using cleaned data create new initial conditions for project.
        :param bulk_list: list of initial condition data to create.
        :return: list initial condition model.
        """
        data_to_create = [InitialCondition(**data) for data in bulk_list]
        self.__condition_repository.bulk_create(data_to_create)
        return data_to_create

    def bulk_update_initial_condition(self, bulk_list: [dict]) -> [dict]:
        """
        Update data for specific initial condition by id.
        :param bulk_list: initial condition id.
        :return: list initial condition model.
        """
        self.__condition_repository.bulk_update(bulk_list)
        return bulk_list
