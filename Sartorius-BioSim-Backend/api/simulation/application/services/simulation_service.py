import base64
import dill as pickle
from middlewares.error_types import (
    SartoriusWrappersError,
)
from middlewares.error_types import ProjectConfigInvalid
from bioreactormodelling.utilities import data_handler as dh
import bioreactormodelling.model_configuration.fit_config_parser as parser
from bioreactormodelling.growth_model import reserved_words as rw
from projects.application.services.model_fitting_service import (
    ModelFittingService,
)
from projects.application.services.project_settings_service import (
    ProjectSettingsService,
)
from projects.infrastructure.models import Project
from sartorius_wrappers.clone_selection import (
    run_simulation,
    simulate_clone,
    SimulationModes,
)
from sartorius_wrappers.utilities import construct_dataframe
from sartorius_wrappers.validation import validate_and_fix_clone_data


class SimulationService:
    def __init__(
        self,
        fitting_service: ModelFittingService,
        project_settings_service: ProjectSettingsService,
    ):
        self.__fitting_service = fitting_service
        self.__project_settings_service = project_settings_service

    def get_growth_kinetics(
        self, project: Project, initial_data: dict
    ) -> dict:
        """
        Method calculates growth kinetics data of fitted project.
        """
        config_dict, batches = self.__fitting_service.get_fitting_config(
            project, initial_data
        )
        fit_profile = parser._construct_result(config_dict)
        fit_config = fit_profile.fit_config
        data_frame = validate_and_fix_clone_data(
            construct_dataframe(initial_data, project.columns_map), config_dict
        )
        data_handler = dh.DataHandler(data_frame, fit_config)
        simulated_data = {}
        for batch_id in batches:
            batch_kinetic_summary = (
                project.kinetic_summary[batch_id]
                if project.kinetic_summary
                else dict()
            )
            # Load titer function
            serialized_titer = project.titer_functions.get(batch_id)
            if serialized_titer:
                serialized_titer_bytes = base64.b64decode(
                    serialized_titer["data"]
                )
                titer_function = pickle.loads(serialized_titer_bytes)
            else:
                titer_function = None
            try:
                result = simulate_clone(
                    data_handler,
                    batch_kinetic_summary,
                    batch_id,
                    titer_function,
                )
            except Exception as e:
                raise SartoriusWrappersError(str(e))
            simulated_data_dict = result.to_dict()
            simulated_data[batch_id] = simulated_data_dict
        return {
            "simulated_data": simulated_data,
            "kinetic_summary": project.kinetic_summary,
            "fit_statistics": project.fit_statistics,
        }

    def recalculate(self, project: Project) -> dict:
        task_id = self.__fitting_service.fit(project)
        return task_id

    def _replace_initial_keys(self, initial_condition: dict) -> dict:
        mapper = {
            "initial_viability": rw.VIABILITY,
            "initial_viable_cell_density": rw.VCD,
            "initial_biomaterial": rw.BIO_MATERIAL,
            "initial_lysed_cell_density": rw.LYSED,
            "dead_cell_density": rw.DEAD,
        }
        for k, v in initial_condition.items():
            if k in mapper:
                initial_condition[mapper[k]] = initial_condition[k]
                del initial_condition[k]

        return initial_condition

    def simulate(
        self,
        project: Project,
        initial_data: dict,
        initial_condition: dict,
        simulation_parameters: dict,
        simulation_mode: SimulationModes,
    ) -> dict:
        column_map = project.columns_map
        batch_id_key = column_map.get(rw.BATCH_ID)
        if not batch_id_key:
            raise ProjectConfigInvalid("Can not find batch ID column map")

        initial_condition["dead_cell_density"] = (
            initial_condition["initial_viable_cell_density"]
            / initial_condition["initial_viability"]
            - initial_condition["initial_viable_cell_density"]
        )

        project_settings = (
            self.__project_settings_service.get_project_settings_by_id(
                project.id
            )
        )
        simulation_config = dict()
        simulation_config["bleed_pid_gain"] = project_settings.dict_config[
            "bleed_controller"
        ]["gain"]
        simulation_config["bleed_pid_tau"] = project_settings.dict_config[
            "bleed_controller"
        ]["rest_time"]
        simulation_config.update(simulation_parameters)

        initial_condition = self._replace_initial_keys(initial_condition)
        result = {"data": dict(), "summary_stats": dict()}

        list_of_batches = initial_data[batch_id_key].values()
        batches_to_fit = list(map(str, set(list_of_batches)))
        for batch in batches_to_fit:
            kinetic_config = project.kinetic_summary[batch]
            serialized_titer = project.titer_functions.get(batch)
            titer_function = None
            if serialized_titer:
                serialized_titer_bytes = base64.b64decode(
                    serialized_titer["data"]
                )
                titer_function = pickle.loads(serialized_titer_bytes)
            simulation_config.update(kinetic_config)

            batch_result = run_simulation(
                simulation_config,
                initial_condition,
                titer_function,
                simulation_mode,
            )
            result["data"][batch] = batch_result["data"]
            result["summary_stats"][batch] = batch_result["summary stats"]

        return result
