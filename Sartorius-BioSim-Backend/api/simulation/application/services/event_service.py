from simulation.infrastructure.models import Event
from simulation.infrastructure.repository.event_repository import (
    EventRepository,
)


class EventService:
    def __init__(self, event_repository: EventRepository):
        self.__event_repository = event_repository

    def get_event_by_id(self, event_id: str) -> [Event]:
        """
        Using request data, get all events for project.
        :param event_id: current event id.
        :return: list of events.
        """
        result = self.__event_repository.get_one_by_id(event_id)
        return result

    def get_events_by_project_id(self, project_id: str) -> [Event]:
        """
        Using request data, get all events for project.
        :param project_id: current project id.
        :return: list of events.
        """
        result = self.__event_repository.get_all_by_project_id(project_id)
        return result

    def create_event(self, data) -> Event:
        """
        Using request data, create new project event.
        :param data: cleaned event data.
        :return: created event.
        """
        event = Event(**data)
        self.__event_repository.save(event)
        return event

    def bulk_create_events(self, bulk_list: [dict]) -> [Event]:
        """
        Using request cleaned data, create new project's events.
        :param bulk_list: request cleaned data.
        :return: list of created events.
        """
        data_to_create = [Event(**data) for data in bulk_list]
        self.__event_repository.bulk_create(data_to_create)
        return data_to_create

    def update_event(self, event_id, data) -> None:
        """
        Update specific event with cleaned data.
        :param event_id: event id.
        :param data: cleaned event data.
        :return: None.
        """
        self.__event_repository.update(event_id, data)
