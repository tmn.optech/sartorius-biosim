from containers.handlers import SimulationContextHandlers
from simulation.application.handlers import (
    SimulationHandler,
    EventsHandler,
    InitialConditionHandler,
)


def init_simulation_routes(api, app):
    handler: SimulationHandler = SimulationContextHandlers.simulation_handler()
    app.add_url_rule(
        "/api/v1/simulation/<project_id>/growth-kinetics",
        "get-project-growth-kinetics",
        handler.get_growth_kinetics,
        methods=["GET"],
    )
    app.add_url_rule(
        "/api/v1/simulation/<project_id>/growth-kinetics",
        "recalculate-growth-kinetics",
        handler.recalculate_growth_kinetics,
        methods=["POST"],
    )

    app.add_url_rule(
        "/api/v1/simulation/<project_id>",
        "run-simulation",
        handler.run_simulation,
        methods=["POST"],
    )


def init_event_routes(api, app):
    handler: EventsHandler = SimulationContextHandlers.events_handler()
    app.add_url_rule(
        "/api/v1/simulation/<project_id>/events",
        "get-project-events",
        handler.get_project_events,
        methods=["GET"],
    )

    app.add_url_rule(
        "/api/v1/simulation/events",
        "create-project-event",
        handler.post,
        methods=["POST"],
    )

    app.add_url_rule(
        "/api/v1/simulation/events/<event_id>",
        "update-project-event",
        handler.patch,
        methods=["PATCH"],
    )


def init_initial_conditions_routes(api, app):
    handler: InitialConditionHandler = (
        SimulationContextHandlers.initial_condition_handler()
    )
    app.add_url_rule(
        "/api/v1/simulation/<project_id>/initial-conditions",
        "get-project-initial-conditions",
        handler.get_project_initial_conditions,
        methods=["GET"],
    )

    app.add_url_rule(
        "/api/v1/simulation/initial-conditions/",
        "bulk-update-project-initial-conditions",
        handler.patch,
        methods=["PATCH"],
    )
    app.add_url_rule(
        "/api/v1/simulation/initial-conditions/",
        "bulk-create-project-initial-conditions",
        handler.post,
        methods=["POST"],
    )
