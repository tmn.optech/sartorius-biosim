import dill as dl
import os

from bioreactormodelling.growth_model import bioreactor as br


class CloneModel:
    def __init__(self, name, fitted_model, titer_function=None):
        self.name = name
        self.configuration = fitted_model
        self.titer_function = titer_function

    def save(self, project_name, mode=None):
        if not os.path.isdir(project_name):
            os.mkdir(project_name)

        if os.path.isfile(project_name + "/" + self.name + ".pkl"):
            if mode != "overwrite":
                raise FileExistsError(
                    "Model already exists,"
                    ' please call the method with method = "overwrite" if you want to replace the model'
                )

        with open(project_name + "/" + self.name + ".pkl", "wb") as f:
            dl.dump(self, f)


def setup_reactor(clone, initial_state, initial_inputs):
    props, coeffs = clone.configuration.get_fit_parameters()
    titer_function = clone.titer_function

    reactor = br.Bioreactor(
        props, coeffs, titer_function=titer_function, use_lysed_cells=True
    )

    reactor.set_inputs(initial_inputs)
    reactor.override_state(initial_state)

    return reactor
