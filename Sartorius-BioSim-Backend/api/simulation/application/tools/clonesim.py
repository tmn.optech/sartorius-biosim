from bioreactormodelling.growth_model import bioreactor as br
from bioreactormodelling.growth_model import reserved_words as rw
from controllers import Centrifuge
import numpy as np
import pandas as pd


def setup_reactor(clone, initial_state, initial_inputs):
    """This function returns a virtual bioreactor, with the initial state
    configured.

    Parameters
    ----------
    clone : CloneModel
        the clone model contans the fit configuration and functions associated
        with the clone in question.
    initial_state : Dict[str, float]
        name-value pairs of the reactor state to use as initial values. Must
        include:
        "Viable cell density",
        "Dead cell density",
        "Bio material",
        "Lysed cell density".
    initial_inputs : [type]
        name-value pairs of the inputs to use as initial values. Must include:
        "Volume".

    Returns
    -------
    Bioreactor
        an instance of a virtual bioreactor, with initial values set.
    """
    props, coeffs = clone.configuration.get_fit_parameters()
    titer_function = clone.titer_function

    reactor = br.Bioreactor(
        props, coeffs, titer_function=titer_function, use_lysed_cells=True
    )

    reactor.set_inputs(initial_inputs)
    reactor.override_state(initial_state)

    return reactor


def simulate_reactor(
    reactor,
    duration,
    controller=None,
    control_start=None,
    control_frequency=None,
):
    """Returns the result of running the reactor, either with or without
    a controller.

    Parameters
    ----------
    reactor : Bioreactor
        an instance of the Bioreactor class, with initial values set
    duration : float
        the amount of time the simulation will run
    controller : ReactorController, optional
        will be used by the simulation to update the inputs to the reactor
        while running, by default None
    control_start : float, optional
        time at which the reactor controller will start providing updates
        to the reactor, by default None
    control_frequency : float, optional
        this is the frequency at which the controller will be applied,
        by default None
    """
    if controller is not None:
        if control_start is None or control_frequency is None:
            raise TypeError(
                "Both control_start and control_frequency must be defined whena controller is used"
            )
        result = _simulate_controlled_reactor(
            reactor, duration, controller, control_start, control_frequency
        )
        # add controller columns
        extract_controller_data(
            result,
            duration,
            controller,
            control_start,
            control_frequency,
            inplace=True,
        )

        calculate_vpr(result, inplace=True)

    else:
        result = _simulate_uncontrolled_reactor(reactor, duration)

    calculate_kinetics(result, reactor)
    stats = make_summary_stats(result, reactor)
    return result, stats


def _simulate_controlled_reactor(
    reactor, duration, controller, control_start, control_frequency
):
    """Simulate the reactor, using a controller

    Parameters
    ----------
    reactor : Bioreactor
        an instance of the Bioreactor class, with initial values set
    duration : float
        the amount of time the simulation will run
    controller : ReactorController, optional
        will be used by the simulation to update the inputs to the reactor
        while running, by default None
    control_start : float, optional
        time at which the reactor controller will start providing updates
        to the reactor, by default None
    control_frequency : float, optional
        this is the frequency at which the controller will be applied,
        by default None

    Returns
    -------
    pandas.DataFrame
        contains the result of the simulation
    """
    times = np.arange(0, duration, control_frequency)

    for start_time, end_time in zip(times[:-1], times[1:]):

        if start_time >= control_start:
            controller.update_controls(start_time, reactor.current_state)
            controller_state, controller_input = controller.get_new_settings()
            if controller_state is not None:
                reactor.override_state(controller_state)
            if controller_input is not None:
                reactor.set_inputs(controller_input)
        reactor.run(start_time, end_time)

    return reactor.history_as_df()


def _simulate_uncontrolled_reactor(reactor, duration, frequency):
    """Simulate the reactor without a controller.

    Parameters
    ----------
    reactor : Bioreactor
        an instance of the Bioreactor class, with initial values set
    duration : float
        the amount of time the simulation will run
    controller : ReactorController, optional
        will be used by the simulation to update the inputs to the reactor
        while running, by default None
    control_start : float, optional
        time at which the reactor controller will start providing updates
        to the reactor, by default None
    control_frequency : float, optional
        this is the frequency at which the controller will be applied,
        by default None

    Returns
    -------
    pandas.DataFrame
        contains the result of the simulation
    """
    times = np.arange(0, duration, frequency)

    for start_time, end_time in zip(times[:-1], times[1:]):
        reactor.run(start_time, end_time)

    return reactor.history_as_df()


def extract_controller_data(
    simulation_result,
    duration,
    controller,
    control_start,
    control_frequency,
    inplace=False,
):

    if isinstance(controller, Centrifuge):
        # the centrifuge does not use bleed/feed/harvest
        return None

    feed_rate = []
    harvest_rate = []
    bleed_rate = []

    for j in range(len(simulation_result)):
        if 0 <= list(simulation_result[rw.TIME])[j] < control_start:
            feed_rate.append(0)
            harvest_rate.append(0)
            bleed_rate.append(0)

    time = np.arange(control_start, duration, control_frequency)[:]
    for i, (start_time, end_time) in enumerate(zip(time[:-1], time[1:])):
        for j in range(len(simulation_result)):
            if start_time <= list(simulation_result[rw.TIME])[j] < end_time:
                feed_rate.append(controller.feed_rate[i + 1])
                harvest_rate.append(controller.harvest_rate[i + 1])
                bleed_rate.append(controller.bleed_rate[i + 1])

    for j in range(len(simulation_result)):
        if list(simulation_result[rw.TIME])[j] == time[-1]:
            feed_rate.append(controller.feed_rate[-1])
            harvest_rate.append(controller.harvest_rate[-1])
            bleed_rate.append(controller.bleed_rate[-1])

    if inplace is not True:
        controller_result = pd.DataFrame()
        controller_result[rw.FEED] = feed_rate
        controller_result[rw.HAVEST] = harvest_rate
        controller_result[rw.BLEED] = bleed_rate

        return controller_result

    simulation_result[rw.FEED] = feed_rate
    simulation_result[rw.HARVEST] = harvest_rate
    simulation_result[rw.BLEED] = bleed_rate


def calculate_vpr(result, inplace=False):
    if rw.TITER in result.columns and rw.HARVEST in result.columns:
        vpr = [t * r for t, r in zip(result[rw.TITER], result[rw.HARVEST])]

        if not inplace:
            return vpr
        result["Volumetric production rate"] = vpr
        return
    else:
        return None


def calculate_kinetics(result, reactor):
    # extract kinetic information from the reactor
    kl = reactor.dynamics.coefficients.toxicity_rate
    kd = reactor.dynamics.coefficients.primary_death_rate
    inh = reactor.dynamics.state_properties[
        rw.BIO_MATERIAL
    ].inhibitor.threshold

    result["Toxicity"] = (kd + kl * result[rw.LYSED]) / kd
    result["Inhibition"] = 1 / ((result[rw.BIO_MATERIAL] / inh) ** 3 + 1)


def make_summary_stats(result, reactor):
    stats = {
        "final viability": [result[rw.VIABILITY].iloc[-1]],
        "final vcd": [result[rw.VCD].iloc[-1]],
        "primary growth rate": [reactor.dynamics.coefficients.umax],
        "primary death rate": [
            reactor.dynamics.coefficients.primary_death_rate
        ],
        "toxicity rate": [reactor.dynamics.coefficients.toxicity_rate],
        "lysing rate": [reactor.dynamics.coefficients.lysing_rate],
        "Biomaterial inhibition": [
            reactor.dynamics.state_properties[
                rw.BIO_MATERIAL
            ].inhibitor.threshold
        ],
    }

    if rw.TITER in result.columns:
        stats["final titer"] = [result[rw.TITER].iloc[-1]]

    return stats


def simulate_to_target(
    reactor,
    target_vcd,
    maximum_duration,
    controller,
    controller_start,
    control_frequency,
):

    if reactor.current_state[rw.VCD] >= target_vcd:
        return 0.0

    times = np.arange(0, maximum_duration, control_frequency)
    for start_time, end_time in zip(times[:-1], times[1:]):
        if reactor.current_state[rw.VCD] >= target_vcd:
            result = reactor.history_as_df()
            vcd_list = list(result[rw.VCD])
            time_list = list(result[rw.TIME])
            x = [0, 0]
            y = [0, 0]
            num_states = len(vcd_list)
            for i, vcd in enumerate(reversed(vcd_list)):
                if vcd >= target_vcd:
                    y[1] = vcd
                    x[1] = time_list[num_states - i - 1]
                else:
                    y[0] = vcd
                    x[0] = time_list[num_states - i - 1]

                    print(f" x: {x}, y: {y}")
                    return interpolate(x, y, target_vcd)

        if start_time >= controller_start:
            controller.update_controls(start_time, reactor.current_state)
            _, controller_inputs = controller.get_new_settings()
            reactor.set_inputs(controller_inputs)

        reactor.run(start_time, end_time)

    return maximum_duration


def interpolate(x, y, target):
    delta = (y[1] - y[0]) / (y[1] - target)
    return x[1] - (x[1] - x[0]) / delta
