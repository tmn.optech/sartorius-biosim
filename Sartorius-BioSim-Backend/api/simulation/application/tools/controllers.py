from bioreactormodelling.growth_model import reserved_words as rw


class ReactorController:
    """The ReactorController implements the functionality of a PID
    controller.

    This class is used as the base for all reactor control strategies. If
    a subclass puts all excess feed out through the harvest stream, then
    the bleed can be disabled by calling the method 'disable_bleed'. This
    will disable the bleed controller and only use feed and harvest. The
    PID can be enabled again by calling 'enable_bleed'.
    """

    def __init__(self, kc, tau_i, vcd_target=0):
        self.bleed_rate = [0]
        self.harvest_rate = [0]
        self.feed_rate = [0]
        self.feed_concentration = dict()

        self.bleed_is_disabled = False

        self.kc = kc
        self.tau_i = tau_i

        self.previous_pi_error = 0
        self.vcd_target = 0

    def update_vcd_target(self, target):
        self.vcd_target = target

    def disable_bleed(self):
        self.bleed_is_disabled = True

    def enable_bleed(self):
        self.bleed_is_disabled = False

    def update_controls(self, t, reactor_state, reactor_inputs=None):
        """Updates the controller with the current state and inputs
        from the reactor.

        To get the new state or inputs from the controller, please call
        'get_new_settings' after calling this method.

        Parameters
        ----------
        t : float
            time used to update the controller
        reactor_state : Dict[str, float]
            the reactor state used to compute the new controller settings
        reactor_inputs : Dict[str, float], optional
            the reactor inputs used to calculate new controller settings,
            by default None
        """
        self.time = t
        self.reactor_state = reactor_state
        self.reactor_inputs = reactor_inputs

        self.feed_rate.append(self.calculate_feed_rate())

        if self.bleed_is_disabled:
            self.bleed_rate.append(0)
            self.harvest_rate.append(self.feed_rate[-1])
        else:
            self.bleed_rate.append(self.calculate_bleed())
            self.harvest_rate.append(self.calculate_harvest())

        # self.feed_concentration.append()

    def calculate_harvest(self):
        return self.feed_rate[-1] - self.bleed_rate[-1]

    def set_feed_concentration(self, feed_concentration):
        self.feed_concentration = feed_concentration

    def calculate_feed_rate(self):
        return 0

    def calculate_bleed(self):
        """This method can be replaced by the user"""

        pi_error = self.reactor_state[rw.VCD] - self.vcd_target
        delta_bleed = (
            pi_error - self.previous_pi_error
        ) * self.kc + self.kc / self.tau_i * pi_error
        bleed_current = self.bleed_rate[-1] + delta_bleed
        self.previous_pi_error = pi_error
        return max(0, bleed_current)

    def get_new_settings(self):
        """This method will return the updated inputs to use by the reactor


        This function must return a dict of the variables to be updated
        """
        inputs = dict()

        inputs[rw.BLEED] = self.bleed_rate[-1]
        inputs[rw.FEED] = self.feed_rate[-1]
        inputs[rw.HARVEST] = self.harvest_rate[-1]

        for substance, concentration in self.feed_concentration.items():
            inputs[substance + rw.FEED_CONC] = concentration

        return None, inputs


class CellSpecificPerfusionRate(ReactorController):
    """This class implements the cell specific perfusion rate model of
    controlling a reactor.

    With this strategy, the rate of feeding is scaled by the VCD in
    the reactor.

    """

    def __init__(self, perfusion_rate, kc, tau_i, vcd_target=None):
        if vcd_target is None:
            super().__init__(kc, tau_i)
        else:
            super().__init__(kc, tau_i, vcd_target)

        self.perfusion_rate = perfusion_rate
        self.perfusion_mode = False

    def calculate_feed_rate(self):
        if self.perfusion_mode:
            return self.perfusion_rate * self.vcd_target
        else:
            if self.reactor_state[rw.VCD] >= self.vcd_target:
                self.perfusion_mode = True
            return self.perfusion_rate * self.reactor_state[rw.VCD]


# OK
class ConstantVolume(ReactorController):
    """This class implements a constant volume feed strategy.

    With this strategy the feed to the reactor depends only on the
    reactor's volume.
    """

    def __init__(self, exchange_rate, volume, kc, tau_i, vcd_target=None):
        if vcd_target is None:
            super().__init__(kc, tau_i)
        else:
            super().__init__(kc, tau_i, vcd_target)

        self.exchange_rate = exchange_rate
        self.volume = volume

    def calculate_feed_rate(self):
        return self.exchange_rate * self.volume


class Centrifuge(ReactorController):
    """This class mimics the effect of doing a centrifuge exchange on the
    reactor.

    Note that 'get_new_settings' in this class provides a new state,
    rather than new input settings.

    Parameters
    ----------
    ReactorController : [type]
        [description]
    """

    def __init__(self, vcd_target, exchange_ratio=1):
        if not 0 <= exchange_ratio <= 1:
            raise ValueError("The exchange rate must be between 0 and 1")

        self.vcd_target = vcd_target
        self.exchange_ratio = exchange_ratio
        self.new_reactor_state = {}

    def get_new_settings(self):
        return self.new_reactor_state, None

    def set_exchange_ratio(self, exchange_ratio):
        if not 0 <= self.exchange_ratio <= 1:
            raise ValueError("The exchange rate must be between 0 and 1")
        self.exchange_ratio = exchange_ratio

    def update_controls(self, t, reactor_state, reactor_inputs=None):
        self.new_reactor_state = dict()
        for state in reactor_state:
            if state != rw.VCD and state != rw.DEAD:
                self.new_reactor_state[state] = reactor_state[state] * (
                    1 - self.exchange_ratio
                )

        if reactor_state[rw.VCD] > self.vcd_target:
            vcd_scaling = self.vcd_target / reactor_state[rw.VCD]

            self.new_reactor_state[rw.VCD] = (
                vcd_scaling * reactor_state[rw.VCD]
            )
            self.new_reactor_state[rw.DEAD] = (
                vcd_scaling * reactor_state[rw.DEAD]
            )
