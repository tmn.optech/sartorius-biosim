"""
    Handlers to manage simulation requests.
"""
from flask import request

from helpers.infrastructure import status
from helpers.infrastructure.model_serializer import ModelSerializer
from projects.application.services import ProjectService
from shared.response import Response
from simulation.application.services.event_service import EventService
from simulation.application.services.initial_condition_service import (
    InitialConditionsService,
)
from simulation.application.services.simulation_service import (
    SimulationService,
)
from simulation.application.validators.event_data_validators import (
    EventsDataValidator,
)
from simulation.application.validators.initial_conditions_data_validators import (
    InitialConditionsDataValidator,
)
from users.application.decorators.auth_decorators import jwt_requires
from sartorius_wrappers.clone_selection import SimulationModes


class SimulationHandler:
    def __init__(
        self,
        data_service: ProjectService,
        simulation_service: SimulationService,
    ):
        self.__data_service = data_service
        self.__simulation_service = simulation_service

    @jwt_requires
    def get_growth_kinetics(self, *args, **kwargs):
        project_id = kwargs["project_id"]
        project = self.__data_service.get_project_by_id(project_id)
        initial_data = self.__data_service.get_initial_file_data(project_id)
        growth_kinetics_data = self.__simulation_service.get_growth_kinetics(
            project, initial_data
        )
        return Response(
            body=growth_kinetics_data, status_code=status.HTTP_200_OK
        ).json

    @jwt_requires
    def recalculate_growth_kinetics(self, *args, **kwargs):
        params = request.json
        project_id = kwargs["project_id"]
        project = self.__data_service.get_project_by_id(project_id)
        new_initial_data = params["data_set"]
        self.__data_service.update_initial_file_data(
            project_id, new_initial_data
        )
        response_data = self.__simulation_service.recalculate(project)
        return Response(
            body=response_data, status_code=status.HTTP_200_OK
        ).json

    @jwt_requires
    def run_simulation(self, *args, **kwargs):
        params = request.json
        project_id = kwargs["project_id"]
        project = self.__data_service.get_project_by_id(project_id)
        initial_conditions = params["initial_conditions"]
        simulation_parameters = params["simulation_parameters"]
        simulation_mode = SimulationModes(params["simulation_mode"])
        initial_data = self.__data_service.get_initial_file_data(project_id)
        response_data = self.__simulation_service.simulate(
            project,
            initial_data,
            initial_conditions,
            simulation_parameters,
            simulation_mode,
        )
        return Response(
            body=response_data, status_code=status.HTTP_200_OK
        ).json


class EventsHandler:
    """
    Handler for managing Projects events settings.
    """

    def __init__(
        self,
        validator: EventsDataValidator,
        event_service: EventService,
        serializer: ModelSerializer,
    ):
        self.__validator = validator
        self.__serializer = serializer
        self.__event_service = event_service

    @jwt_requires
    def get_project_events(self, *args, **kwargs):
        data = self.__event_service.get_events_by_project_id(
            kwargs["project_id"]
        )
        serialized_data = self.__serializer.serialize_list(data)
        return Response(
            body=serialized_data, status_code=status.HTTP_200_OK
        ).json

    @jwt_requires
    def post(self, *args, **kwargs):
        params = request.json
        cleaned_data = self.__validator.event_data_create_validate(params)
        data = self.__event_service.create_event(cleaned_data)
        serialized_data = self.__serializer.serialize(data)
        return Response(
            body=serialized_data, status_code=status.HTTP_201_CREATED
        ).json

    @jwt_requires
    def patch(self, *args, **kwargs):
        params = request.json
        cleaned_data = self.__validator.event_data_create_validate(params)
        event_id = kwargs["event_id"]
        self.__event_service.update_event(event_id, cleaned_data)
        data = self.__event_service.get_event_by_id(event_id)
        response_data = self.__serializer.serialize(data)
        return Response(
            body=response_data, status_code=status.HTTP_200_OK
        ).json


class InitialConditionHandler:
    """
    Handler for managing Projects initial conditions.
    """

    def __init__(
        self,
        validator: InitialConditionsDataValidator,
        initial_condition_service: InitialConditionsService,
        serializer: ModelSerializer,
    ):
        self.__validator = validator
        self.__serializer = serializer
        self.__initial_condition_service = initial_condition_service

    @jwt_requires
    def get_project_initial_conditions(self, *args, **kwargs):
        data = self.__initial_condition_service.get_initial_condition_by_project_id(
            kwargs["project_id"]
        )
        serialized_data = self.__serializer.serialize_list(data)
        return Response(
            body=serialized_data, status_code=status.HTTP_200_OK
        ).json

    @jwt_requires
    def post(self, *args, **kwargs):
        params = request.json
        cleaned_data = self.__validator.condition_data_create_validate(params)
        conditions = (
            self.__initial_condition_service.bulk_create_initial_condition(
                cleaned_data
            )
        )
        serialized_data = self.__serializer.serialize_list(conditions)
        return Response(
            body=serialized_data, status_code=status.HTTP_201_CREATED
        ).json

    @jwt_requires
    def patch(self, *args, **kwargs):
        params = request.json
        cleaned_data = self.__validator.condition_data_create_validate(params)
        conditions_list = (
            self.__initial_condition_service.bulk_update_initial_condition(
                cleaned_data
            )
        )
        return Response(
            body=conditions_list, status_code=status.HTTP_200_OK
        ).json
