from helpers.domain.base_id import BaseID


class InitialConditionID(BaseID):
    """Entity for representing id of initial_condition entity"""
