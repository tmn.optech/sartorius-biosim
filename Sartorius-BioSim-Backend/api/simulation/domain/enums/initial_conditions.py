from enum import Enum, unique


@unique
class InitialConditionGroup(Enum):
    CELL_METRICS = "Cell Metrics"
    METABOLITES = "Metabolites"
    INPUTS = "Inputs"
    FEEDS = "Feeds"
