from helpers.domain.base_id import BaseID


class EventID(BaseID):
    """Entity for representing id of event entity"""
