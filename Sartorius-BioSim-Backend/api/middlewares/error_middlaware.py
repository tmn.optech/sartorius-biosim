"""
Set default application handlers to process custom type of errors
"""
from flask import jsonify
from flask import request

from containers.services.application_container import AppContextContainer

from middlewares.error_types import (
    DoesNotExist,
    ProjectConfigInvalid,
    RemoteStorageError,
    ValidationError,
    SartoriusWrappersError,
)


logger = AppContextContainer.logger()


def set_default_application_handlers(app):
    @app.errorhandler(DoesNotExist)
    def handle_does_not_exist(error):
        response = jsonify(error.to_dict())
        response.status_code = error.status_code
        request.environ["STATUS"] = error.status_code
        logger.error(error.message, request.environ)
        return response

    @app.errorhandler(ProjectConfigInvalid)
    def handle_project_invalid_config(error):
        response = jsonify(error.to_dict())
        response.status_code = error.status_code
        request.environ["STATUS"] = error.status_code
        logger.error(error.message, request.environ)
        return response

    @app.errorhandler(RemoteStorageError)
    def handle_remote_storage_error(error):
        response = jsonify(error.to_dict())
        response.status_code = error.status_code
        request.environ["STATUS"] = error.status_code
        logger.error(error.message, request.environ)
        return response

    @app.errorhandler(ValidationError)
    def handle_validation_error(error):
        response = jsonify(error.to_dict())
        response.status_code = error.status_code
        request.environ["STATUS"] = error.status_code
        logger.error(error.message, request.environ)
        return response

    @app.errorhandler(SartoriusWrappersError)
    def handle_sartorius_error(error):
        response = jsonify(error.to_dict())
        response.status_code = error.status_code
        request.environ["STATUS"] = error.status_code
        logger.error(error.message, request.environ)
        return response
