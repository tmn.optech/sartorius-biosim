class BaseAppException(Exception):
    status_code = 200

    def __init__(self, message, payload=None):
        Exception.__init__(self)
        self.message = message
        self.payload = payload

    def to_dict(self):
        rv = dict(self.payload or ())
        rv["message"] = self.message
        rv["status"] = self.status_code
        return rv


class DoesNotExist(BaseAppException):
    status_code = 404


class ProjectConfigInvalid(BaseAppException):
    status_code = 400


class RemoteStorageError(BaseAppException):
    status_code = 400


class SartoriusWrappersError(BaseAppException):
    status_code = 400

    def __init__(self, message, payload=None):
        BaseAppException.__init__(self, message, payload)
        self.message = f"Sartorius wrappers error: {message}"


class ValidationError(BaseAppException):
    status_code = 400
