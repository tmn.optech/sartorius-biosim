from flask import Flask
from flask_restful import Api

from middlewares.error_middlaware import set_default_application_handlers
from config import Config
from routes import init_routes
from flask_migrate import Migrate
from flask_request_id_header.middleware import RequestID
from flask_swagger_ui import get_swaggerui_blueprint
from containers.services.application_container import AppContextContainer
from helpers.infrastructure.base_repository import database as db


def setup_logger(application: Flask):
    config = application.config.get("LOGGER", {})
    AppContextContainer.logger().configure(**config)


def setup_swagger(application: Flask):
    if application.config.get("DEBUG"):
        swagger_url = application.config.get("SWAGGER_URL")
        api_url = application.config.get("API_URL")
        swagger_blueprint = get_swaggerui_blueprint(swagger_url, api_url)
        application.register_blueprint(swagger_blueprint)


def create_app():
    # create and configure the app
    application = Flask(__name__, instance_relative_config=True)
    application.config.from_object(Config)

    # setup logger
    setup_logger(application)
    api = Api(application)
    init_routes(api, application)

    # setup request context id
    RequestID(application)
    # set up swagger
    setup_swagger(application)

    set_default_application_handlers(application)
    return application


app = create_app()

db.init_app(app)
migrate = Migrate(app, db)

if __name__ == "__main__":
    app.run()
