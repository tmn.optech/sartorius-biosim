# -*- coding: utf-8 -*-
import os
from celery import Celery
from flask import Flask
from flask_sqlalchemy import SQLAlchemy

CELERY_BROKER_URL = os.environ.get("CELERY_BROKER_URL", "redis://redis:6379")
CELERY_RESULT_BACKEND = os.environ.get(
    "CELERY_RESULT_BACKEND", "redis://redis:6379"
)
DB_TYPE = os.getenv("DB_TYPE", "postgresql")
DB_HOST = os.getenv("DB_HOST", "postgres")
DB_PORT = os.getenv("DB_PORT", 5432)
DB_NAME = os.getenv("DB_NAME", "postgres")
DB_USER = os.getenv("DB_USER", "postgres")
DB_PASSWORD = os.getenv("DB_PASSWORD", "postgres")
SQLALCHEMY_DATABASE_URI = (
    f"{DB_TYPE}://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DB_NAME}"
)


def init_app():
    app = Flask("celery_app")
    app.config["SQLALCHEMY_DATABASE_URI"] = SQLALCHEMY_DATABASE_URI
    app.config["SQLALCHEMY_ENGINE_OPTIONS"] = {
        "pool_size": 40,
        "max_overflow": 80,
        "pool_recycle": 120,
        "pool_pre_ping": True,
    }
    db = SQLAlchemy(app)
    db.init_app(app)
    return app


def init_celery():
    """Add flask app context to celery.Task"""
    app = init_app()
    celery = Celery(
        "tasks", broker=CELERY_BROKER_URL, backend=CELERY_RESULT_BACKEND
    )
    TaskBase = celery.Task

    class ContextTask(TaskBase):
        abstract = True

        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)

    celery.Task = ContextTask
    return celery
