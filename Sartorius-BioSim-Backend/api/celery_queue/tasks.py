"""
    File contains all scheduled celery tasks. Please update new task here if its required.
"""

import base64
import logging
import time
from datetime import datetime

import dill as pickle
import pandas as pd
import numpy as np

from celery_queue.worker import celery
from celery import current_task

from projects.domain.enums.creating_step import RenderStatus
from sartorius_wrappers.clone_selection import fit_clone
from sartorius_wrappers.utilities import construct_dataframe
from sartorius_wrappers.digital_twin import perfusion_fit
from projects.domain.enums import CreatingStep
from projects.infrastructure.repository import ProjectRepository

repo = ProjectRepository()
logger = logging.getLogger()


@celery.task(name="tasks.add")
def add(x: int, y: int) -> int:
    time.sleep(5)
    return x + y


@celery.task(name="tasks.fit_model")
def fit_model(
    project_id,
    initial_data: dict,
    fit_config: dict,
    columns_map: dict,
    batches_to_fit: str,
) -> dict:
    """
    Async task for model fitting.
    :param project_id: id of project that we are going to fit.
    :param initial_data: dictionary with initial data.
    :param fit_config: dictionary with fitting config.
    :param columns_map: dictionary columns name map.
    :param batches_to_fit: list of batches required to be fitted.
    """
    current_task.update_state(
        state="PROGRESS", meta={"current": 10, "total": 100}
    )
    result = {
        "creating_step": CreatingStep.GROWTH_KINETICS.name,
        "kinetic_summary": dict(),
        "titer_functions": dict(),
        "fit_statistics": dict(),
    }
    try:
        data_frame = construct_dataframe(initial_data, columns_map)
        for index, batch_id in enumerate(batches_to_fit):
            current_progress = index * 100 // len(batches_to_fit)
            current_task.update_state(
                state="PROGRESS",
                meta={"current": current_progress, "total": 100},
            )
            fitted_model, titer_function, fit_statistics = fit_clone(
                data_frame, fit_config, batch_id
            )
            result["fit_statistics"][batch_id] = fit_statistics
            if fitted_model:
                fitted_model_as_dict = (
                    fitted_model.get_fit_parameters_as_dict()
                )
                result["kinetic_summary"][batch_id] = fitted_model_as_dict
            if titer_function:
                serialized_titer_bytes = pickle.dumps(titer_function)
                serialized_titer = base64.b64encode(
                    serialized_titer_bytes
                ).decode("ascii")
                result["titer_functions"][batch_id] = {
                    "data": serialized_titer
                }
        result["render_finished"] = datetime.now()
        result["render_status"] = RenderStatus.COMPLETED.name
        repo.update(project_id, result)
    except Exception as e:
        result["render_finished"] = datetime.now()
        result["render_status"] = RenderStatus.FAILED.name
        repo.update(project_id, result)
        raise e
    current_task.update_state(
        state="PROGRESS", meta={"current": 100, "total": 100, "result": result}
    )
    return result


@celery.task(name="tasks.digital_twin_fit")
def digital_twin_fit(
    project_id,
    initial_data: dict,
    fit_config: dict,
    columns_map: dict,
    metabolite_consumption_rates: dict,
    feed_composition: dict,
) -> dict:
    """
    Async task for digital twin fitting.
    :param project_id: id of project that we are going to fit.
    :param initial_data: dictionary with initial data.
    :param fit_config: dictionary with fitting config.
    :param columns_map: dictionary columns name map.
    :param metabolite_consumption_rates: dictionary with rates data
    :param feed_composition: dictionary with feed composition data
    """
    result = {
        "fit_statistics": dict(),
        "creating_step": CreatingStep.GROWTH_KINETICS.name,
    }
    try:
        data_frame = construct_dataframe(initial_data, columns_map)
        metabolic_rates_df = pd.DataFrame.from_dict(
            metabolite_consumption_rates
        ).replace("", np.nan)
        fitting_data = perfusion_fit(
            data=data_frame,
            fit_config=fit_config,
            feed_compositions=feed_composition,
            metabolic_rates=metabolic_rates_df
            if not metabolic_rates_df.empty
            else None,
        )
        result["fit_statistics"] = fitting_data
        result["render_status"] = RenderStatus.COMPLETED.name
        result["render_finished"] = datetime.now()
        repo.update(project_id, result)
    except Exception as e:
        result["render_status"] = RenderStatus.FAILED.name
        result["render_finished"] = datetime.now()
        repo.update(project_id, result)
        raise e
    return result
