from celery_queue.application.handlers import CeleryHandler
from containers.handlers.celery_handlers import CeleryContextHandlers


def init_celery_routes(api, app):
    handler: CeleryHandler = CeleryContextHandlers.celery_handler()
    app.add_url_rule(
        "/api/v1/queue/<task_id>/status",
        "check_task",
        handler.check_task,
        methods=["GET"],
    )
    app.add_url_rule(
        "/api/v1/queue/tasks",
        "all_tasks",
        handler.all_tasks,
        methods=["GET"],
    )
