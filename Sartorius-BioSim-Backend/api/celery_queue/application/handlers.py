import celery.states as states

from celery_queue.worker import celery
from helpers.infrastructure import status
from shared.response import Response


class CeleryHandler:
    @staticmethod
    def check_task(*args, **kwargs) -> Response:
        """Endpoint for checking progress of celery tasks.

        :return: web response with task current status"""
        res = celery.AsyncResult(kwargs["task_id"])
        result = {}
        if res.state == states.PENDING:
            result["state"] = res.state
        else:
            result["state"] = res.state
            result["result"] = res.get()
        return Response(body=result, status_code=status.HTTP_200_OK).json

    @staticmethod
    def all_tasks(*args, **kwargs) -> Response:
        """Endpoint for checking progress of celery tasks.

        :return: web response with task current status"""
        i = celery.control.inspect()

        # Show tasks that are currently active.
        tasks_info = i.active().values()
        result = []
        for worker in tasks_info:
            result = [
                {"task_id": task["id"], "project_id": task["args"][0]}
                for task in worker
            ]

        return Response(body=result, status_code=status.HTTP_200_OK).json
