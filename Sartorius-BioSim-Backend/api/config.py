import os
import sys


class Config:
    """Set Flask configuration from .env file."""

    # General Config
    SECRET_KEY = os.getenv("SECRET_KEY")
    DEBUG = os.getenv("DEBUG", False)
    FLASK_ENV = os.getenv("FLASK_ENV", "production")
    REQUEST_ID_UNIQUE_VALUE_PREFIX = "sartorius-"

    # Database settings
    DB_TYPE = os.getenv("DB_TYPE", "postgresql")
    DB_HOST = os.getenv("DB_HOST", "0.0.0.0")
    DB_PORT = os.getenv("DB_PORT", 5432)
    DB_NAME = os.getenv("DB_NAME", "postgres")
    DB_USER = os.getenv("DB_USER", "postgres")
    DB_PASSWORD = os.getenv("DB_PASSWORD", "postgres")
    SQLALCHEMY_DATABASE_URI = (
        f"{DB_TYPE}://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DB_NAME}"
    )
    SQLALCHEMY_ECHO = False
    SQLALCHEMY_POOL_SIZE = 15
    SQLALCHEMY_MAX_OVERFLOW = 20
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_ENGINE_OPTIONS = {
        "pool_size": 15,
        "max_overflow": 20,
        "pool_recycle": 120,
        "pool_pre_ping": True,
    }

    # Logger settings
    LOGGER = {
        "handlers": [
            {
                "sink": sys.stdout,
                "format": "<green>{time:YYYY-MM-DD HH:mm:ss.SSS}</green> | "
                "<level>{level: <8}</level> | "
                "<cyan>{name}</cyan>:<cyan>{function}</cyan>:<cyan>{line}</cyan> | "
                "<level>{message}</level> | "
                "<yellow>correlation_id: {extra[correlation_id]} | "
                "scope: {extra[scope]} | "
                "method: {extra[method]} | "
                "status: {extra[status]} | "
                "url: {extra[url]} | "
                "user: {extra[user]}</yellow>",
                "level": "INFO",
                "colorize": True,
                "serialize": os.getenv("SERIALIZED_LOGS"),
                "diagnose": os.getenv("DEBUG"),
            }
        ],
        "extra": {
            "user": "system",
            "url": "None",
            "correlation_id": "None",
            "method": "None",
            "status": "None",
            "scope": "None",
        },
    }

    # Swagger
    SWAGGER_URL = "/api/v1/docs"
    API_URL = "/static/swagger.yaml"

    # Remote storage settings
    S3_LOCAL_HOSTED = os.getenv("S3_LOCAL_HOSTED")
    aws_settings = {
        "aws_access_key_id": os.getenv("S3_ACCESS_KEY"),
        "aws_secret_access_key": os.getenv("S3_SECRET_KEY"),
        "region_name": "us-east-1",
    }
    if S3_LOCAL_HOSTED == "True":
        AWS_STORAGE_SETTINGS = {
            "endpoint_url": "http://10.5.0.1:9000",
            "aws_access_key_id": os.getenv("S3_ACCESS_KEY"),
            "aws_secret_access_key": os.getenv("S3_SECRET_KEY"),
            "region_name": "us-east-1",
        }
    else:
        AWS_STORAGE_SETTINGS = {
            "aws_access_key_id": os.getenv("S3_ACCESS_KEY"),
            "aws_secret_access_key": os.getenv("S3_SECRET_KEY"),
            "region_name": "us-east-1",
        }
    BUCKET_NAME = os.getenv("BUCKET_NAME")
    STORAGE_TYPE = "s3"

    NUMBER_OF_PROCESSES = os.getenv("NUMBER_OF_PROCESSES", 8)
