Coding style
===============

### Custom ID
`Problem:` \
Extra logic in class constructor.

`Purpose:` \
Rid of extra logic in class constructor. \
Standardization of the use of custom ID in different project layers.

Each domain model must have definite type for id. For example, do this:
```python
from helpers.domain import Domain
from some_domain import SomeID

class Foo(Domain):
    def __init__(self):
        self.id = SomeID()

```

instead of:
```python
from helpers.domain import Domain
from some_domain import SomeID

class Foo(Domain):
    def __init__(self, id_: SomeID = None):
        self.id = id_ or SomeID()
```

### Parameters in class constructor
`Problem:` \
Implicit number of parameters in class constructor. \
Class constructor can receive an unlimited number of attributes in a parameter of type dict.

`Purpose:` \
Have a clear understanding of the number of parameters in the class constructor.

Each class constructor must have a certain number of parameters. For example, do this:
```python
class Foo:
    def __init__(self, id_, bar, baz):
        self.id = id_
        self.bar = bar
        self.baz = baz
```

instead of:
```python
class Foo:
    def __init__(self, params: dict):
        self.id = params.get("id")
        self.bar = params.get("bar")
        self.baz = params.get("baz")
```

### Related tables in repositories
`Problem:` \
from some_domain import SomeID

class Foo(Domain):
    def __init__(self):
        self.id = SomeID()

Violation of the single-responsibility principle

`Purpose:` \
Get a new tool for interacting with related tables.\
Separate logic for related tables and tables without relations.

```python
from sqlalchemy import select
from aiopg.sa.result import ResultProxy

from ...domain import SingleEntity, SingleEntityWithRelatedEntity
from .infrustructure.model import single_entity_model, related_entity_model


class SingleEntityRepository(BaseRepository):

    async def get_all(self):
        async with self.pool.acquire() as conn:
            result: ResultProxy = await conn.execute(
                single_entity_model.select()
            )
            return [SingleEntity(**dict(k)) for k in result]


class SingleEntityWithRelatedDataRepository(BaseRepository):
    async def get_all(self):
        selectable_join = single_entity_model.join(
            related_entity_model,
            single_entity_model.c.primary_id == related_entity_model.c.related_id,
        )

        query = select(
            [
                single_entity_model,
                related_entity_model.c.foo,
                related_entity_model.c.bar,
            ]
        ).select_from(selectable_join)

        async with self.pool.acquire() as conn:
            result: ResultProxy = await conn.execute(query)
            return [SingleEntityWithRelatedEntity(**dict(row)) for row in result]

```

instead of:
```python
from sqlalchemy import select
from aiopg.sa.result import ResultProxy

from ...domain import KycRequest, SingleEntityWithRelatedEntity
from .infrustructure.model import single_entity_model, related_entity_model


class SingleEntityRepository(BaseRepository):

    async def get_all(self):
        selectable_join = single_entity_model.join(
            related_entity_model,
            single_entity_model.c.primary_id == related_entity_model.c.related_id,
        )

        query = select(
            [
                single_entity_model,
                related_entity_model.c.foo,
                related_entity_model.c.bar,
            ]
        ).select_from(selectable_join)

        async with self.pool.acquire() as conn:
            result: ResultProxy = await conn.execute(query)
            return [SingleEntityWithRelatedEntity(**dict(row)) for row in result]
```


### Overloaded context container services
`Problem:` \
Extra dependencies in context containers. \
We can use the context container in several places of the project, 
and for different tasks we may need different dependencies. \
In this case we may get overloaded container with redundant dependencies that may not be used.


`Purpose:` \
Separate service containers for definite context with a minimum number of dependencies.
Move mix of different types of domains in handler containers.

```python
class TransactionsContextContainer(containers.DeclarativeContainer):
    transactions_validators = providers.Singleton(TransactionsValidators)
    transaction_repository = providers.Singleton(
        TransactionRepository, storage_client
    )
```
```python
class TransactionsContextHandlers(containers.DeclarativeContainer):

    transactions_validators = (
        TransactionsContextContainer.transactions_validators
    )
    transaction_data_service = (
        TransactionsContextContainer.transaction_data_service
    )
    loan_withdrawal_service = (
        TransactionsContextContainer.loan_withdrawal_service
    )
    loans_service = LoanContextContainer.loans_service
```

instead of:
```python
class TransactionsContextContainer(containers.DeclarativeContainer):
    transactions_validators = providers.Singleton(TransactionsValidators)
    transaction_repository = providers.Singleton(
        TransactionRepository, storage_client
    )
    loan_withdrawal_service = (
        TransactionsContextContainer.loan_withdrawal_service
    )
    loans_service = LoanContextContainer.loans_service
```

### Service layer implementation
As a standard way of implementing Domain / Application service we follow rules described below:

* Service should NOT inject other servise with repositories injected
* Each service should have its own (single) repository service
* In case we need to work with multiple domain objects we can:
    * Prepare domain instances in application controller and pass them via parameters
    * Inject dependent repository into main service repository
* HandlerContextContainers should be separated from DomainContextContainer
* Each service should include and operate only with domain models that created for 
specific business tasks.
* All services methods should operate with domain classes and infrostructure level classes.
Do not use standard python data types.
```python
class UsersService:
    """ Class provides methods to work with users data. """

    def __init__(
        self,
        user_repository: UsersRepository,
        profile_repository: UsersProfileRepository,
        token_provider: TokenProvider,
    ):
...
    async def get_user_by_id(self, user_id: UserID) -> User:
        """Method for getting user by user id from db. Validation of user
        exists in db should be made earlier.

        :param user_id: UserID entity from domain level
        :return: User instance"""

        return await self.__user_repository.get_one_by_id(user_id)
```


instead of:
```python
class UsersService:
    """ Class provides methods to work with users data. """

    def __init__(
        self,
        user_repository: UsersRepository,
        profile_repository: UsersProfileRepository,
        loan_repository: LoanRepository,
        deposit_data_provider: DepositDataProvider
    ):
...
    async def get_user_by_id(self, user_id: str) -> dict():
        """Method for getting user by user id from db. Validation of user
        exists in db should be made earlier.

        :param user_id: UserID entity from domain level
        :return: User instance"""

        return await self.__user_repository.get_one_by_id(user_id)
```
