## Requirements
- Linux instance (ubuntu 20.04LTS)
- docker - https://docs.docker.com/engine/install/ubuntu/
- docker-compose - https://docs.docker.com/compose/install/
- git

### START Project
```
docker-compose up -d 
```

### Open url
```
http://localhost/
```
