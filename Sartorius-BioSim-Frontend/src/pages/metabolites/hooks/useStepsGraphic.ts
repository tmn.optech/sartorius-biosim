import { useBatchNames } from './useBatchNames';
import { useParams } from 'react-router-dom';
import { useProjectMetabolites } from '../../../queries/projects';
import { useMemo } from 'react';

const addCorners = (data: { x: number; y: number }[]) => {
  const final: { x: number; y: number }[] = [];
  data.forEach((item, idx) => {
    if (idx !== 0) {
      final.push({
        y: item.y,
        x: data[idx - 1].x
      });
    }
    final.push(item);
  });
  return final;
};
export const useStepsGraphic = (selectedMetabolite: string) => {
  const batchNames = useBatchNames(selectedMetabolite);
  const { projectId } = useParams<{ projectId: string }>();
  const metabolites =
    useProjectMetabolites(projectId).data?.data.metabolic_rates ?? [];
  return useMemo(
    () =>
      batchNames.map((batchName) => {
        const dotsOfBatch = metabolites
          .filter((m: any) => m.BatchID === batchName)
          .map((metabolite: any) => {
            return {
              x: metabolite.Time,
              y: metabolite[selectedMetabolite]
            };
          });
        return addCorners(dotsOfBatch).map(({ x, y }) => ({
          Time: x,
          [selectedMetabolite]: y
        }));
      }),
    [batchNames, metabolites, selectedMetabolite]
  );
};
