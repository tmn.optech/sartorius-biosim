import { useMemo } from 'react';
import { useBatchNames } from './useBatchNames';
import {
  EXCLUDED_COLOR,
  REGULAR_COLOR,
  SELECTED_COLOR
} from '../../../components/Graphic/constants';
import { GraphicLine } from '../../../components';

export const useLines = (
  selectedMetabolite: string,
  selectedBatches: string[],
  excludedBatches: string[]
) => {
  const batchNames = useBatchNames(selectedMetabolite);
  return useMemo(
    () =>
      (
        batchNames.map((batchName) => ({
          dataKey: batchName,
          name: `${selectedMetabolite}(${batchName})`,
          stroke: excludedBatches.includes(batchName)
            ? EXCLUDED_COLOR
            : selectedBatches.includes(batchName)
            ? SELECTED_COLOR
            : REGULAR_COLOR
        })) as GraphicLine[]
      ).concat({
        dataKey: 'average',
        name: `${selectedMetabolite}(average)`,
        stroke: '#000000',
        strokeWidth: 0.5
      }),
    [batchNames, excludedBatches, selectedBatches, selectedMetabolite]
  );
};
