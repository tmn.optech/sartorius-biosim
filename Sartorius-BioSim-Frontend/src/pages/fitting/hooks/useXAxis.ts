import { useMemo } from 'react';

export const useXAxis = () =>
  useMemo(
    () => ({
      dataKey: 'Time',
      label: 'Time'
    }),
    []
  );
