import { useMemo } from 'react';
import {
  EXCLUDED_COLOR,
  SELECTED_COLOR
} from '../../../components/Graphic/constants';

const PREDICTED_COLOR = '#BDBDBD';

export const useLines = (
  batchNames: string[],
  selectedBatches: string[],
  excludedBatches: string[]
) =>
  useMemo(
    () =>
      batchNames
        .map((batchName) => ({
          dataKey: batchName,
          name: batchName,
          onlyDots: true
        }))
        .concat(
          batchNames.map(
            (batchName) =>
              ({
                dataKey: `predicted${batchName}`,
                name: 'Prediction',
                stroke: excludedBatches.includes(batchName)
                  ? EXCLUDED_COLOR
                  : selectedBatches.includes(batchName)
                  ? SELECTED_COLOR
                  : PREDICTED_COLOR,
                onlyLine: true
              } as any)
          )
        ),
    [batchNames, excludedBatches, selectedBatches]
  );
