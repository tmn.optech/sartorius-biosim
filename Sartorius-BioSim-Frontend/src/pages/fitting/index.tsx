import React, { useCallback, useEffect, useMemo, useState } from 'react';

import s from './model-configuring.module.scss';
import { Header } from '../../components/Header/Header';
import {
  Graphic,
  Heading,
  HeadingVariant,
  SelectedPoint
} from '../../components';
import { Button, ButtonVariant } from '../../components/Button/Button';
import { ReactComponent as SaveAndContinueIcon } from './assets/save-and-continue.svg';
import { Paths } from '../../routes/paths';
import { useHistory, useParams } from 'react-router-dom';
import { mapRowsToObject } from '../projects';
import { ProjectBreadcrumbs } from '../project-settings';
import { useInitialData, useProject } from '../../queries/projects';
import {
  useGrowthKinetics,
  useUpdateGrowthKinetics
} from '../../queries/simulation';
import { LastSaved } from '../model-configuring';
import { useRenderTooltip } from './hooks/useRenderTooltip';
import { useXAxis } from './hooks/useXAxis';
import { useLines } from './hooks/useLines';
import { MultiSelect } from '../../components/MultiSelect/MultiSelect';
import { useProjectSettings } from '../../queries/projectSettings';

const Info = ({
  fields,
  title
}: {
  title: string;
  fields: { name: string; value: string | number }[];
}) => {
  return (
    <div className={s.Info}>
      <div className={s.Info__title}>{title}</div>
      <div className={s.Info__content}>
        {fields.map((field) => {
          return (
            <div key={field.name} className={s.Info__item}>
              <div className={s.Info__itemName}>{field.name}</div>
              <div className={s.Info__itemValue}>{field.value}</div>
            </div>
          );
        })}
      </div>
    </div>
  );
};

const addColumnValue = (
  prepared: any[],
  mappedSimulatedData: { batchName: string; data: {}[] }[],
  columnName: string
) => {
  return prepared
    .map((row, idx) => {
      return {
        ...row,
        [row.BatchID]: row[columnName]
      };
    })
    .concat(
      mappedSimulatedData.reduce((acc, { batchName, data: rows }) => {
        return acc.concat(
          rows.map((row) => ({
            ...row,
            [`predicted${batchName}`]: (row as any)[columnName]
          }))
        );
      }, [] as any[])
    )
    .map((row) => ({
      ...row,
      [row.BatchID]: row[columnName] || null
    }))
    .sort((a, b) => a.Time - b.Time);
};

const formatIfNumber = (
  value: string | number | undefined | null,
  nums = 4
) => {
  if (typeof value === 'string' || typeof value === 'number') {
    const isValid = isFinite(parseFloat(value.toString()));
    if (isValid) {
      return parseFloat(value.toString()).toFixed(nums);
    }
  }
  return '-';
};

const computeExcludedPointsToGraphic = (
  graphicData: { BatchID: string; Time: number; [key: string]: any }[],
  excludedPoints: SelectedPoint<string>[],
  field: string
): SelectedPoint<string>[] => {
  return excludedPoints.map((point) => {
    const graphicPoint = graphicData.find(
      (gp) => gp.BatchID === point.dataKey && gp.Time === point.x
    );

    return {
      dataKey: point.dataKey,
      id: point.id,
      x: graphicPoint?.Time || null,
      y: graphicPoint?.[field] || null
    };
  });
};

const computeSelectedPointToGraphic = (
  graphicData: { BatchID: string; Time: number; [key: string]: any }[],
  selectedPoint: SelectedPoint<string> | null,
  field: string
) => {
  if (!selectedPoint) return null;
  const graphicPoint = graphicData.find(
    (gp) => gp.BatchID === selectedPoint.dataKey && gp.Time === selectedPoint.x
  );
  if (!graphicPoint) return null;
  return {
    dataKey: graphicPoint.dataKey,
    id: graphicPoint.id,
    x: graphicPoint?.Time || null,
    y: graphicPoint?.[field] || null
  };
};

const columns = {
  batchId: 'BatchID',
  viableCellDensity: 'Viable cell density',
  viability: 'Cell viability',
  time: 'Time',
  titer: 'Titer'
} as const;

const columnsWeights: { [key: string]: number } = {
  Titer: 8,
  Inhibition: 7,
  Toxicity: 6,
  'Specific growth rate': 5,
  'Specific death rate': 4,
  'Specific productivity': 3,
  'Bio material': 2,
  'Lysed cell density': 1
};
export const FittingPage = () => {
  const { projectId } = useParams<{ projectId: string }>();
  const project = useProject(projectId);
  const projectSettings = useProjectSettings(projectId);
  const initialData = useInitialData(projectId)?.data;
  const columnsMap = useMemo(() => {
    const BatchID =
      project.project?.columns_map?.[columns.batchId] || columns.batchId;
    const viableCellDensity =
      project.project?.columns_map?.[columns.viableCellDensity] ||
      columns.viableCellDensity;
    const viability =
      project.project?.columns_map?.[columns.viability] || columns.viability;
    const time = project.project?.columns_map?.[columns.time] || columns.time;
    const titer =
      project.project?.columns_map?.[columns.titer] || columns.titer;
    return {
      BatchID,
      viableCellDensity,
      viability,
      time,
      titer
    };
  }, [project.project?.columns_map]);
  const mapInitialColumnNamesToSystemColumnNames = useCallback(
    (data: object[]) => {
      return (data || []).map((id: any) => {
        return {
          ...id,
          [columns.batchId]: id[columnsMap.BatchID],
          [columns.time]: id[columnsMap.time],
          [columns.viableCellDensity]: id[columnsMap.viableCellDensity],
          [columns.viability]: id[columnsMap.viability],
          [columns.titer]: id[columnsMap.titer]
        };
      });
    },
    [columnsMap]
  );
  const mappedInitialData = useMemo(
    () => mapInitialColumnNamesToSystemColumnNames(initialData || []),
    [initialData, mapInitialColumnNamesToSystemColumnNames]
  );

  const batchNames = useMemo(() => {
    const set = new Set(
      mappedInitialData.map((row: any) => {
        return row[columnsMap.BatchID];
      })
    );
    const ret: string[] = [];
    set.forEach((v) => ret.push(v.toString()));
    return ret;
  }, [columnsMap.BatchID, mappedInitialData]);

  const [selectedInSelectorBatch, setSelectedInSelectorBatch] = useState<
    string | undefined
  >(batchNames[0]);

  const { data: fittingData } = useGrowthKinetics({
    projectId
  });

  useEffect(() => {
    if (selectedInSelectorBatch === undefined) {
      setSelectedInSelectorBatch(batchNames[0]);
    }
  }, [batchNames, selectedInSelectorBatch]);
  const mappedSimulatedData = useMemo(() => {
    return fittingData?.simulated_data || [];
  }, [fittingData]);

  const mappedInitialDataVariablesNames = useMemo(() => {
    return Object.keys(
      mappedInitialData.reduce((acc, row) => ({ ...acc, ...row }), {})
    );
  }, [mappedInitialData]);

  const mappedFittingDataVariablesNames = useMemo(() => {
    return Object.keys(
      mappedSimulatedData.reduce(
        (acc, row) => ({
          ...acc,
          ...row.data.reduce((acc2, row2) => ({ ...acc2, ...row2 }), {})
        }),
        {}
      )
    );
  }, [mappedSimulatedData]);

  const variablesNames = useMemo(
    () =>
      Object.keys(
        mappedInitialDataVariablesNames
          .concat(mappedFittingDataVariablesNames)
          .reduce((acc, variable) => ({ ...acc, [variable]: true }), {})
      )
        .sort((a, b) => {
          const aWeight = columnsWeights[a] || 0;
          const bWeight = columnsWeights[b] || 0;
          return aWeight - bWeight;
        })
        .reverse()
        .filter((s) => {
          if (
            [
              columnsMap.BatchID,
              'BatchID',
              'Time',
              columnsMap.time,
              columnsMap.viableCellDensity,
              'Viable cell density',
              'Cell viability',
              columnsMap.viability
            ].includes(s)
          ) {
            return false;
          }
          return true;
        }),
    [
      mappedInitialDataVariablesNames,
      mappedFittingDataVariablesNames,
      columnsMap.BatchID,
      columnsMap.time,
      columnsMap.viableCellDensity,
      columnsMap.viability
    ]
  );
  console.log('>>> variablesNames:', variablesNames);

  // FIXME: not string type in <>
  const [selectedPoint, onSelectPoint] = useState<SelectedPoint<string> | null>(
    null
  );
  const [selectedPoints, onChangeSelectedPoints] = useState<
    SelectedPoint<string>[]
  >([]);
  const [selectedBatches, onChangeSelectedBatches] = useState<string[]>([]);
  const [excludedPoints, onChangeExcludedPoints] = useState<
    SelectedPoint<string>[]
  >([]);
  const [excludedBatches, onChangeExcludedBatches] = useState<string[]>([]);

  const metabolites = useMemo(() => {
    return (
      projectSettings.data?.fitParameters.data.filter(
        (d) => d.role === 'Metabolite'
      ) || []
    );
  }, [projectSettings.data]);
  const [selectedMetabolite, setSelectedMetabolite] = useState(
    metabolites[0]?.name
  );
  useEffect(() => {
    setSelectedMetabolite(metabolites[0]?.name);
  }, [metabolites]);

  const viableCellDensity = useMemo(
    () =>
      addColumnValue(
        mappedInitialData,
        mappedSimulatedData,
        columns.viableCellDensity
      ),
    [mappedInitialData, mappedSimulatedData]
  );

  const viability = useMemo(
    () =>
      addColumnValue(mappedInitialData, mappedSimulatedData, columns.viability),
    [mappedInitialData, mappedSimulatedData]
  );

  const titer = useMemo(
    () => addColumnValue(mappedInitialData, mappedSimulatedData, columns.titer),
    [mappedInitialData, mappedSimulatedData]
  );
  const metabolite = useMemo(
    () =>
      addColumnValue(
        mappedInitialData,
        mappedSimulatedData,
        selectedMetabolite
      ),
    [mappedInitialData, mappedSimulatedData, selectedMetabolite]
  );
  const [selected1, setSelected1] = useState('');
  const variableSelector1 = useMemo(() => {
    if (!selected1) return [];
    return addColumnValue(mappedInitialData, mappedSimulatedData, selected1);
  }, [mappedSimulatedData, mappedInitialData, selected1]);
  const [selected2, setSelected2] = useState('');
  const variableSelector2 = useMemo(() => {
    if (!selected2) return [];
    return addColumnValue(mappedInitialData, mappedSimulatedData, selected2);
  }, [mappedSimulatedData, mappedInitialData, selected2]);
  const [selected3, setSelected3] = useState('');
  const variableSelector3 = useMemo(() => {
    if (!selected3) return [];
    return addColumnValue(mappedInitialData, mappedSimulatedData, selected3);
  }, [mappedSimulatedData, mappedInitialData, selected3]);

  const history = useHistory();

  const [isLoading, setLoading] = useState(false);

  const { mutateAsync: mutateGrowthKinetics } = useUpdateGrowthKinetics();
  const handleRecalculate = useCallback(() => {
    setLoading(true);

    const columnsMap = project.project?.columns_map || null;
    const notTransformed = mappedInitialData
      .filter((row) => {
        if (excludedBatches.includes(row.BatchID)) {
          return false;
        }
        if (
          excludedPoints.find(
            (point) => point.dataKey === row.BatchID && point.x === row.Time
          )
        ) {
          return false;
        }
        return true;
      })
      .map((row) => {
        if (!columnsMap) {
          return row;
        }
        if (columnsMap[columns.viability] !== columns.viability) {
          delete row[columns.viability];
        }
        if (
          columnsMap[columns.viableCellDensity] !== columns.viableCellDensity
        ) {
          delete row[columns.viableCellDensity];
        }
        if (columnsMap[columns.batchId] !== columns.batchId) {
          delete row[columns.batchId];
        }
        if (columnsMap[columns.time] !== columns.time) {
          delete row[columns.time];
        }
        return row;
      });
    const dataSet = mapRowsToObject(notTransformed);

    mutateGrowthKinetics({
      projectId,
      batchId: selectedInSelectorBatch as string,
      data: dataSet
    })
      .then(() => {
        onChangeSelectedPoints([]);
        onChangeExcludedPoints([]);
        onChangeSelectedBatches([]);
        onChangeExcludedBatches([]);
        onSelectPoint(null);
      })
      .finally(() => setLoading(false));
  }, [
    excludedBatches,
    excludedPoints,
    mappedInitialData,
    mutateGrowthKinetics,
    project.project?.columns_map,
    projectId,
    selectedInSelectorBatch
  ]);

  const isTiter = history.location.pathname.includes('titer');
  const isMetabolites = history.location.pathname.includes(
    'fitting-metabolites'
  );
  console.log('>>>>> metabolites:', metabolites);
  const vccRenderTooltip = useRenderTooltip('VCC');
  const viabilityRenderTooltip = useRenderTooltip('Viability');
  const metaboliteRenderTooltip = useRenderTooltip(selectedMetabolite);
  const titerRenderTooltip = useRenderTooltip('Titer');
  const selected1RenderTooltip = useRenderTooltip(selected1);
  const selected2RenderTooltip = useRenderTooltip(selected2);
  const selected3RenderTooltip = useRenderTooltip(selected3);
  const xAxis = useXAxis();
  const lines = useLines(batchNames, selectedBatches, excludedBatches);
  console.log('>>> selectedPoint:', selectedPoint);
  const commonGraphicsProps = {
    xAxis,
    showIncludeExcludeControls: true,
    connectPointsByLines: false,
    selectedPoints,
    onChangeSelectedPoints,
    selectedBatches,
    onChangeSelectedBatches,
    excludedPoints,
    onChangeExcludedPoints,
    excludedBatches,
    onChangeExcludedBatches,
    selectedPoint,
    onSelectPoint,
    lines
  };

  return (
    <div className={s.ProjectSettings}>
      <Header />
      <ProjectBreadcrumbs />
      <div className={s.ProjectSettings__content}>
        <div className={s.ModelConfiguring__contentLeft}>
          <Heading
            className={s.ProjectSettings__headingH2}
            variant={HeadingVariant.H2}
          >
            Fitting: {isMetabolites ? 'Metabolites' : isTiter ? 'Titer' : 'Growth Kinetics'}
          </Heading>
          <LastSaved />
        </div>
        <Button
          hoverVariant={ButtonVariant.ACTION}
          rightIcon={<SaveAndContinueIcon />}
          uppercase
          size="small"
          onClick={() => {
            history.push(
              Paths.CLONE_SELECTION_SIMULATION.replace(
                /:projectId/gi,
                projectId
              )
            );
          }}
        >
          Save & Continue
        </Button>
      </div>
      <div className={s.Fitting__recalculateWrapper}>
        <div className={s.Fitting__selectors}>
          <div>
            <div className={s.Fitting__recalculateLabel}>Batch selector</div>
            <div>
              <MultiSelect
                value={selectedBatches}
                onChange={onChangeSelectedBatches}
                options={batchNames.map((batchName) => ({
                  value: batchName,
                  label: batchName
                }))}
              />
            </div>
          </div>
          {isMetabolites && <div>
            <div className={s.Fitting__recalculateLabel}>
              Metabolite selector
            </div>
            <div>
              <MultiSelect
                value={[selectedMetabolite]}
                onlyOne
                onChange={(v) => setSelectedMetabolite(v[0])}
                options={metabolites.map((metabolite) => ({
                  value: metabolite.name,
                  label: metabolite.name
                }))}
              />
            </div>
          </div>}
        </div>
        <Button
          uppercase
          variant={ButtonVariant.ACTION}
          onClick={handleRecalculate}
          disabled={isLoading}
        >
          Recalculate
        </Button>
      </div>
      <div className={s.ModelConfiguring__frames}>
        <div>
          <div>
            <div className={s.Fitting__recalculateLabel}>
              Measured and Predicted Fit
            </div>
            <div className={s.Fitting__measuredVsPredictedFit}>
              <div>
                <div
                  style={{
                    height: 'calc(100vh - 300px + 35px)',
                    width: '100%',
                    border: '1px solid #BDBDBD'
                  }}
                >
                  <Graphic
                    data={viableCellDensity}
                    renderTooltip={vccRenderTooltip}
                    yAxisUnit={columns.viableCellDensity}
                    {...commonGraphicsProps}
                    selectedPoint={computeSelectedPointToGraphic(
                      viableCellDensity,
                      commonGraphicsProps.selectedPoint,
                      columns.viableCellDensity
                    )}
                    selectedPoints={computeExcludedPointsToGraphic(
                      viableCellDensity,
                      selectedPoints,
                      columns.viableCellDensity
                    )}
                    excludedPoints={computeExcludedPointsToGraphic(
                      viableCellDensity,
                      excludedPoints,
                      columns.viableCellDensity
                    )}
                  />
                </div>
                <div className={s.Fitting__variableName}>
                  Viable cell density
                </div>
              </div>

              <div>
                <div
                  style={{
                    height: 'calc(100vh - 300px + 35px)',
                    width: '100%',
                    border: '1px solid #BDBDBD'
                  }}
                >
                  <Graphic
                    data={
                      isMetabolites ? metabolite : isTiter ? titer : viability
                    }
                    yAxisUnit={
                      isMetabolites
                        ? selectedMetabolite
                        : isTiter
                        ? columns.titer
                        : columns.viability
                    }
                    renderTooltip={
                      isMetabolites
                        ? metaboliteRenderTooltip
                        : isTiter
                        ? titerRenderTooltip
                        : viabilityRenderTooltip
                    }
                    {...commonGraphicsProps}
                    selectedPoint={computeSelectedPointToGraphic(
                      isMetabolites ? metabolite : isTiter ? titer : viability,
                      commonGraphicsProps.selectedPoint,
                      isMetabolites
                        ? selectedMetabolite
                        : isTiter
                        ? columns.titer
                        : columns.viability
                    )}
                    selectedPoints={computeExcludedPointsToGraphic(
                      isMetabolites ? metabolite : isTiter ? titer : viability,
                      selectedPoints,
                      isMetabolites
                        ? selectedMetabolite
                        : isTiter
                        ? columns.titer
                        : columns.viability
                    )}
                    excludedPoints={computeExcludedPointsToGraphic(
                      isMetabolites ? metabolite : isTiter ? titer : viability,
                      excludedPoints,
                      isMetabolites
                        ? selectedMetabolite
                        : isTiter
                        ? columns.titer
                        : columns.viability
                    )}
                  />
                </div>
                <div className={s.Fitting__variableName}>
                  {isMetabolites ? selectedMetabolite : isTiter ? 'Titer' : 'Viability'}
                </div>
              </div>
            </div>
            <div className={s.Fitting__recalculateLabel}>
              Variable Trajectories
            </div>
            <div className={s.Fitting__variablesGraphics}>
              <div>
                <div className={s.Fitting__variablesGraphicsSelect}>
                  <div className={s.Fitting__recalculateSelectWrapper}>
                    <select
                      value={selected1}
                      onChange={(e) => setSelected1(e.target.value)}
                      className={s.Fitting__recalculateSelect}
                    >
                      <option value="">Select variable</option>
                      {variablesNames.map((col) => (
                        <option key={col} value={col}>
                          {col}
                        </option>
                      ))}
                    </select>
                  </div>
                </div>
                <div
                  style={{
                    marginTop: '10px',
                    height: '400px',
                    width: '23vw'
                  }}
                >
                  <Graphic
                    data={variableSelector1}
                    yAxisUnit={selected1}
                    renderTooltip={selected1RenderTooltip}
                    {...commonGraphicsProps}
                    selectedPoint={computeSelectedPointToGraphic(
                      variableSelector1,
                      commonGraphicsProps.selectedPoint,
                      selected1
                    )}
                    selectedPoints={computeExcludedPointsToGraphic(
                      variableSelector1,
                      selectedPoints,
                      selected1
                    )}
                    excludedPoints={computeExcludedPointsToGraphic(
                      variableSelector1,
                      excludedPoints,
                      selected1
                    )}
                  />
                </div>
              </div>
              <div>
                <div className={s.Fitting__variablesGraphicsSelect}>
                  <div className={s.Fitting__recalculateSelectWrapper}>
                    <select
                      value={selected2}
                      onChange={(e) => setSelected2(e.target.value)}
                      className={s.Fitting__recalculateSelect}
                    >
                      <option value="">Select variable</option>
                      {variablesNames.map((col) => (
                        <option key={col} value={col}>
                          {col}
                        </option>
                      ))}
                    </select>
                  </div>
                </div>
                <div
                  style={{
                    marginTop: '10px',
                    height: '400px',
                    width: '23vw'
                  }}
                >
                  <Graphic
                    data={variableSelector2}
                    yAxisUnit={selected2}
                    renderTooltip={selected2RenderTooltip}
                    {...commonGraphicsProps}
                    selectedPoint={computeSelectedPointToGraphic(
                      variableSelector2,
                      commonGraphicsProps.selectedPoint,
                      selected2
                    )}
                    selectedPoints={computeExcludedPointsToGraphic(
                      variableSelector2,
                      selectedPoints,
                      selected2
                    )}
                    excludedPoints={computeExcludedPointsToGraphic(
                      variableSelector2,
                      excludedPoints,
                      selected2
                    )}
                  />
                </div>
              </div>
              <div>
                <div className={s.Fitting__variablesGraphicsSelect}>
                  <div className={s.Fitting__recalculateSelectWrapper}>
                    <select
                      value={selected3}
                      onChange={(e) => setSelected3(e.target.value)}
                      className={s.Fitting__recalculateSelect}
                    >
                      <option value="">Select variable</option>
                      {variablesNames.map((col) => (
                        <option key={col} value={col}>
                          {col}
                        </option>
                      ))}
                    </select>
                  </div>
                </div>
                <div
                  style={{
                    marginTop: '10px',
                    height: '400px',
                    width: '23vw'
                  }}
                >
                  <Graphic
                    data={variableSelector3}
                    yAxisUnit={selected3}
                    renderTooltip={selected3RenderTooltip}
                    {...commonGraphicsProps}
                    selectedPoint={computeSelectedPointToGraphic(
                      variableSelector3,
                      commonGraphicsProps.selectedPoint,
                      selected3
                    )}
                    selectedPoints={computeExcludedPointsToGraphic(
                      variableSelector3,
                      selectedPoints,
                      selected3
                    )}
                    excludedPoints={computeExcludedPointsToGraphic(
                      variableSelector3,
                      excludedPoints,
                      selected3
                    )}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
        <div>
          <div className={s.Fitting__infos}>
            {selectedBatches.map((selectedBatch) => (
              <React.Fragment key={selectedBatch}>
                <Info
                  title={`Kinetic Summary(${selectedBatch})`}
                  fields={[
                    {
                      name: 'Growth Rate',
                      value: formatIfNumber(
                        (fittingData?.kineticSummary as any)[selectedBatch]
                          ?.growth_parameters.primary_growth_rate
                      )
                    },
                    {
                      name: 'Death Rate',
                      value: formatIfNumber(
                        (fittingData?.kineticSummary as any)[selectedBatch]
                          ?.growth_parameters.primary_death_rate
                      )
                    },
                    {
                      name: 'Toxicity',
                      value: formatIfNumber(
                        (fittingData?.kineticSummary as any)[selectedBatch]
                          ?.growth_parameters.toxicity_rate
                      )
                    },
                    {
                      name: 'Lysing Rate',
                      value: formatIfNumber(
                        (fittingData?.kineticSummary as any)[selectedBatch]
                          ?.growth_parameters.lysing_rate
                      )
                    },
                    {
                      name: 'Inhibition',
                      value: formatIfNumber(
                        (fittingData?.kineticSummary as any)[selectedBatch]
                          ?.inhibitor_factors[0]?.threshold
                      )
                    }
                  ]}
                />
                <Info
                  title={`Viability Fit Statistics(${selectedBatch})`}
                  fields={[
                    {
                      name: 'RSquare',
                      value: fittingData?.fit_statistics?.[selectedBatch]?.['Cell viability']
                        ?.r_squared
                        ? formatIfNumber(
                            fittingData?.fit_statistics?.[selectedBatch]?.['Cell viability']
                              ?.r_squared
                          )
                        : '-'
                    },
                    // {
                    //   name: 'RSquare adj',
                    //   value: '-'
                    // },
                    {
                      name: 'Root mean sq error',
                      value: fittingData?.fit_statistics?.[selectedBatch]?.['Cell viability']?.rmse
                        ? formatIfNumber(
                            fittingData?.fit_statistics?.[selectedBatch]?.['Cell viability']?.rmse
                          )
                        : '-'
                    }
                  ]}
                />
                <Info
                    title={`VCD Fit Statistics(${selectedBatch})`}
                    fields={[
                      {
                        name: 'RSquare',
                        value: fittingData?.fit_statistics?.[selectedBatch]?.['Viable cell density']
                            ?.r_squared
                            ? formatIfNumber(
                                fittingData?.fit_statistics?.[selectedBatch]?.['Viable cell density']
                                    ?.r_squared
                            )
                            : '-'
                      },
                      // {
                      //   name: 'RSquare adj',
                      //   value: '-'
                      // },
                      {
                        name: 'Root mean sq error',
                        value: fittingData?.fit_statistics?.[selectedBatch]?.['Viable cell density']?.rmse
                            ? formatIfNumber(
                                fittingData?.fit_statistics?.[selectedBatch]?.['Viable cell density']?.rmse
                            )
                            : '-'
                      }
                    ]}
                />
              </React.Fragment>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};
