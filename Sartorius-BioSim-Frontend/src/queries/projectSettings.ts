import { useMutation, useQuery } from 'react-query';
import { QUERIES } from './queriesEnum';
import {
  FitParameter,
  FitProcedure,
  ProjectSettings,
  ProjectSettingsApi
} from '../api/projectSettings';
import { MUTATIONS } from './mutationsEnum';

export const useProjectSettings = (projectId?: string | null) => {
  return useQuery(
    [QUERIES.PROJECT_SETTINGS, projectId],
    () => ProjectSettingsApi.getById(projectId || ''),
    {
      enabled: Boolean(projectId)
    }
  );
};

export const useCreateProjectSettings = () =>
  useMutation(
    ({
      projectId,
      projectSettings,
      fitParameters,
      fitProcedures
    }: {
      projectId: string;
      projectSettings: ProjectSettings;
      fitProcedures: FitProcedure[];
      fitParameters: FitParameter[];
    }) => {
      return ProjectSettingsApi.create(projectId, {
        projectSettings,
        fitParameters,
        fitProcedures
      });
    },
    {
      mutationKey: MUTATIONS.UPDATE_PROJECT_SETTINGS
    }
  );

export const useUpdateProjectSettings = () =>
  useMutation(
    ({
      projectId,
      projectSettings,
      fitParameters,
      fitProcedures
    }: {
      projectId: string;
      projectSettings: ProjectSettings & { id: string };
      fitProcedures: (FitProcedure & { id: string })[];
      fitParameters: (FitParameter & { id: string })[];
    }) => {
      return ProjectSettingsApi.update({
        projectId,
        projectSettings,
        fitParameters,
        fitProcedures
      });
    },
    {
      mutationKey: MUTATIONS.UPDATE_PROJECT_SETTINGS
    }
  );

export const useUpdateFitParameters = () =>
  useMutation(
    ({
      projectId,
      fitParameters
    }: {
      projectId: string;
      fitParameters: (Partial<FitParameter> & { id: string })[];
    }) => ProjectSettingsApi.updateFitParameters(projectId, fitParameters),
    { mutationKey: MUTATIONS.UPDATE_FIT_PARAMETERS }
  );
