import { useMutation, useQuery, useQueryClient } from 'react-query';
import { QUERIES } from './queriesEnum';
import {
  SimulationApi,
  SimulationInitialConditions,
  SimulationParameters
} from '../api/simulation';
import { MUTATIONS } from './mutationsEnum';
import { SimulationMode } from '../pages/simulation';

export const useGrowthKinetics = ({
  projectId
}: {
  projectId?: string | null;
}) =>
  useQuery(
    [QUERIES.GROWTH_KINETICS],
    () => {
      return SimulationApi.getGrowthKinetics({
        projectId: projectId as string
      });
    },
    {
      enabled: Boolean(projectId)
    }
  );

export const useUpdateGrowthKinetics = () => {
  const queryClient = useQueryClient();
  return useMutation(
    ({
      projectId,
      batchId,
      data
    }: {
      projectId: string;
      batchId: string;
      data: object;
    }) => {
      return SimulationApi.recalculateGrowthKinetics({
        projectId,
        batchId,
        data
      });
    },
    {
      mutationKey: MUTATIONS.UPDATE_GROWTH_KINETICS,
      onSuccess() {
        queryClient.refetchQueries(QUERIES.INITIAL_DATA);
        queryClient.refetchQueries(QUERIES.GROWTH_KINETICS);
      }
    }
  );
};

export const useRunSimulation = (projectId: string) =>
  useMutation(
    MUTATIONS.RUN_SIMULATION,
    (body: {
      initial_conditions: SimulationInitialConditions;
      simulation_parameters: SimulationParameters;
      simulation_mode: SimulationMode;
    }) => SimulationApi.runSimulation({ projectId, ...body })
  );
