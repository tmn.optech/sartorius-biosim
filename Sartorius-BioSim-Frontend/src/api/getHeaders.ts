export const getHeaders = () => {
  return {
    Authorization: `Bearer ${localStorage.getItem('TOKEN')}`,
    'Content-Type': 'application/json'
  };
};
