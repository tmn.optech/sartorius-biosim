export type RequestWrapper<T> = {
  data: T;
  status: number;
};
