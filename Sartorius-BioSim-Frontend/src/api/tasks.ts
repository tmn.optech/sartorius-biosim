import { get } from './methods';

export type Task = { task_id: string; project_id: string };
export class TasksApi {
  static status = (taskId: string) => get(`queue/${taskId}/status`);

  static getAll = () => get<Task[]>('queue/tasks');
}
