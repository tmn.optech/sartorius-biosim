import React, { useCallback } from 'react';
import { CustomizedDot } from '../components/CustomizedDot';
import { REGULAR_COLOR } from '../constants';
import { Line } from 'recharts';

export const useRenderCustomizedLine = ({
  xAxisKey,
  connectPointsByLines
}: {
  xAxisKey: string;
  connectPointsByLines?: boolean;
}) => {
  const dot = useCallback(
    (props) => {
      if (props?.payload?.hidden) return <>{null}</>;
      return <CustomizedDot xAxisKey={xAxisKey} {...props} />;
    },
    [xAxisKey]
  );
  return useCallback(
    ({
      dataKey,
      color = REGULAR_COLOR,
      onlyLine,
      onlyDots,
      strokeWidth = onlyLine ? 1 : onlyDots ? 0 : connectPointsByLines ? 1 : 0
    }: {
      dataKey: string;
      color?: string;
      onlyLine?: boolean;
      onlyDots?: boolean;
      strokeWidth?: number;
    }) => {
      return (
        <Line
          animationDuration={0}
          isAnimationActive={false}
          connectNulls={true}
          name={dataKey}
          dot={onlyLine ? false : dot}
          activeDot={onlyLine ? false : dot}
          type="monotone"
          dataKey={dataKey}
          stroke={color}
          strokeWidth={strokeWidth}
        />
      );
    },
    [connectPointsByLines, dot]
  );
};
