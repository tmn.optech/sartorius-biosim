import React, { CSSProperties, useCallback, useMemo } from 'react';
import { ReferenceDot } from 'recharts';

const useReferenceDotStyle = (): CSSProperties =>
  useMemo(
    () => ({
      pointerEvents: 'none'
    }),
    []
  );
export const useRenderReferenceDot = () => {
  const style = useReferenceDotStyle();
  return useCallback(
    ({
      color,
      point
    }: {
      color: string;
      point: { x: number | null; y: number | null };
    }) =>
      Boolean(point.x && point.y) && (
        <ReferenceDot
          strokeWidth={1}
          stroke={color}
          fill={color}
          x={point.x as number}
          y={point.y as number}
          r={3}
          style={style}
        />
      ),
    [style]
  );
};
