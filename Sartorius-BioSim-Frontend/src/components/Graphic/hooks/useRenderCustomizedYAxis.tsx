import React, { useCallback } from 'react';
import { YAxis } from 'recharts';

export const useRenderCustomizedYAxis = (yAxisUnit?: string) => {
  const render = useCallback(
    () => (
      <YAxis
        name={yAxisUnit}
        label={{
          value: yAxisUnit,
          angle: -90
        }}
        width={90}
        orientation="right"
      />
    ),
    [yAxisUnit]
  );
  if (!yAxisUnit) return () => null;
  return render;
};
