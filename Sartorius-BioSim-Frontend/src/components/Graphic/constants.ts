export const DOT_CLICK_EVENT = 'SARTORIUS_BIO_SIM__DOT_CLICK';

export const HOVER_COLOR = '#00A7FF';
export const SELECTED_COLOR = '#54214D';
export const SELECTED_BATCH = '80D4FF';
export const EXCLUDED_COLOR = '#C10202';

export const REGULAR_COLOR = '#BDBDBD';
export const PREDICTED_LINE_COLOR = '#606060';
