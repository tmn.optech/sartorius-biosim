import React, { useEffect, useState } from 'react';
import { TasksApi } from '../../api/tasks';
import { useHistory, useParams } from 'react-router-dom';
import { ProjectsApi } from '../../api/projects';
import { useProject } from '../../queries/projects';
/* eslint-disable */
export const AuthGuard = (Component: React.FC) => () => {
  const { projectId } = useParams<{ projectId?: string }>();
  const project = useProject(projectId);
  const history = useHistory();
  const [isAvailable, setIsAvaliable] = useState(false);
  useEffect(() => {
    ProjectsApi.rendering()
      .then(() => {
        setIsAvaliable(true);
      })
      .catch(() => {
        setIsAvaliable(false);
        history.push('/login');
        localStorage.removeItem('TOKEN');
      });
  });
  useEffect(() => {
    if (project && project.project?.render_status === 'Rendering') {
      history.push('/projects');
    }
  }, [project.project, history]);
  if (isAvailable) {
    return <Component />;
  }
  return <></>;
};
