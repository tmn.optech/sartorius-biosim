import React from 'react';
import clsx from 'clsx';

import s from './TextBold.module.scss';
import { BaseComponent } from '../../utils/BaseComponent';

export interface TextBoldProps extends BaseComponent {}

export const TextBold: React.FC<TextBoldProps> = ({ className, children }) => (
  <span className={clsx(className, s.TextBold)}>{children}</span>
);
