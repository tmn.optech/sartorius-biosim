import React from 'react';
import s from './ItsMockedData.module.scss';

export const ItsMockedData: React.FC = ({
  children = 'You see mocked data'
}) => {
  return <div className={s.ItsMockedData}>{children}</div>;
};
