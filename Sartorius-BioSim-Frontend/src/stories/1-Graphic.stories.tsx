import React, { useMemo, useState } from 'react';
import { Graphic } from '../components';
import {
  CustomizedTooltipField,
  CustomizedTooltipFieldValue
} from '../components/Graphic/components/CustomizedTooltip';

export default {
  title: 'Graphic',
  component: Graphic
};

export const GraphicStory = () => {
  const data = useMemo(() => {
    return new Array(100).fill(null).map((_, idx) => ({
      id: idx + 1,
      a: Math.random() * 10,
      b: Math.random() * 100,
      c: Math.random() * 200,
      d: Math.random() * 300
    }));
  }, []);
  const xAxis = useMemo(() => ({ label: 'Test label', dataKey: 'a' }), []);
  const lines = useMemo(
    () => [
      {
        name: 'B',
        dataKey: 'b'
      },
      {
        name: 'C',
        dataKey: 'c'
      },
      {
        name: 'D',
        dataKey: 'd'
      }
    ],
    []
  );
  const [excludedPoints, onChangeExcludedPoints] = useState<any[]>([]);
  const [excludedBatches, onChangeExcludedBatches] = useState<string[]>([]);
  return (
    <div style={{ width: 800, height: 800 }}>
      <Graphic
        excludedBatches={excludedBatches}
        excludedPoints={excludedPoints}
        onChangeExcludedBatches={onChangeExcludedBatches}
        onChangeExcludedPoints={onChangeExcludedPoints}
        showIncludeExcludeControls={true}
        renderTooltip={(payload) => (
          <div>
            <CustomizedTooltipField>
              x:{' '}
              <CustomizedTooltipFieldValue>
                {payload.x}
              </CustomizedTooltipFieldValue>
            </CustomizedTooltipField>
            <CustomizedTooltipField>
              y:{' '}
              <CustomizedTooltipFieldValue>
                {payload.y}
              </CustomizedTooltipFieldValue>
            </CustomizedTooltipField>
          </div>
        )}
        lines={lines}
        data={data}
        xAxis={xAxis}
      />
    </div>
  );
};
