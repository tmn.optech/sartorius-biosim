## Dependence install - needs to be installed in advance

- Redis 6.X
- PostgreSQL 11-14
- Kubernetes 1.19 - 1.21
- Private docker registry

## Redis install
```
cd system-utils/keydb
helm upgrade --install redis -f values.yaml .
```
## PostgreSQL install 
```
cd system-utils/postgresql
helm upgrade --install postgresql -f values.yaml .
```

## Build Frontend

- Step 1

```
cd ./Sartorius-Deploy/Sartorius-BioSim-Frontend
```

- Step 2
```
edit .env  # specify fqdn record backend api
```

- Step 3
```
docker build -t registry-host/sartorius-frontend:{tag} .
docker push registry-host/sartorius-frontend:{tag}
```

## Deploy Frontend

- Step 1

```
cd ./Sartorius-Deploy/Sartorius-BioSim-Frontend
```

- Step 2
```
edit helm/values.yaml 
...
change env

image:
  repository: registry-host/sartorius-frontend
  tag: {tag}
  pullPolicy: Always
  secrets:
    - name: docker-registry 
```

- Step 3

```
cd helm
kubectl create ns sartorius-biosim
kubectl -n sartorius-biosim  create secret docker-registry docker-registry --docker-server=REGISTRY_HOST --docker-username=DOCKER_USERNAME --docker-password=DOCKER_PASSWORD --docker-email=DOCKER_EMAIL
helm upgrade --install -n sartorius-biosim sartorius-frontend  -f values.yml .
```
#####################################################################################

## Build Backend

- Step 1

```
cd ./Sartorius-Deploy/Sartorius-BioSim-Backend
```

- Step 2
```
docker build -t registry-host/sartorius-backend:{tag} .
docker push registry-host/sartorius-backend:{tag}
```

## Deploy Backend

- Step 1

```
cd ./Sartorius-Deploy/Sartorius-BioSim-Backend
```

- Step 2
```
edit helm/values.yaml 
...
change env

image:
  repository: registry-host/sartorius-backend
  tag: {tag}
  pullPolicy: Always
  secrets:
    - name: docker-registry 
    ...
    
 extraEnv:
  FLASK_ENV: production
  FLASK_APP: app.py
  DB_HOST: ""
  DB_PORT: "5432"
  DB_NAME: postgres
  DB_USER: postgres
  DB_PASSWORD: ""
  SECRET_KEY: secret
  DEBUG: "True"
  S3_ENDPOINT: ""
  S3_ACCESS_KEY: ""
  S3_SECRET_KEY: ""
  BUCKET_NAME: ""
  CELERY_BROKER_URL: "redis://redis:6379/0"
  FLOWER_PORT: "5555"
  FLOWER_BASIC_AUTH: "admin:admin"
   
```

- Step 3

```
cd helm
helm upgrade --install -n sartorius-biosim sartorius-backend  -f values.yml .
```
